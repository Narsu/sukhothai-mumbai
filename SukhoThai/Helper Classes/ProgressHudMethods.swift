//
//  ProgressHudMethods.swift
//  SukhoThai
//
//  Created by kesarimac2 on 01/08/18.
//  Copyright © 2018 Kesari Tours. All rights reserved.
//

import Foundation
import HKProgressHUD

struct ProgressHud {
    
    static func textHud(onview view:UIView,message:String) {
        let hud = HKProgressHUD.show(addedToView: view, animated: true)
        
        // Set the Text mode
        hud.mode = .text
        hud.label?.lineBreakMode = .byWordWrapping
        hud.label?.numberOfLines = 2
        hud.label?.text = NSLocalizedString(message, comment: "hud message title")
        // Move to bottom center.
//        hud.offset = CGPoint(x: 0, y: HKProgressHUD.maxOffset)
        hud.center = view.center
        hud.hide(animated: true, afterDelay: 1.0)
        
    }
   static func hide(fromView:UIView) {
        _ = HKProgressHUD.hide(addedToView: fromView, animated: true)
    }
    
    static func progressHud(onview view:UIView) {
        _ = HKProgressHUD.show(addedToView: view, animated: true)
    }
}
