//
//  File.swift
//  Team KT
//
//  Created by kesarimac2 on 02/06/17.
//  Copyright © 2017 kesarimac2. All rights reserved.
//

import Foundation
import UIKit

//var OMKAR_IP = "http://192.168.1.27:5001"
//var STAGING_IP = "http://192.168.1.230:5002"
//var VINAY_IP = "http://192.168.1.7:5002"
//var SIDDHESH_IP = "http://192.168.1.8:5002"
var LIVE_IP = "http://login.sukhothai.in"
var HOST_IP = LIVE_IP

func urlGetDestination() -> String{
    return HOST_IP + "/route/designation/getDesignation"
}
func getLoginUrl() -> String {
    return HOST_IP + "/app/login"
}
func getSendPasswordUrl() -> String {
    return HOST_IP + "/route/F21_STAF/getPassword"
}
func urlPostRegister() -> String{
    return HOST_IP + "/route/sukhoThaiMaster/staffRegistration"
}
func urlPostLogin() -> String{
    return HOST_IP + "/route/sukhoThaiMaster/staffLogin"
}
func urlPostTherapistLogin() -> String{
    return HOST_IP + "/route/sukhoThaiMaster/therapistLogin"
}
func urlGetAllSukhoThai() -> String{
    return HOST_IP + "/route/branches/getAllBranches"
}

func urlGetAllTherapist(branchCode:String) -> String{
    ///route/F21_STAF/getTherapistList/\(branchCode)
    return HOST_IP + "/route/F21_STAF/getTherapistList/\(branchCode)"
}
func urlUpdateThPBusyFlag() -> String{
    return HOST_IP + "/route/appointmentST/updateTHPBussyFlag"
}
 
func urlGetAllTherapyList() -> String{
    let branchcode = getUserdefaultDataForKey(Key: BRANCH) as! String
    return HOST_IP + "/route/therapiesST/getTherapyList/\(branchcode)"
}
func urlGetAllTherapyCodeDetails(branchCode:String) -> String{                // mark 1
    return HOST_IP + "/route/branchwiseTherapyPricing/list/\(branchCode)"
}

func urlPostCreateTherapyCode() -> String{
    return HOST_IP + "/route/branchwiseTherapyPricing/"
}

func urlGetAllTherapyCodes() -> String{
    return HOST_IP + "/route/therapycode/getAllTherapyCode"
}

func urlPostAppointment(uniqueID : String) -> String{
    //return HOST_IP + "/route/therapyAppointmentCode"
    return HOST_IP + "/route/therapyAppointmentCode/updateGuestAppointment/\(uniqueID)"
 
}

func urlGetAllAppointments(branchCode : String) -> String{
    return HOST_IP + "/route/therapyAppointmentCode/getAllAppointment/\(branchCode)"
}

func urlPostTransaction() -> String{
    return HOST_IP + "/route/dailyTransaction/getAllDailyTransaction"
}

func urlPostReceipt() -> String{
    return HOST_IP + "/route/dailyTransaction/getReceiptRecords"
}
func urlPostFeedbackData() -> String{
    return HOST_IP + "/route/guestFeedBackMaster"
}
func urlGetFeedbackData(branchCode : String) -> String{
    return HOST_IP + "/route/guestFeedBackMaster/getGuestFeedBackRecords/\(branchCode)"
}
func urlGetAllCreatedTherapyCode(branchCode: String) -> String{
    //route/appointmentST/getAppointmentRecords/IN003/receptionApp
    return HOST_IP + "/route/appointmentST/getAppointmentRecords/\(branchCode)/receptionApp"
}
func urlGetNotBookedTherapyCode(branchCode: String) -> String{
    //route/appointmentST/getAppointmentRecords/IN003/receptionApp
    return HOST_IP + "/route/appointmentST/getIncompleteAppointment/Y"
}
func urlPostCreateAppointment() -> String{
    return HOST_IP + "/route/appointmentST/createAppointment"
}
func urlPutBookAppointment() -> String{
    return HOST_IP + "/route/appointmentST/bookingAppointment"
}
func urlPostBillingData(id : String) -> String{
    return HOST_IP + "/route/dailyTransaction/createBilling/\(id)"
}

func urlCreateGroup() -> String{
    return HOST_IP + "/route/appointmentST/createGroupingApp"
}

func urlPostReceiptByDate() -> String {
    return HOST_IP + "/route/appointmentST/getAppointmentsOfApp"
}
func urlGetGiftCards(number : String) -> String{
    return HOST_IP + "/route/appointmentST/validateGiftVoucherForReceiptThroughIOS/\(number)"
}
func urlAppointmentReceipt() -> String{
    return HOST_IP + "/route/appointmentST/generateAppointmentReceiptApp"
}
//route/appointmentST/generateAppointmentReceiptApp
func urlPostCategoryData() -> String{
    return HOST_IP + "/route/taxiDriver/saveDriverDtl"
}
func urlGetCategoryData(Category : String) -> String{
    return HOST_IP + "/route/taxiDriver/driverList/\(Category)" //   "/TAXI" SAME API AS HOTEL LIST BY APPENDING /(CATEGORY)
}
func urlGetPrinterStatus() -> String{
     let branchcode = getUserdefaultDataForKey(Key: BRANCH) as! String
    return HOST_IP + "/route/appointmentST/checkPOSPrinterExists/\(branchcode)"
}
//    /route/appointmentST/checkPOSPrinterExists
func urlScanGiftCard(number:String) -> String{
    return HOST_IP + "/route/taxiDriver/validateCardNumToAssignHotel?cardNo=\(number)&cardType=Hotel Promo"
}
func urlAssignCardToHotel() -> String{
    return HOST_IP + "/route/taxiDriver/updateCardNumToAssignHotel"
}
func urlgetHotelList() -> String{
    return HOST_IP + "/route/taxiDriver/getAssignHotelList"
}
func urlGetHotels(name:String) -> String{    //get hotel names
    return HOST_IP + "/route/taxiDriver/cityWiseHotelList/\(name)"
}
func urlGetStandingLocation() -> String{
    return HOST_IP + "/route/taxiDriver/standLocation"
}
func urlGetCardDetails(number:String) -> String{    //get Card Details
    return HOST_IP + "/route/appointmentST/validateGiftCard/\(number)"
}

func urlGetCardLedger(number : String) -> String{   //get Card Status
    return HOST_IP + "/route/cardLoadings/getLedgerTableData/\(number)"
}
