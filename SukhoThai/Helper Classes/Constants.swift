//
//  Constants.swift
//  Team KT
//
//  Created by kesarimac2 on 01/06/17.
//  Copyright © 2017 kesarimac2. All rights reserved.
//

import Foundation
import UIKit
let GET:String = "GET"
let POST:String = "POST"
let PUT:String = "PUT"
let DELETE:String = "DELETE"

let GET_REALM:String = "GET"
let SET_REALM:String = "SET"



//MARK:- login constants

let USER_DETAILS_DICT:String = "userDetails"
let SUPPLY_NO:String = "supplyNo"
let USER_ID:String = "uid"
let USER_NAME:String = "name"
let USER_EMAIL:String = "emailId"
let USER_MOBILE_NO:String = "MobileNo"
let IS_USER_LOGGED_IN = "isUserLoggedIn"
let IS_HISTORY_USER = "isHistoryUser"
let DEPARTMENT:String = "department"
let LOCATION_ID:String = "locationId"

let IS_RECEPTION_LOGGED_IN = "isReceptionLoggedIn"
let RECEPTIONIST_ID = "ReceptionistID"
let RECEPTIONIST_FNAME = "ReceptionistFname"
let RECEPTION_LOGGED_IN_DATETIME = "ReceptionLoggedInDateTime"
let PRINTER_IP_ADDRESS = "ipaddress"
let TECEPTIONS_ID:String = "name"

let ARN_CODE:String = "arnCode"
let USER_COMPANY:String = "company"
let IS_USER_ADMIN = "isUserAdmin"

let CURRENT_TOUR_ID = "currentTourId"
let FORM_NO = "formNo"
let TN_NO = "tnNo"
let TOKEN = "token"
let DEVICE_ID:String = "deviceId"
let FIREBASE_TOKEN:String = "fbt"
let BRANCH = "branch"
let COLOR_SEARCHBAR_BACKGROUND = UIColor(red: 176/255, green: 0/255, blue: 18/255, alpha: 1.0)



let URL_MICE = "http://www.kesarimice.in/"
let URL_BLOG = "http://blog.kesari.in/"
let TERM_CONDITION="https://www.kesari.in/terms-conditions"
let PRIVACY_POLICY="https://www.kesari.in/Privacy-Policy"
let CONTACT_US = "https://www.kesari.in/contact"
let ABOUT_US = "https://www.kesari.in/About-Us"
//MARK: Alert Messages

let MSG_FNAME = "Please enter first name."
let MSG_LNAME = "Please enter last name."
let MSG_MOBNO = "Please enter mobile number."
let MSG_SELECT_TOUR_TYPE = "Please select tour type."
let MSG_EXPECTED_TOUR_DATE = "Please enter expected tour date."
let MSG_EMAIL = "Please enter email id."
let MSG_CORRECT_EMAIL = "Please enter valid email id."
let MSG_CORRECT_MOBNO = "Please enter valid mobile number."
let MSG_WHATSAPP_MOBNO = "Please enter Whatsapp number."
let MSG_CORRECT_WHATSAPP_MOBNO = "Please enter valid Whatsapp number."

let MSG_ENTER_TITLE = "Please enter title."
let MSG_ENTER_DESCRIPTION = "Please enter description"


let MSG_SELECT_FEEDBACK_TYPE = "Please select feedback type."

//MARK: Work to do alert messages

let MSG_ASSIGN_NAME = "Please enter first name."
let MSG_WORK_TITLE = "Please enter last name."
let MSG_EXPECTED_DATE = "Please enter mobile number."

//MARK: Color Suraj
let separatorColor : UIColor = #colorLiteral(red: 0.4115892947, green: 0.2305042148, blue: 0.4444019794, alpha: 1)
let viewBackGroundColor : UIColor = #colorLiteral(red: 0.1764705882, green: 0.01960784314, blue: 0.2, alpha: 1)
let tableviewBackgroundColor : UIColor = #colorLiteral(red: 0.1764705882, green: 0.01960784314, blue: 0.2, alpha: 1)
let cellBackGroundColor : UIColor = #colorLiteral(red: 0.1764705882, green: 0.01960784314, blue: 0.2, alpha: 1)

//localization

let networkConnectionAlert = LocalizationSystem.sharedInstance.localizedStringForKey(key: "internetconnectionalert", comment: "")
let fillInReqDetailsAlert = LocalizationSystem.sharedInstance.localizedStringForKey(key: "fillinreqdetails", comment: "")
let requestTimeOutAlert = LocalizationSystem.sharedInstance.localizedStringForKey(key: "RequestTimeout", comment: "")
let MSG_ENTER_SUGGESTION = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enterfeedback", comment: "")
let noDataAlert = LocalizationSystem.sharedInstance.localizedStringForKey(key: "NoDataAvailable", comment: "")

let selectConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select", comment: "")
let only = LocalizationSystem.sharedInstance.localizedStringForKey(key: "only", comment: "")
let therapistConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Therapist", comment: "")
let therapyConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "therapy", comment: "")
let selectHotel = LocalizationSystem.sharedInstance.localizedStringForKey(key: "SelectHotel", comment: "")
let enter = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")
let firstNameConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "FirstNamePH", comment: "")
let lastNameConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "LastNamePH", comment: "")
let emailConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "EmailPH", comment: "")
let mobileConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "MobileNoPH", comment: "")
let birthdateConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "BirthDatePH", comment: "")
let idproofConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "IDProofPH", comment: "")
let validConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "valid", comment: "")
let photoCloseUpConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "PhotoCloseUp", comment: "")
let videofilenotAccepted = LocalizationSystem.sharedInstance.localizedStringForKey(key: "videofilenotAccepted", comment: "")
let numberConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "number", comment: "")
let submitConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Submit", comment: "")
let appointmentConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "appointment", comment: "")
let timeConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "time", comment: "")
let dateConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "date", comment: "")
let guestnameConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "GuestNamePH", comment: "")
let newConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "New", comment: "")
let createdConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "created", comment: "")
let codeConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "code", comment: "")
let cancelConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Cancel", comment: "")
let billingDoneConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "done", comment: "")
let singleConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Single", comment: "")
let thankyouConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "thankyou", comment: "")
let usernameConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "username", comment: "")
let passwordConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "password", comment: "")
let loginConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Login", comment: "")

