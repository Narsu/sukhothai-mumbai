//
//  ApiManager.swift
//  Team KT
//
//  Created by kesarimac2 on 01/06/17.
//  Copyright © 2017 kesarimac2. All rights reserved.
//

import Foundation
import Alamofire
import PKHUD
class ApiManager: NSObject {
    private override init() {
        
    }
    static let shared = ApiManager()
    func requestApiWithDataType(methodType:String,urlString:String,parameters:[String:Any]? = nil,isBody:Bool? = false,completionHandeler: @escaping (Any?,Bool?,NSError?) -> Void ){
        var headers = [String:String]()
        if !urlString.contains("app/login") {
            headers["Authorization"] = "JWT \(String(describing: getUserdefaultDataForKey(Key: TOKEN) ?? ""))"
        }
        
        //        if urlString.contains("searchAirlineWithOneWayFare")
        //        {
        //            headers["Content-Type"] = "x-www-form-urlencoded"
        //        }
        //        else
        //        {
        headers["Content-Type"] = "application/json"
        //        }
        print("......")
        print(headers)
        print(parameters ?? " ")
        print(urlString)
        switch methodType
        {
        case GET:
            Alamofire.request(urlString, method: .get, parameters: parameters, encoding: JSONEncoding.default,headers: headers)
                .responseString { response in
                    debugPrint(response)
                    switch response.result
                    {
                    case .success(let JSON) :
                        if response.response?.statusCode == 200 {
                            
                            completionHandeler(JSON,response.result.isSuccess ,nil)
                        } else {
                            completionHandeler(JSON,false,nil)
                            
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                        PKHUD.sharedHUD.contentView = PKHUDTextView(text: error.localizedDescription as String)
                        PKHUD.sharedHUD.show()
                        PKHUD.sharedHUD.hide(afterDelay: 1.0)
                        PKHUD.sharedHUD.hide(afterDelay: 1.0, completion: { (complet) in
                            //completionHandeler(nil,response.result.isSuccess ,error as NSError)
                        })
                        
                    }
            }
            break
        case POST:
            if isBody == true{
            }
            else
            {
                Alamofire.request(urlString, method:.post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseString(completionHandler: { response in
                    debugPrint(response)
                    switch response.result
                    {
                    case .success(let JSON) :
                        if response.response?.statusCode == 200
                        {
                            completionHandeler(JSON,response.result.isSuccess ,nil)
                        }
                        else
                        {
                            completionHandeler(JSON,false,nil)
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                        PKHUD.sharedHUD.contentView = PKHUDTextView(text: error.localizedDescription as String)
                        PKHUD.sharedHUD.show()
                        PKHUD.sharedHUD.hide(afterDelay: 1.0)
                        PKHUD.sharedHUD.hide(afterDelay: 1.0, completion: { (complet) in
                            completionHandeler(nil,response.result.isSuccess ,error as NSError)
                        })
                    }
                })
            }
            break
        case PUT:
            Alamofire.request(urlString, method:.put, parameters: parameters!, encoding: JSONEncoding.default, headers: headers).responseString(completionHandler: { response in
                // debugPrint(response)
                switch response.result
                {
                case .success(let JSON) :
                    if response.response?.statusCode == 200
                    {
                        completionHandeler(JSON,response.result.isSuccess ,nil)
                    }
                    else
                    {
                        completionHandeler(JSON,false,nil)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    PKHUD.sharedHUD.contentView = PKHUDTextView(text: error.localizedDescription as String)
                    PKHUD.sharedHUD.show()
                    PKHUD.sharedHUD.hide(afterDelay: 1.0)
                    PKHUD.sharedHUD.hide(afterDelay: 1.0, completion: { (complet) in
                        completionHandeler(nil,response.result.isSuccess ,error as NSError)
                    })
                }
            })
            
            break
        default :
            break
        }
    }
    
    
}

