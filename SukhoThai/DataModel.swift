//
//  DataModel.swift
//  SukhoThai
//
//  Created by Kesari on 21/08/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import Foundation
import RealmSwift

class NewBudget: Object {
    @objc dynamic var urlName = ""
    @objc dynamic var response = ""
    //    @objc dynamic var timerInterval = Date()
    override class func primaryKey() -> String? { return "urlName" }
}
