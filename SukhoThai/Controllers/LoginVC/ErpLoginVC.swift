//
//  ErpLoginVC.swift
//  SliderPoc
//
//  Created by Narsu's iMac on 14/06/19.
//  Copyright © 2019 kesarimac4. All rights reserved.
//

import UIKit
import PKHUD
protocol ErpLoginDelegate {
    func loginBtnClicked()
}

class ErpLoginVC: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var ibUserId: UITextField!
    @IBOutlet weak var ibPassword: UITextField!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var ibBranchCode: UITextField!
    var delegate:ErpLoginDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        animateView()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapClicked))
        self.view.addGestureRecognizer(tap)
        registerKeyboardNotifications()
    }
    @objc func tapClicked() {
        self.view.endEditing(true)
    }
    func setupView() {
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    //MARK: Keyboard setup
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height+40, right: 0)
        scrollview.contentInset = contentInsets
        scrollview.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollview.contentInset = UIEdgeInsets.zero
        scrollview.scrollIndicatorInsets = UIEdgeInsets.zero
        
    }
    
    @IBAction func login(_ sender: UIButton) {
        
        
        
        
        if (ibUserId.text?.isEmpty)! {
            showAlertMessage(message: "Enter User id")
        } else if (ibPassword.text?.isEmpty)! {
            showAlertMessage(message: "Enter Password")
        } else if (ibBranchCode.text?.isEmpty)!{
            showAlertMessage(message: "Enter Branch Code")
        }else {
            login()
        }
        
    }
    
    @IBAction func loginWithOtp(_ sender: UIButton) {
        
        if (ibUserId.text?.isEmpty)! {
            HUD.flash(HUDContentType.label("Enter User id"))
        } else {
            sendPassword()
        }
        
    }
    
    func showAlertForOTP() {
        let alertController = UIAlertController(title: "Password sent successfully.", message: "Password has been sent to your mobile no.Please login again.", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sendPassword() {
        if isConnectedToNetwork() {
            let parameter = ["RS_ID":ibUserId.text!]
            ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: getSendPasswordUrl(),parameters: parameter, isBody: false) { (response, isSuccess, error) in
                
                if isSuccess!  {
                    
                    let dict = convertStringToDictionary(json: response as! String)! as NSDictionary
                    if let str = dict["message"] as? String,str == "success" {
                        self.showAlertForOTP()
                    }
                } else {
                    HUD.flash(HUDContentType.label("Something went wrong."))
                }
            }
        } else {
            HUD.flash(HUDContentType.label("Internet connection not available."))
        }
    }
    
    func login() {
        if isConnectedToNetwork() {
            let parameter = ["username": ibUserId.text!,"password":ibPassword.text!, "branchCode" : ibBranchCode.text!] as [String : Any]
            ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: getLoginUrl(), parameters: parameter) { (response, isSuccess, error) in
                if isSuccess!  {
                    let dict = convertStringToDictionary(json: response as! String)! as NSDictionary
                    if let str = dict["message"] as? String,str == "successfully login" {
                        
                        let dictData = dict["data"] as? NSDictionary
                        let F21_FNAME = dictData?["F21_FNAME"] as? String ?? ""
                        let F21_MOBILE = dictData?["F21_MOBILE"] as? String ?? ""
                        let branchCode = dictData?["ST_BRN"] as? String ?? ""
                        let token = dictData?["token"] as? String ?? ""
                        createUserDefault(key: TOKEN, data: token)
                        createUserDefault(key: BRANCH, data: branchCode)
                        createUserDefault(key: RECEPTIONIST_FNAME, data: F21_FNAME)
                        createUserDefault(key: USER_MOBILE_NO, data: F21_MOBILE)
                        createUserDefault(key: IS_RECEPTION_LOGGED_IN, data: true)
                        createUserDefault(key: RECEPTIONIST_ID, data: Int(self.ibUserId.text!) ?? 0)
                        createUserDefault(key: RECEPTION_LOGGED_IN_DATETIME, data: Date())
                        createUserDefault(key: IS_USER_LOGGED_IN, data: true)
                        let tabController = TabBarController()
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = tabController
                        appDelegate.window?.makeKeyAndVisible()
                        
                    }
                } else {
//                    let dict = convertStringToDictionary(json: response) as? NSDictionary
//                    let message = dict["message"]
                    HUD.flash(HUDContentType.label("Something went wrong."))
                }
            }
        } else {
            HUD.flash(HUDContentType.label("Internet connection not available."))
        }
    }
}
