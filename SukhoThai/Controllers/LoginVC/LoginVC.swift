//
//  LoginVC.swift
//  SukhoThai
//
//  Created by kesarimac2 on 17/07/18.
//  Copyright © 2018 Kesari Tours. All rights reserved.
//

import UIKit
import FontAwesomeKit_Swift
import Alamofire
import PKHUD
class LoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var ibForgetPWOutlet: UIButton!
    @IBOutlet weak var ibRegisterOutlet: UIButton!
    @IBOutlet weak var ibLoginLbl: UILabel!
    @IBOutlet weak var ibUserImage: UIImageView!
    @IBOutlet weak var ibPasswordImage: UIImageView!
    @IBOutlet weak var ibUserName: UITextField!
    @IBOutlet weak var ibPassword: UITextField!
    @IBOutlet weak var ibLogin: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var ibQrView: UIView!
    @IBOutlet weak var segementControl: UISegmentedControl!
    @IBOutlet weak var ibBarCodeScanOutlet: UIButton!
    var button1 = UIBarButtonItem()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toRegister") {
            let vc = segue.destination as! RegisterVC
            vc.designation = "Owner"
            vc.hidePassField = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func updateLogin(){
        segementControl.setTitle("\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Owner-Manager", comment: ""))", forSegmentAt: 0)
        segementControl.setTitle("\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Therapist", comment: ""))", forSegmentAt: 1)
        ibLogin.setTitle("\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Login", comment: ""))", for: .normal)
        ibLoginLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Login", comment: "")
        
        ibUserName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "username", comment: "")
        ibPassword.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "password", comment: "")
        ibUserName.placeHolderColor = .lightText
        ibPassword.placeHolderColor = .lightText
        self.navigationItem.title = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Login", comment: "")
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
    }
    @objc func action(){
        
        let alertTitle = LocalizationSystem.sharedInstance.localizedStringForKey(key: "AlertTitle", comment: "")
        let alertMessage = LocalizationSystem.sharedInstance.localizedStringForKey(key: "AlertMessage", comment: "")
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "English", style: .default, handler: { (action) in
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")

            self.updateLogin()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let loginViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav = UINavigationController(rootViewController: loginViewController)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = nav
            nav.navigationBar.barTintColor = .darkText
            
            
        }))
        alert.addAction(UIAlertAction(title: "ไทย(Thai)", style: .default, handler: { (action) in
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "th")
           
//            // LoginVC
//            let nav = UINavigationController(nibName: "NavVC", bundle: nil)
            self.updateLogin()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let loginViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let nav = UINavigationController(rootViewController: loginViewController)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = nav
            nav.navigationBar.barTintColor = .darkText
            
        }))
        alert.addAction(UIAlertAction(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Cancel", comment: ""), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    override func viewDidAppear(_ animated: Bool) {
        button1 = UIBarButtonItem(image: UIImage(named: "googleTranslate1"), style: .plain, target: self, action: #selector(action))
        self.navigationItem.setRightBarButton(button1, animated: false)
        self.navigationController?.navigationBar.barTintColor = .darkText
        self.navigationItem.title = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Login", comment: "")
        registerKeyboardNotifications()
        updateLogin()
    }
    @IBAction func segementChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            ibQrView.isHidden = true
        } else {
            ibQrView.isHidden = false
        }
    }
    
    //MARK: Keyboard setup
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    @IBAction func unwindToHomeScreen(segue: UIStoryboardSegue) {
        dismiss(animated: true, completion: nil)
    }

    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    @objc func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height+50, right: 0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    @IBAction func loginPressed(_ sender: UIButton) {
        
        if isValidForm(){
            ProgressHud.progressHud(onview: self.view)
            login()
        }
        
    }
    @IBAction func forgetPassword(_ sender: UIButton) {
        
    }
    func isValidForm() -> Bool{
        var isValid = true
        

        if ibUserName.text!.isEmptyStr{
           showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "username", comment: ""))")
           // showAlertMessage(message: "\(enter) \(usernameConst)")
            isValid = false
        }
        else if ibPassword.text!.isEmptyStr{
            showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "password", comment: ""))")
           // showAlertMessage(message: "\(enter) \(passwordConst)")
            isValid = false
        }
        return isValid
    }
    func login() {
        
        let parameter:Parameters = ["mobileNo":ibUserName.text ?? "","password": ibPassword.text ?? ""]
        
        ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: urlPostLogin(),parameters: parameter as [String : Any],isBody: false, completionHandeler: { (response, isSuccess, error) in
            
            if isSuccess! {
                let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                
                let success = dict["success"] as? Bool ?? false
                if success {
                    
                    ProgressHud.hide(fromView: self.view)
                    let arrayOfTherapyCodes = NSMutableArray(array: dict["data"] as! NSArray ?? NSArray())
                    
                    print("array of therapy codes \(arrayOfTherapyCodes) ---")
                    
                    let tempDict = arrayOfTherapyCodes[0] as? NSDictionary ?? NSDictionary()
                    
                    let branchCode = tempDict["branchCode"] as? String ?? ""
                    
                    let branchName = tempDict["branchName"] as? String ?? ""
                    
                    let createdBy = tempDict["therapistId"] as? String ?? ""    // therapistID
                    
 //                   let printerIp = tempDict["printerIp"] as? String ?? ""
                    
                    print("branch code is \(branchCode) -- branch Name is \(branchName) created By \(createdBy)")
                    
                    createUserDefault(key: BRANCH, data: branchCode)
                    createUserDefault(key: "branchName", data: branchName)
                    createUserDefault(key: "createdBy", data: createdBy)
                    
                    
                let tabController = TabBarController()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = tabController
                    
               // UserDefaults.standard.set("1", forKey: "isLogged")
                createUserDefault(key: "logged", data: "isLogged")
                }
                else{
                    let err = error?.localizedDescription
                    let key = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Please Contact Administration.", comment: "")
                    showAlertMessage(message: err ?? "\(key)")
                    ProgressHud.hide(fromView: self.view)
                }
            } else {
                let errorString = String(describing: error)
                PKHUD.sharedHUD.contentView = PKHUDTextView(text: errorString)
                PKHUD.sharedHUD.show(onView: self.view)
                PKHUD.sharedHUD.hide(afterDelay: 1.0)
            }
        })
    }
}
