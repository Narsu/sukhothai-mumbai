//
//  PopoverVC.swift
//  Finsparc
//
//  Created by Narsu's iMac on 03/06/19.
//  Copyright © 2019 Narsu. All rights reserved.
//

import UIKit

protocol PopoverDelegate {
    func getSelectedPopoverData(selectedCategory:String)
}
class PopoverVC: UIViewController {
    
    var resultArray:[String]!
    @IBOutlet weak var ibTableView: UITableView!
    var delegate:PopoverDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        ibTableView.register(UINib(nibName: "PopoverCell", bundle: nil), forCellReuseIdentifier: "cell")
        ibTableView.tableFooterView = UIView()
        
    }
    
}
extension PopoverVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PopoverCell
           cell.ibTitleLable.text = resultArray[indexPath.row]
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            let returnTitle = self.resultArray[indexPath.row]
            self.delegate?.getSelectedPopoverData(selectedCategory: returnTitle)
        }
    }
    
}
