//
//  BookAppointmentVC.swift
//  SukhoThai Mumbai
//
//  Created by Narsu's iMac on 07/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit
import PKHUD
class BookAppointmentVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var ibSelectDayTimeCollectionView: UICollectionView!
    @IBOutlet weak var ibTherapyCollectionView: UICollectionView!
    @IBOutlet weak var ibTherapistCV: UICollectionView!
    @IBOutlet weak var ibCreatedAppointmentCV: UICollectionView!
    
    @IBOutlet weak var ibDatePickerView: UIDatePicker!
    @IBOutlet weak var ibTimePickerHeight: NSLayoutConstraint!
    @IBOutlet weak var ibTherapyCode: UILabel!
    @IBOutlet weak var ibTherapist: UILabel!
    @IBOutlet weak var ibTherapist2: UILabel!
    @IBOutlet weak var ibDate: UILabel!
    @IBOutlet weak var ibGuestName: UITextField!
    @IBOutlet weak var ibMobileNo: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    let SUKHO_GOLDEN = #colorLiteral(red: 0.9318091273, green: 0.7251556516, blue: 0.241263926, alpha: 1)
    let SUKHO_PINK = #colorLiteral(red: 0.9333333333, green: 0, blue: 0.5490196078, alpha: 1)
    var selectedDate = ""
    var selectedTherapist = ""
    var selectedDay = ""
    var selectedtime = ""
    var dateToPass = ""
    var selectedTherapyCodeIndex:Int!
    var isTwoTherapist = false
    var toolBar = UIToolbar()
    var selectedAppointmentDict = NSMutableDictionary()
    var selectedTherapistIndex = NSMutableArray()
    var arrayOfCreatedTherapy = NSMutableArray()
    var arrayOfTherapyList = NSMutableArray()
    var arrayOfTherapist = NSMutableArray()
    let colorsArray = [UIColor.red,.cyan,.blue,#colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1),.green,.orange,.yellow,.purple,.magenta]
    override func viewDidLoad() {
        super.viewDidLoad()
        registerKeyboardNotifications()
        getAllCreatedTherapy()
        getAllTherapyCode()
        getAllTherapist()
        setFormData()
        
        ibCreatedAppointmentCV.register(UINib(nibName: "CreatedTherapyCell", bundle: nil), forCellWithReuseIdentifier: "CreatedTherapyCell")
        ibCreatedAppointmentCV.register(UINib(nibName: "TwoTherapistTherapyCell", bundle: nil), forCellWithReuseIdentifier: "TwoTherapistTherapyCell")
        self.ibSelectDayTimeCollectionView.register(UINib(nibName: "DateTimeCell", bundle: nil), forCellWithReuseIdentifier: "DateTimeCell")
        self.ibTherapyCollectionView.register(UINib(nibName: "TherapyListCell", bundle: nil), forCellWithReuseIdentifier: "TherapyListCell")
        self.ibTherapistCV.register(UINib(nibName: "TherapistNameCell", bundle: nil), forCellWithReuseIdentifier: "TherapistNameCell")
        self.navigationController?.tabBarController?.tabBar.isHidden = true
        
        self.title = "Book Appointment"
        ibGuestName.delegate = self
        ibMobileNo.delegate = self
        ibDatePickerView.backgroundColor = .white
        ibDatePickerView.setDate(Date(), animated: true)
        addTootlbar(textField: ibMobileNo)
    }
    func setFormData() {
        print(selectedAppointmentDict)
        let F31_TCODE = selectedAppointmentDict["F31_TCODE"] as? String ?? ""
        let F31_IDNM1 = selectedAppointmentDict["F31_IDNM1"] as? String ?? ""
        let F31_IDNM2 = selectedAppointmentDict["F31_IDNM2"] as? String ?? ""
        let F22_TNOS = selectedAppointmentDict["F22_TNOS"] as? Int ?? 1
        let F31_NAME = selectedAppointmentDict["F31_NAME"] as? String ?? ""
        let F31_MOBILE = selectedAppointmentDict["F31_MOBILE"] as? String ?? ""
        let F31_APPTDT = selectedAppointmentDict["F31_APPTDT"] as? String ?? ""
        let F31_TIME1 = selectedAppointmentDict["F31_TIME1"] as? String ?? ""
        let day =  convertDateString(dateString: F31_APPTDT, dateFormat: "E")
        if day.isEmpty {
            ibDate.text = ""
        } else {
            ibDate.text = "\(day) @ \(F31_TIME1)"
        }
        let date =  convertDateString(dateString: F31_APPTDT, dateFormat: "d")
        dateToPass = convertDateString(dateString: F31_APPTDT, dateFormat: "dd/MM/yyyy")
        selectedDate = date
        ibGuestName.text = F31_NAME
        ibMobileNo.text = F31_MOBILE
        ibTherapyCode.text = F31_TCODE
        ibTherapist.text = F31_IDNM1
        ibTherapist2.text = F31_IDNM2
        if F22_TNOS == 2 {
            isTwoTherapist = true
        } else {
            isTwoTherapist = false
        }
        setupTherapyCode()
        setupTherapist()
       
        self.ibSelectDayTimeCollectionView.reloadData()
        self.ibCreatedAppointmentCV.reloadData()
    }
    
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height+40, right: 0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func getToolBar(){
        toolBar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.height - 230, width: self.view.frame.width, height: 40))
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.barTintColor = #colorLiteral(red: 0.8039215686, green: 0.5843137255, blue: 0.04705882353, alpha: 1)
        toolBar.backgroundColor = .black
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
    }
    func setupTherapyCode() {
        for i in 0 ..< self.arrayOfTherapyList.count {
            let dict1 = self.arrayOfTherapyList[i] as? NSDictionary
            let F31_TCODE = self.selectedAppointmentDict["F31_TCODE"] as? String ?? ""
            let F22_TCODE = dict1?["F22_TCODE"] as? String ?? ""
            if F31_TCODE == F22_TCODE  {
                self.selectedTherapyCodeIndex = 0
                self.arrayOfTherapyList.removeObject(at: i)
                self.arrayOfTherapyList.insert(dict1!, at: 0)
                return
            }
        }
        self.ibTherapyCollectionView.reloadData()
        
    }
    func setupTherapist() {
        self.selectedTherapistIndex.removeAllObjects()
        for i in 0 ..< self.arrayOfTherapist.count {
            let dict1 = self.arrayOfTherapist[i] as? NSDictionary
            let F31_IDNO1 = self.selectedAppointmentDict["F31_IDNO1"] as? Int ?? 0
            let F31_IDNO2 = self.selectedAppointmentDict["F31_IDNO2"] as? Int ?? 0
            let F22_TNOS = self.selectedAppointmentDict["F22_TNOS"] as? Int ?? 0
            let F21_IDNO = dict1?["F21_IDNO"] as? Int ?? 0
            if F21_IDNO == F31_IDNO1 || F21_IDNO == F31_IDNO2 {
                self.selectedTherapistIndex.add(i)
            }
            
        }
    }
    @objc func donePicker(){
        if selectedtime != ""{
            ibDate.text = "\(selectedDay) @ \(selectedtime)"
        } else {
            let time = getCurrentDateWithTime(dateFormat: "HH:mm")
            selectedtime = time
            ibDate.text = "\(selectedDay) @ \(time)"
        }
        toolBar.removeFromSuperview()
        ibTimePickerHeight.constant = 0
        self.view.endEditing(true)
    }
    @objc func cancelPicker(){
        toolBar.removeFromSuperview()
        ibTimePickerHeight.constant = 0
        self.view.endEditing(true)
    }
    func addTootlbar(textField:UITextField) {
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneWithNumberPad))]
        numberToolbar.sizeToFit()
        textField.inputAccessoryView = numberToolbar
        
    }
    
    @objc func cancelNumberPad() {
        self.view.endEditing(true)
    }
    @objc func doneWithNumberPad() {
        self.view.endEditing(true)
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if ibGuestName.text!.isEmpty {
            showAlertMessage(message: "Enter guest name")
        } else if ibMobileNo.text!.isEmpty {
            showAlertMessage(message: "Enter mobile number")
        }  else if !isValidPhone(phone: ibMobileNo.text!){
            showAlertMessage(message: "Enter valid mobile number")
        } else if isTwoTherapist && selectedTherapistIndex.count != 2{
            showAlertMessage(message: "Select two therapist")
        } else if !isTwoTherapist && selectedTherapistIndex.count != 1{
            showAlertMessage(message: "Select one therapist")
        } else {
            postCreateAppointment()
        }
        
    }
    @IBAction func datePickerValueChange(_ sender: UIDatePicker) {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        selectedtime = dateFormatter.string(from: sender.date)
    }
    func getAllCreatedTherapy(isRefresh:Bool=false) {
        if isConnectedToNetwork() {
            let branchcode = getUserdefaultDataForKey(Key: BRANCH) as! String
            let urlString = urlGetNotBookedTherapyCode(branchCode: branchcode)
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString, completionHandeler: { (response, isSuccess, error) in
                
                if isSuccess! {
                    let therapyCountLbl = LocalizationSystem.sharedInstance.localizedStringForKey(key: "TotalTherapy", comment: "")
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary?
                    self.arrayOfCreatedTherapy = NSMutableArray(array: dict?["result"] as? NSArray ?? NSArray())
                    self.ibCreatedAppointmentCV.reloadData()
                    if isRefresh {
                        if self.arrayOfCreatedTherapy.count > 0 {
                            let dict1 = self.arrayOfCreatedTherapy[0] as? NSDictionary
                            let dataArray = dict1?["DATA"] as? NSArray ?? NSArray()
                            let dict = dataArray[0] as? NSDictionary
                            self.selectedAppointmentDict = NSMutableDictionary(dictionary: dict ?? NSDictionary())
                            self.setFormData()
                        } else {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    PKHUD.sharedHUD.hide()
                } else {
                    let er = String(describing: error.debugDescription)
                    showAlertMessage(message: "\(requestTimeOutAlert) -----------\n\(er)")
                    PKHUD.sharedHUD.hide()
                }
            })
            
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    func getAllTherapyCode() {
        if isConnectedToNetwork() {
            let urlString = urlGetAllTherapyList()
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString, completionHandeler: { (response, isSuccess, error) in
                if isSuccess! {
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary?
                    self.arrayOfTherapyList = NSMutableArray(array: dict?["result"] as? NSArray ?? NSArray())
                    self.setupTherapyCode()
                    self.ibTherapyCollectionView.reloadData()
                    PKHUD.sharedHUD.hide()
                } else {
                    let er = String(describing: error.debugDescription)
                    print("The request timed out -----------\n\(er)")
                    showAlertMessage(message: "\(requestTimeOutAlert) -----------\n\(er)")
                    PKHUD.sharedHUD.hide()
                }
            })
            
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    func getAllTherapist() {
        if isConnectedToNetwork() {
            let branchcode = getUserdefaultDataForKey(Key: BRANCH) as! String
            let urlString = urlGetAllTherapist(branchCode: branchcode)
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString, completionHandeler: { (response, isSuccess, error) in
                if isSuccess! {
                    
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary?
                    let success = dict!["success"] as? Bool ?? false
                    if success {
                        let dataArray = NSMutableArray(array: dict?["data"] as? NSArray ?? NSArray())
                        let array = dataArray.filter {
                            ($0 as! NSDictionary)["ERP_______"] as! String != "Y"
                        }
                        self.arrayOfTherapist = NSMutableArray(array: array)
                        self.setupTherapist()
                        
                        self.ibTherapistCV.reloadData()
                        PKHUD.sharedHUD.hide()
                    }
                } else {
                    let er = String(describing: error.debugDescription)
                    print("The request timed out -----------\n\(er)")
                    showAlertMessage(message: "\(requestTimeOutAlert) -----------\n\(er)")
                    PKHUD.sharedHUD.hide()
                }
            })
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    
    func postCreateAppointment(){
        var selectedTherapyst = NSDictionary()
        var selectedTherapyst2 = NSDictionary()
        var arrSelectedTherapistForBusyFlag = NSMutableArray()
        if isTwoTherapist {
            selectedTherapyst = arrayOfTherapist[selectedTherapistIndex[0] as! Int] as? NSDictionary ?? NSDictionary()
            selectedTherapyst2 = arrayOfTherapist[selectedTherapistIndex[1] as! Int] as? NSDictionary ?? NSDictionary()
        } else {
            selectedTherapyst = arrayOfTherapist[selectedTherapistIndex[0] as! Int] as? NSDictionary ?? NSDictionary()
        }
        
        let selectedCode = arrayOfTherapyList[selectedTherapyCodeIndex] as? NSDictionary
        let firstName = selectedTherapyst["F21_FNAME"] as? String ?? ""
        let IMG_PHOTO = selectedTherapyst["IMG_PHOTO"] as? String ?? ""
        let F21_IDNO = selectedTherapyst["F21_IDNO"] as? Int ?? 0
        arrSelectedTherapistForBusyFlag.add(F21_IDNO)
        let firstName2 = selectedTherapyst2["F21_FNAME"] as? String ?? ""
        let IMG_PHOTO2 = selectedTherapyst2["IMG_PHOTO"] as? String ?? ""
        let id2 = selectedTherapyst2["F21_IDNO"] as? Int ?? 0
        
        let F22_HHMM = selectedCode?["F22_HHMM"] as? String ?? ""
        let therapyCode = selectedCode?["F22_TCODE"] as? String ?? ""
        let F22_POINT = selectedCode?["F22_POINT"] as? Double ?? 0
        let F22_PT1 = selectedCode?["F22_PT1"] as? Double ?? 0
        let F22_PT2 = selectedCode?["F22_PT2"] as? Double ?? 0
        let F22_TNOS = selectedCode?["F22_TNOS"] as? Int ?? 0
        let F22_TAMT1 = selectedCode?["therapyAmmount"] as? Int ?? 0
        let id = selectedAppointmentDict["_id"] as? String ?? ""
        let F31_NAME = selectedAppointmentDict["F31_NAME"] as? String ?? ""
        let F31_TRANNO = selectedAppointmentDict["F31_TRANNO"] as? Int ?? 0
        let groupNumber = selectedAppointmentDict["groupNumber"] as? String ?? ""
        
        let branchCode = getUserdefaultDataForKey(Key: BRANCH) as? String ?? ""
        let urlString = urlPutBookAppointment()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let date2 = formatter.string(from: Date())
        dateToPass = date2
        let time = getCurrentDateWithTime(dateFormat: "HH:mm")
        selectedtime = time
        var parameter = [
            "_id":id,
            "F31_NAME" : ibGuestName.text!,
            "F31_MOBILE":ibMobileNo.text!,
            "F31_NOPER" : 1,
            "F31_APPTDT":dateToPass,
            "F31_TIME1" : selectedtime,
            "ST_BRN" : branchCode,
            "F31_TCODE":therapyCode,
            "F31_IDNO1":F21_IDNO,
            "F31_IDNM1":firstName,
            "IMG_PHOTO":IMG_PHOTO,
            "F31_POINT":F22_POINT,
            "F31_PT1":F22_PT1,
            "F31_PT2":F22_PT2,
            "F22_HHMM":F22_HHMM,
            "F22_TNOS":F22_TNOS,
            "F31_AMT":F22_TAMT1,
            "F31_TOTAL":F22_TAMT1,
            "entryFrom" : "receptionApp"] as [String : Any]
        if isTwoTherapist {
            parameter["F31_IDNO2"] = id2
            parameter["F31_IDNM2"] = firstName2
            parameter["IMG_PHOTO1"] = IMG_PHOTO2
            arrSelectedTherapistForBusyFlag.add(id2)
        }
        if groupNumber == "0" {
            parameter["groupNumber"] = "\(F31_TRANNO)"
        }
        if F31_NAME.isEmpty {
            parameter["KT_SYSLOG"] = "ADD"
        } else {
            parameter["KT_SYSLOG"] = "UPD"
        }
        
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType : PUT, urlString : urlString, parameters : parameter, isBody :  false) { (response, isSuccess, error) in
                if isSuccess!{
                    let arrayOfSelectedAppointmentTherapistIds = NSMutableArray()
                    if !F31_NAME.isEmpty {
                        let F22_TNOS = self.selectedAppointmentDict["F22_TNOS"] as? Int ?? 0
                        let F31_IDNO1 = self.selectedAppointmentDict["F31_IDNO1"] as? Int ?? 0
                        if F22_TNOS == 2 {
                            let F31_IDNO2 = self.selectedAppointmentDict["F31_IDNO2"] as? Int ?? 0
                            arrayOfSelectedAppointmentTherapistIds.add([F31_IDNO1,F31_IDNO2])
                        } else {
                            arrayOfSelectedAppointmentTherapistIds.add(F31_IDNO1)
                        }
                        self.updateBusyFlagToTherapist(arrOfThp: arrayOfSelectedAppointmentTherapistIds,isBusy: false)
                    }
                    
                    self.updateBusyFlagToTherapist(arrOfThp: arrSelectedTherapistForBusyFlag,isBusy: true)
                    
                } else {
                    let errorString = String(describing: error)
                    PKHUD.sharedHUD.contentView = PKHUDTextView(text: errorString)
                    PKHUD.sharedHUD.hide(afterDelay: 1.0)
                }
                
            }
        } else{
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    
    func updateBusyFlagToTherapist(arrOfThp:NSMutableArray,isBusy:Bool) {
        if isConnectedToNetwork() {
            let urlString = urlUpdateThPBusyFlag()
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            let parameter = ["thArr":arrOfThp,
                             "isBusy":isBusy] as [String : Any]
            ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: urlString,parameters: parameter, completionHandeler: { (response, isSuccess, error) in
                if isSuccess! {
                    
                    PKHUD.sharedHUD.contentView = PKHUDTextView(text: "Appointment booked successfully.")
                    PKHUD.sharedHUD.show()
                    PKHUD.sharedHUD.hide(afterDelay: 2) { (success) in
                        self.getAllCreatedTherapy(isRefresh: true)
                        self.getAllTherapist()
                    }
                } else {
                    let er = String(describing: error.debugDescription)
                    print("The request timed out -----------\n\(er)")
                    showAlertMessage(message: "\(requestTimeOutAlert) -----------\n\(er)")
                    PKHUD.sharedHUD.hide()
                }
            })
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
}

extension BookAppointmentVC : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == ibCreatedAppointmentCV{
            let dict1 = arrayOfCreatedTherapy[indexPath.section] as? NSDictionary
            let dataArray = dict1?["DATA"] as? NSArray ?? NSArray()
            let dict = dataArray[indexPath.row] as? NSDictionary
            let F22_TNOS = dict?["F22_TNOS"] as? Int ?? 0
            if F22_TNOS == 2 {
                return CGSize(width: 200, height: 100)
            } else {
                return CGSize(width: 100, height: 100)
            }
        } else  if collectionView == ibSelectDayTimeCollectionView{
            return CGSize(width: 66.0, height: 66.0)
        } else if collectionView == ibTherapyCollectionView{
            return CGSize(width: 132.0, height: 66.0)
        } else if collectionView == ibTherapistCV{
            let dict = arrayOfTherapist[indexPath.row] as? NSDictionary ?? NSDictionary()
            let labeltext = UILabel()
            labeltext.font = UIFont.systemFont(ofSize: 16)
            labeltext.text = dict["F21_FNAME"] as? String ?? ""
            labeltext.sizeToFit()
            return CGSize(width: labeltext.frame.size.width + 15, height: 35)
        }
        return CGSize(width: 110.0, height: 40.0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: 5, height: 100)
    }
}
extension BookAppointmentVC : UICollectionViewDataSource, UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == ibCreatedAppointmentCV{
            return arrayOfCreatedTherapy.count
        } else {
            return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == ibCreatedAppointmentCV{
            let dict = arrayOfCreatedTherapy[section] as? NSDictionary
            let dataArray = dict?["DATA"] as? NSArray ?? NSArray()
            return dataArray.count
        } else  if collectionView == ibSelectDayTimeCollectionView{
            return 10
        } else if collectionView == ibTherapyCollectionView{
            return arrayOfTherapyList.count
        } else if collectionView == ibTherapistCV{
            return arrayOfTherapist.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == ibCreatedAppointmentCV){
            
            let dict1 = arrayOfCreatedTherapy[indexPath.section] as? NSDictionary
            let dataArray = dict1?["DATA"] as? NSArray ?? NSArray()
            let dict = dataArray[indexPath.row] as? NSDictionary
            let idDict = dict1?["_id"] as? NSDictionary
            let F22_TNOS = dict?["F22_TNOS"] as? Int ?? 0
            if F22_TNOS == 2 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoTherapistTherapyCell", for: indexPath) as! TwoTherapistTherapyCell
                cell.setCellData(dict: dict)
                if dict == selectedAppointmentDict {
                    cell.ibTherapyCode.backgroundColor = SUKHO_PINK
                } else {
                    cell.ibTherapyCode.backgroundColor = SUKHO_GOLDEN
                }
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreatedTherapyCell", for: indexPath) as! CreatedTherapyCell
                cell.setCellData(dict: dict)
                
                if dict == selectedAppointmentDict {
                    cell.ibTherapyCode.backgroundColor = SUKHO_PINK
                } else {
                    cell.ibTherapyCode.backgroundColor = SUKHO_GOLDEN
                }
                return cell
            }
            
        } else if (collectionView == ibSelectDayTimeCollectionView){
            let dateCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateTimeCell", for: indexPath) as! DateTimeCell
            
            let curdate =  Calendar.current.date(byAdding: .day, value: indexPath.row, to: Date())
            let formatter = DateFormatter()
            formatter.dateFormat = "E"
            let day = formatter.string(from: curdate!)
            dateCell.ibDay.text = day
            formatter.dateFormat = "d"
            let date = formatter.string(from: curdate!)
            dateCell.ibDate.text = date
            
            if selectedDate == date{
                dateCell.ibDay.textColor = .white
                dateCell.ibDate.textColor = .black
                dateCell.backgroundColor = SUKHO_PINK
            } else{
                dateCell.ibDay.textColor = .black
                dateCell.ibDate.textColor = .white
                dateCell.backgroundColor = SUKHO_GOLDEN
            }
            return dateCell
        } else if collectionView == ibTherapyCollectionView{
            let therapyCell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "TherapyListCell", for: indexPath) as! TherapyListCell
            let therapyDict = arrayOfTherapyList[indexPath.row] as? NSMutableDictionary ?? NSMutableDictionary()
            //            let F22_TCODE = therapyDict["F22_TCODE"] as? String ?? ""
            //            therapyCell1.ibTherapyCode.text = F22_TCODE
            //            therapyCell1.ibTherapyName.text = therapyDict["F22_TNAME"] as? String ?? ""
            therapyCell1.setCellData(dict: therapyDict)
            if indexPath.row == selectedTherapyCodeIndex {
                therapyCell1.ibTherapyName.textColor = #colorLiteral(red: 0.07959023717, green: 0.01960784314, blue: 0.1566566781, alpha: 1)
                therapyCell1.ibTherapyCode.textColor = #colorLiteral(red: 0.07959023717, green: 0.01960784314, blue: 0.1566566781, alpha: 1)
                therapyCell1.backgroundColor = SUKHO_PINK
            } else {
                therapyCell1.ibTherapyName.textColor = #colorLiteral(red: 0.9176470588, green: 0.6784313725, blue: 0.3019607843, alpha: 1)
                therapyCell1.ibTherapyCode.textColor = #colorLiteral(red: 0.9176470588, green: 0.6784313725, blue: 0.3019607843, alpha: 1)
                therapyCell1.backgroundColor = #colorLiteral(red: 0.07959023717, green: 0.01960784314, blue: 0.1566566781, alpha: 1)
            }
            
            return therapyCell1
        } else if collectionView == ibTherapistCV{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TherapistNameCell", for: indexPath) as! TherapistNameCell
            let dict = arrayOfTherapist[indexPath.row] as? NSDictionary ?? NSDictionary()
            cell.ibCityLbl.font = UIFont.boldSystemFont(ofSize: 14)
            
            if selectedTherapistIndex.contains(indexPath.row)  {
                cell.ibCityLbl.textColor = SUKHO_PINK
            } else {
                if "M" == dict["F21_MF"] as? String{
                    cell.ibCityLbl.textColor = .black
                } else {
                    cell.ibCityLbl.textColor = .lightGray
                }
            }
            cell.ibCityLbl.text = dict["F21_FNAME"] as? String ?? ""
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        if collectionView == ibCreatedAppointmentCV {
            let dict1 = arrayOfCreatedTherapy[indexPath.section] as? NSDictionary
            let dataArray = dict1?["DATA"] as? NSArray ?? NSArray()
            let dict = dataArray[indexPath.row] as? NSDictionary
            selectedAppointmentDict = NSMutableDictionary(dictionary: dict ?? NSDictionary())
            setFormData()
        } else  if collectionView == ibSelectDayTimeCollectionView{
            
            let curdate =  Calendar.current.date(byAdding: .day, value: indexPath.row, to: Date())
            let formatter = DateFormatter()
            formatter.dateFormat = "d"
            let date = formatter.string(from: curdate!)
            selectedDate = date
            formatter.dateFormat = "dd/MM/yyyy"
            let date2 = formatter.string(from: curdate!)
            dateToPass = date2
            
            formatter.dateFormat = "E"
            let day = formatter.string(from: curdate!)
            selectedDay = day
            
            ibTimePickerHeight.constant = 230
            getToolBar()
            selectedtime = ""
            self.view.addSubview(toolBar)
            ibSelectDayTimeCollectionView.reloadData()
        } else if collectionView == ibTherapyCollectionView || collectionView == ibTherapistCV{
            if collectionView == ibTherapyCollectionView {
                let dict = arrayOfTherapyList[indexPath.row] as? NSDictionary
                let F22_TNOS = dict?["F22_TNOS"] as? Int ?? 0
                selectedTherapyCodeIndex = indexPath.row
                if F22_TNOS >= 2 {
                    isTwoTherapist = true
                } else {
                    isTwoTherapist = false
                }
                ibTherapyCode.text = dict?["F22_TCODE"] as? String ?? ""
            } else {
                if isTwoTherapist {
                    if selectedTherapistIndex.contains(indexPath.row) {
                        selectedTherapistIndex.remove(indexPath.row)
                    } else  if selectedTherapistIndex.count < 2 {
                        selectedTherapistIndex.add(indexPath.row)
                    }
                    if selectedTherapistIndex.count > 0 {
                        let dict = arrayOfTherapist[selectedTherapistIndex[0] as! Int] as? NSDictionary ?? NSDictionary()
                        self.ibTherapist.text = dict["F21_FNAME"] as? String ?? ""
                        self.ibTherapist2.text = ""
                    }
                    if selectedTherapistIndex.count > 1 {
                        let dict1 = arrayOfTherapist[selectedTherapistIndex[1] as! Int] as? NSDictionary ?? NSDictionary()
                        self.ibTherapist2.text = dict1["F21_FNAME"] as? String ?? ""
                    }
                } else {
                    let dict = arrayOfTherapist[indexPath.row] as? NSDictionary ?? NSDictionary()
                    self.ibTherapist.text = dict["F21_FNAME"] as? String ?? ""
                    selectedTherapistIndex.removeAllObjects()
                    if selectedTherapistIndex.count < 1 {
                        selectedTherapistIndex.add(indexPath.row)
                    }
                }
            }
            collectionView.reloadData()
        }
    }
}


