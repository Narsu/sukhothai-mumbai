//
//  TherapyCell.swift
//  Sukhothai Management
//
//  Created by Kesari on 12/08/19.
//  Copyright © 2019 Kesari. All rights reserved.
//

import UIKit
import MarqueeLabel
class TherapyCell: UICollectionViewCell {

    @IBOutlet weak var ibTherapyCode: UILabel!
    @IBOutlet weak var ibTherapyName: MarqueeLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ibTherapyName.marqueeType = .MLContinuous
        ibTherapyName.scrollDuration = 15.0
    }

}
