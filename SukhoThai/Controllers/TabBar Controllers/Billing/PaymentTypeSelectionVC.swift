//
//  PaymentTypeSelectionVC.swift
//  SukhoThai Mumbai
//
//  Created by Narsu's iMac on 10/12/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

enum PaymentType:String {
    case CASH = "CASH"
    case CrCard = "Credit Card"
    case WALLET = "Wallet"
    case GIFTCARD = "Gift Card"
}
protocol PaymentTypeDelegate {
    func paymentTypeSelected(paymentType:PaymentType)
}
class PaymentTypeSelectionVC: UIViewController {

    @IBOutlet weak var ibTableView: UITableView!
    var delegate:PaymentTypeDelegate?
   private var paymentTypeArray = [PaymentType]()
    var selectedPaymentTypeArray = [PaymentType]()
    override func viewDidLoad() {
        super.viewDidLoad()

        ibTableView.register(UINib(nibName: "PaymentTypeCell", bundle: nil), forCellReuseIdentifier: "PaymentTypeCell")
        paymentTypeArray = [.CASH,.CrCard,.GIFTCARD,.WALLET]
    }

}
extension PaymentTypeSelectionVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentTypeArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTypeCell", for: indexPath) as! PaymentTypeCell
        if selectedPaymentTypeArray.contains(paymentTypeArray[indexPath.row]) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        cell.ibTitle.text = paymentTypeArray[indexPath.row].rawValue
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async
        {
        self.delegate?.paymentTypeSelected(paymentType: self.paymentTypeArray[indexPath.row])
        self.dismiss(animated: true, completion: nil)
        
        }
    }
}
