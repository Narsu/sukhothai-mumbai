//
//  ReceiptVC.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 07/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

import PKHUD
class ReceiptVC: UIViewController,PopoverDelegate {
    
    var TotalAmount = 0
    let cellHeight : CGFloat = 56.5
    var giftAmt1 = 0
    var giftAmt2 = 0
    var creditCardAmt = 0
    var cashAmt = 0
    var walletAmount = 0
    var netAmountAfterCalculation = 0
    var isFromGiftCard1 = false
    var giftCardNo1 = ""
    var giftCardNo2 = ""
    var arrayOfReceiptData = NSMutableArray()
    var currentTextfield = UITextField()
    var paymentType = "Cash"
    var isGiftCardApiHit = false
    @IBOutlet weak var ibTotal: UILabel!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var ibGiftCard1: UITextField!
    @IBOutlet weak var ibGiftCard2: UITextField!
    @IBOutlet weak var ibCash: UITextField!
    @IBOutlet weak var ibCreditCard: UITextField!
    @IBOutlet weak var ibGiftAmount: UITextField!
    @IBOutlet weak var ibWalletAmt: UITextField!
    @IBOutlet weak var ibWalletTranNo: UITextField!
    
    @IBOutlet weak var ibTableHeight: NSLayoutConstraint!
    @IBOutlet weak var ibAvaillableAmtGftCard1: UILabel!
    @IBOutlet weak var ibAvaillableAmtGftCard2: UILabel!
    @IBOutlet weak var ibTableView: UITableView!
    @IBOutlet weak var ibWalletTypeBtn: UIButton!
    @IBOutlet weak var ibQrScanBtn: UIButton!
    @IBOutlet weak var ibAlertLbl: UILabel!
    @IBOutlet weak var ibToBeCollectedAmt: UILabel!
    @IBOutlet weak var ibToBeCollectedLbl: UILabel!
    @IBOutlet weak var ibGiftCardOwnerName: UILabel!
    @IBOutlet weak var ibGiftCardType: UILabel!
    var selectedAppointmentIndex = NSMutableArray()
    
    @IBOutlet weak var ibWalletHeight: NSLayoutConstraint!
    @IBOutlet weak var ibCashHeight: NSLayoutConstraint!
    @IBOutlet weak var ibCrCardHeight: NSLayoutConstraint!
    @IBOutlet weak var ibGiftCardHeight: NSLayoutConstraint!
    @IBOutlet weak var ibGiftCard2Height: NSLayoutConstraint!
    
    
    var arrayOfSelectedPaymentMethod = [PaymentType]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkPrinterAvailability()
        ibTotal.text = "\(TotalAmount)"
        ibToBeCollectedAmt.text = "\(TotalAmount)"
        ibCash.text = "\(TotalAmount)"
        self.title = "BILLING"
        registerKeyboardNotifications()
        ibTableView.register(UINib(nibName: "ReceiptCell", bundle: nil), forCellReuseIdentifier: "cell")
        if arrayOfReceiptData.count < 4{
            ibTableHeight.constant = CGFloat(cellHeight * CGFloat(arrayOfReceiptData.count))
        }else{
            ibTableHeight.constant = CGFloat(cellHeight * 4) + 10.0
        }
        for i in 0..<arrayOfReceiptData.count {
            selectedAppointmentIndex.add(i)
        }
        ibWalletTypeBtn.setTitle("PAYTM", for: .normal)
        ibWalletHeight.constant = 0
        ibCashHeight.constant = 0
        ibCrCardHeight.constant = 0
        ibGiftCardHeight.constant = 0
        ibGiftCard2Height.constant = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK: Keyboard setup
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height+40, right: 0)
        scrollview.contentInset = contentInsets
        scrollview.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollview.contentInset = UIEdgeInsets.zero
        scrollview.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func validateGiftCard(cardNo:String,isCard1:Bool){
        
        let urlString = urlGetGiftCards(number: cardNo)
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString) { (response, isSuccess, error) in
                if isSuccess!{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    if let balance = dict["balanceRemain"] as? Int,!(balance <= 0) {
                        let F26_TONAME = dict["F26_TONAME"] as? String ?? ""
                        let F26_GIFTTY = dict["F26_GIFTTY"] as? String ?? ""
                        self.ibGiftCardOwnerName.text = F26_TONAME
                        self.ibGiftCardType.text = F26_GIFTTY
                        
                        if isCard1 {
                            self.giftAmt1 = balance
                            self.ibAvaillableAmtGftCard1.text = "Balance :\(balance)"
                        } else {
                            self.giftAmt2 = balance
                            self.ibAvaillableAmtGftCard2.text = "Balance :\(balance)"
                        }
                        self.calculateGiftCard()
                    } else {
                        let message = dict["message"] as? String ?? ""
                        showAlertMessage(message: message, duration: 2.0)
                    }
                    PKHUD.sharedHUD.hide()
                }else{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    let message = dict["message"] as? String ?? ""
                    // showAlertMessage(message: message, duration: 2.0)
                    self.alert(message: message, title: "")
                    let er = String(describing: error)
                    print("\(requestTimeOutAlert) -----------\n\(er)")
                }
                PKHUD.sharedHUD.hide()
            }
        }
        else{
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    
    func checkPrinterAvailability() {
        let urlString = urlGetPrinterStatus()
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString) { (response, isSuccess, error) in
                if isSuccess!{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    if let message = dict["message"] as? String,message == "Success" {
                        let dict1 = dict["result"] as? NSDictionary
                        if let ERP_PRTIP = dict1?["ERP_PRTIP"] as? String,!ERP_PRTIP.isEmpty{
                            self.ibAlertLbl.text = "Printer IP address:\(ERP_PRTIP)"
                        } else {
                            self.ibAlertLbl.text = "Printer is not configured for this branch."
                        }
                    }
                    PKHUD.sharedHUD.hide()
                }else{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    let message = dict["message"] as? String ?? ""
                    showAlertMessage(message: message, duration: 2.0)
                    //self.alert(message: message, title: "")
                    let er = String(describing: error)
                    print("\(requestTimeOutAlert) -----------\n\(er)")
                }
                PKHUD.sharedHUD.hide()
            }
        }
        else{
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    
    func calculateGiftCard() {
        
        let totalGiftCardAmt = giftAmt1+giftAmt2
        if totalGiftCardAmt > 0 && arrayOfSelectedPaymentMethod.contains(.GIFTCARD) {
            if totalGiftCardAmt > TotalAmount {
                ibToBeCollectedAmt.text = "0"
                self.ibCash.text = ""
                self.ibWalletAmt.text = ""
                ibCashHeight.constant = 0
                ibCrCardHeight.constant = 0
                ibWalletHeight.constant = 0
                cashAmt = 0
                creditCardAmt = 0
                walletAmount = 0
                
            } else {
                netAmountAfterCalculation = TotalAmount - totalGiftCardAmt
                setPayableAmtLbl(amt: netAmountAfterCalculation)
            }
            if giftAmt1 > 0 {
                self.ibGiftCard1.text = "\(TotalAmount)"
            } else {
                self.ibGiftCard2.text = "\(TotalAmount)"
            }
        } else {
            netAmountAfterCalculation = TotalAmount-cashAmt-creditCardAmt-walletAmount
            setPayableAmtLbl(amt: netAmountAfterCalculation)
        }
    }
    func setPayableAmtLbl(amt:Int) {
        ibToBeCollectedAmt.text = "\(amt)"
        if amt < 0 {
            ibToBeCollectedLbl.text = "To be refunded"
        } else {
            ibToBeCollectedLbl.text = "To be collected"
        }
    }
    
    func checkPaymentType() {
        if (giftAmt1 != 0 || giftAmt2 != 0)  {
            if ibCash.text != "0" || !ibCash.text!.isEmpty {
                paymentType = "Gift+Cash"
            }
            if creditCardAmt != 0 {
                paymentType = "Gift+Cash/CC" // need to check with RS
            }
            if (ibCash.text != "0" || !ibCash.text!.isEmpty) && creditCardAmt != 0 {
                paymentType = "Gift+Cash/CC"
            }
            
            if walletAmount != 0 {
                paymentType = "Walt+Gift"
            }
        } else {
            if ibCash.text != "0" || !ibCash.text!.isEmpty {
                paymentType =  "Cash"
                if creditCardAmt != 0 {
                    paymentType = "Cash+Cr.Card"
                }
                if walletAmount != 0 {
                    paymentType = "Walt+Cash"
                }
            } else {
                if creditCardAmt != 0 && walletAmount != 0 {
                    paymentType = "Walt+CC"
                } else {
                    paymentType = "Wallet"
                }
            }
        }
    }
    
    func getReceiptId(){
        
        checkPaymentType()
        let arrayOfids = NSMutableArray()
        let arrayOfTherapistIds = NSMutableArray()
        for i in selectedAppointmentIndex {
            let indexpath = i as! Int
            let dict = arrayOfReceiptData[indexpath] as? NSDictionary
            let id = dict?["F31_TRANNO"] as? Int ?? 0
            arrayOfids.add(id)
            let TNOS = dict?["F22_TNOS"] as? Int ?? 0
            if TNOS == 2 {
                let id1 = dict?["F31_IDNO1"] as? Int ?? 0
                let id2 = dict?["F31_IDNO2"] as? Int ?? 0
                arrayOfTherapistIds.add(id1)
                arrayOfTherapistIds.add(id2)
            } else {
                let id1 = dict?["F31_IDNO1"] as? Int ?? 0
                arrayOfTherapistIds.add(id1)
            }
        }
        let appointmentDict = arrayOfReceiptData[selectedAppointmentIndex[0] as! Int] as? NSDictionary
        let apptId = appointmentDict?["_id"] as? String ?? ""
        let F31_NAME = appointmentDict?["F31_NAME"] as? String ?? ""
        let F31_MOBILE = appointmentDict?["F31_MOBILE"] as? String ?? ""
        let F31_AMT = appointmentDict?["F31_AMT"] as? Int ?? 0
        let ST_BRN = appointmentDict?["ST_BRN"] as? String ?? ""
        let F31_TCODE = appointmentDict?["F31_TCODE"] as? String ?? ""
        let F31_TRANNO = appointmentDict?["F31_TRANNO"] as? Int ?? 0
        let BRN_CURCD = appointmentDict?["BRN_CURCD"] as? String ?? ""
        
        let parameter = [
            "F18_GFTSTR1":ibGiftCard1.text ?? "",
            "F18_GFTSTR2":ibGiftCard1.text ?? "",
            "F41_CASH":ibCash.text!,
            "F41_CCAMT":ibCreditCard.text ?? "",
            "WALLET_TYP":"PAYTM",
            "F41_WLAMT":ibWalletAmt.text ?? "",
            "WALLET_TNO":ibWalletTranNo.text ?? "",
            "giftVoucherAmt": self.giftAmt1+self.giftAmt2,
            "giftVoucherAmt1": self.giftAmt1,
            "giftVoucherAmt2": self.giftAmt2,
            "_id": apptId,
            "F31_NAME": F31_NAME,
            "F31_MOBILE": F31_MOBILE,
            "F22_TAMT1": F31_AMT,
            "F31_TCODE": F31_TCODE,
            "ST_BRN": ST_BRN,
            "F31_TRANNO": F31_TRANNO,
            "totalCost":TotalAmount,
            "F21_IDNO": getUserdefaultDataForKey(Key: RECEPTIONIST_ID) as? Int ?? 0,
            "BRN_CURCD":BRN_CURCD,
            "F41_AMT":TotalAmount,
            "F41_CCFLG":paymentType,
            "thpIDForPrint":arrayOfids,
            "entryFrom" : "receptionApp"
            ] as [String : Any]
        PKHUD.sharedHUD.contentView = PKHUDProgressView(title: "", subtitle: "Printing")
        PKHUD.sharedHUD.show()
        ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: urlAppointmentReceipt(),parameters: parameter as [String : Any]) { (response, isSuccess, error) in
            if isSuccess!{
                PKHUD.sharedHUD.hide()
                let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                if let issuccess = dict["success"] as? Bool, issuccess{
                    self.updateBusyFlagToTherapist(arrOfThp: arrayOfTherapistIds)
                    let alertController = UIAlertController(title: "Billing Completed", message: "", preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (success) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else{
                HUD.flash(HUDContentType.label("Something went wrong."))
            }
        }
    }
    
    func updateBusyFlagToTherapist(arrOfThp:NSMutableArray) {
        if isConnectedToNetwork() {
            let urlString = urlUpdateThPBusyFlag()
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            let parameter = ["thArr":arrOfThp,
                             "isBusy":false] as [String : Any]
            ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: urlString,parameters: parameter, completionHandeler: { (response, isSuccess, error) in
                if isSuccess! {
                    
                    
                } else {
                    let er = String(describing: error.debugDescription)
                    print("The request timed out -----------\n\(er)")
                    showAlertMessage(message: "\(requestTimeOutAlert) -----------\n\(er)")
                    PKHUD.sharedHUD.hide()
                }
            })
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    func setToolBar(textField:UITextField){
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(cancelClick))]
        numberToolbar.sizeToFit()
        textField.inputAccessoryView = numberToolbar
    }
    
    @objc func cancelClick(){
        self.view.endEditing(true)
    }
    
    @IBAction func qrCodeScanAction(_ sender: UIButton) {
        scanQRcard()
    }
    func scanQRcard() {
        if #available(iOS 10.0, *) {
            if giftCardNo1.isEmpty {
                isFromGiftCard1 = true
            } else if giftCardNo2.isEmpty {
                isFromGiftCard1 = false
            }
            
            let vc1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QRScannerController") as! QRScannerController
            vc1.delegate = self
            let navVC = UINavigationController(rootViewController: vc1)
            self.present(navVC, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func printReceiptAction(_ sender: UIButton) {
        if netAmountAfterCalculation > 0 {
            self.alert(message: "Please collect full payment", title: "Payment Error")
        } else {
            if !ibWalletAmt.text!.isEmpty && arrayOfSelectedPaymentMethod.contains(.WALLET) {
                if ibWalletTranNo.text!.isEmpty {
                    self.alert(message: "", title: "Enter Wallet Tran No")
                } else {
                    getReceiptId()
                }
            } else {
                getReceiptId()
            }
        }
    }
    @IBAction func selectPaymentMethodAction(_ sender: UIButton) {
        selectPaymentType()
    }
    
    func selectPaymentType(){
        let paymentTypeVC = PaymentTypeSelectionVC(nibName: "PaymentTypeSelectionVC", bundle: nil)
        paymentTypeVC.providesPresentationContextTransitionStyle = true
        paymentTypeVC.definesPresentationContext = true
        paymentTypeVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        paymentTypeVC.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        paymentTypeVC.selectedPaymentTypeArray = arrayOfSelectedPaymentMethod
        paymentTypeVC.delegate = self
        self.present(paymentTypeVC, animated: true, completion: nil)
    }
    
    @IBAction func selectWalletTypeAction(_ sender: UIButton) {
        let vc = PopoverVC(nibName: "PopoverVC", bundle: nil)
        vc.delegate = self
        
        vc.resultArray = ["PAYTM"]
        let height = 2 * 40
        let h = self.view.frame.height
        vc.preferredContentSize = CGSize(width: 200, height: min(height,Int(h)))
        vc.modalPresentationStyle = UIModalPresentationStyle.popover
        let popover: UIPopoverPresentationController = vc.popoverPresentationController!
        popover.sourceView = sender
        popover.sourceRect = sender.bounds
        popover.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func getSelectedPopoverData(selectedCategory: String) {
        ibWalletTypeBtn.setTitle(selectedCategory, for: .normal)
    }
}
extension ReceiptVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setToolBar(textField: textField)
        ibCash.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .allEditingEvents)
        ibCreditCard.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .allEditingEvents)
        ibWalletAmt.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .allEditingEvents)
        ibGiftCard1.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .allEditingEvents)
        ibGiftCard2.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .allEditingEvents)
        ibWalletAmt.addTarget(self, action: #selector(walletAmtEntered(textField:)), for: .allEditingEvents)
    }
    @objc func walletAmtEntered(textField: UITextField) {
        if textField.text!.count > 0 {
            ibWalletTranNo.isHidden = false
        } else {
            ibWalletTranNo.isHidden = true
        }
    }
    @objc func myTargetFunction(textField: UITextField) {
        
        if let ccAmt = ibCash.text as String?,!ccAmt.isEmpty{
            cashAmt = Int(ccAmt)!
        }
        
        if !ibCreditCard.text!.isEmpty{
            if let ccAmt = ibCreditCard.text as String?{
                creditCardAmt = Int(ccAmt)!
            }
        }else{
            creditCardAmt = 0
        }
        
        //MARK: TO GET WALLET AMOUNT
        if !ibWalletAmt.text!.isEmpty{
            if let walletAmt = ibWalletAmt.text as String?{
                walletAmount = Int(walletAmt)!
            }
            ibWalletTranNo.isEnabled = true
        } else {
            walletAmount = 0
            ibWalletTranNo.isEnabled = false
        }
        currentTextfield = textField
        //MARK: TO HIT API FOR GETTING GIFT CARD AMOUNT
        if !ibGiftCard1.text!.isEmpty || !ibGiftCard2.text!.isEmpty{
            if ibGiftCard1.text != ibGiftCard2.text{
                if currentTextfield == ibGiftCard1{
                    if ibGiftCard1.text?.count == 13{
                        if !isGiftCardApiHit {
                            isGiftCardApiHit = true
                            validateGiftCard(cardNo: ibGiftCard1.text!, isCard1: true)
                        }
                    } else {
                        isGiftCardApiHit = false
                        giftAmt1 = 0
                        self.ibAvaillableAmtGftCard1.text = ""
                    }
                } else if currentTextfield == ibGiftCard2{
                    if ibGiftCard2.text?.count == 13{
                        if !isGiftCardApiHit {
                            isGiftCardApiHit = true
                            validateGiftCard(cardNo: ibGiftCard2.text!, isCard1: false)
                        }
                    } else{
                        isGiftCardApiHit = false
                        giftAmt2 = 0
                        self.ibAvaillableAmtGftCard2.text = ""
                    }
                }
            } else{
                self.alert(message: "Same Gift Card Numbers!", title: "")
            }
        }
        calculateGiftCard()
    }
}
extension ReceiptVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfReceiptData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReceiptCell
        let tempDict = arrayOfReceiptData[indexPath.row] as? NSDictionary ?? NSDictionary()
        cell.setCellData(dict: tempDict)
        if selectedAppointmentIndex.contains(indexPath.row) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedAppointmentIndex.contains(indexPath.row) {
            if !(selectedAppointmentIndex.count <= 1){
                selectedAppointmentIndex.remove(indexPath.row)
            }
        } else {
            selectedAppointmentIndex.add(indexPath.row)
        }
        var amount = 0
        for index in selectedAppointmentIndex {
            let dict = arrayOfReceiptData[index as! Int] as? NSDictionary
            
            if let amount1 = dict?["F31_AMT"] as? Int{
                amount += amount1
            }
        }
        TotalAmount = amount
        ibTotal.text = "\(amount)"
        calculateGiftCard()
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}
extension ReceiptVC:QRscannerDelegate {
    func getQrScanData(giftCardNo: String) {
        print(giftCardNo)
        if giftCardNo.count == 13 {
            if isFromGiftCard1 || giftCardNo1 == giftCardNo {
                isFromGiftCard1 = true
                giftCardNo1 = giftCardNo
            } else {
                ibGiftCard2.isHidden = false
                ibGiftCardHeight.constant = 180
                ibGiftCard2Height.constant = 65
                giftCardNo2 = giftCardNo
            }
            validateGiftCard(cardNo: giftCardNo, isCard1: isFromGiftCard1)
        } else {
            self.alert(message: "Please use valid Gift Card", title: "Error")
        }
    }
}

extension ReceiptVC:UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}
extension ReceiptVC: PaymentTypeDelegate {
    func paymentTypeSelected(paymentType: PaymentType) {
        if PaymentType.GIFTCARD == paymentType && !arrayOfSelectedPaymentMethod.contains(PaymentType.GIFTCARD){
            scanQRcard()
        }
        if arrayOfSelectedPaymentMethod.contains(paymentType) {
            guard let i = arrayOfSelectedPaymentMethod.index(of: paymentType) else {return}
            arrayOfSelectedPaymentMethod.remove(at: i)
            
        } else {
            arrayOfSelectedPaymentMethod.append(paymentType)
        }
        
        if arrayOfSelectedPaymentMethod.contains(PaymentType.CASH) {
            ibCashHeight.constant = 45
        } else {
            ibCashHeight.constant = 0
        }
        if arrayOfSelectedPaymentMethod.contains(PaymentType.CrCard) {
            ibCrCardHeight.constant = 45
        } else {
            ibCrCardHeight.constant = 0
        }
        if arrayOfSelectedPaymentMethod.contains(PaymentType.WALLET) {
            ibWalletHeight.constant = 85
        } else {
            ibWalletHeight.constant = 0
        }
        if arrayOfSelectedPaymentMethod.contains(PaymentType.GIFTCARD) {
            ibGiftCardHeight.constant = 120
        } else {
            ibGiftCardHeight.constant = 0
        }
    }
}
