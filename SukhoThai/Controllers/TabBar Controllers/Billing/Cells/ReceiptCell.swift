//
//  ReceiptCell.swift
//  SukhoThai
//
//  Created by Kesari on 08/06/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class ReceiptCell: UITableViewCell {

    
    @IBOutlet weak var ibName: UILabel!
    @IBOutlet weak var ibMobile: UILabel!
    @IBOutlet weak var ibTherapy: UILabel!
    @IBOutlet weak var ibINR: UILabel!
    @IBOutlet weak var ibStatus: UILabel!
   
    func setCellData(dict : NSDictionary){
 
        if let name = dict["F31_NAME"] as? String{
            ibName.text = name.capitalizingFirstLetter()
        } else {
            ibName.text = ""
        }
        
        if let name = dict["F31_TCODE"] as? String{
            ibTherapy.text = "  \(name)"
        } else {
            ibTherapy.text = ""
        }
        if let name = dict["F31_AMT"] as? Int{
            ibINR.text = "\(name)"
        } else {
            ibINR.text = "0"
        }
        
    }
}
