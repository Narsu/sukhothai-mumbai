    //
    //  ReceiptViewController.swift
    //  SukhoThai
    //
    //  Created by Kesari on 06/06/19.
    //  Copyright © 2019 Kesari Tours. All rights reserved.
    //
    
    import UIKit
    import PKHUD
    
    class ReceiptViewController: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate {
        
        @IBOutlet weak var ibTableView: UITableView!
        
        var arrayOfNotPrintedData = NSMutableArray()
        var arrayOfAllData = NSMutableArray()
        var arrayOfReceiptData = NSMutableArray()
        var activeTextField = UITextField()
        var startDate = ""
        var endDate = ""
        var totalAmt = 0
        var currentAmt = 0
        var tableheight : CGFloat = 56.5
        var segmentControl : UISegmentedControl!
        override func viewDidLoad() {
            super.viewDidLoad()
            self.title = "BILLING"
            ibTableView.register(UINib(nibName: "OuterCell", bundle: nil), forCellReuseIdentifier: "OuterCell")
            let date = Date()
            let format = DateFormatter()
            format.dateFormat = "dd/MM/yyyy"
            let formattedDate = format.string(from: date)
            let endDateValue = Calendar.current.date(byAdding: .day, value: 10, to: Date())
        
            startDate = formattedDate
            endDate = format.string(from: endDateValue!)
            setSegementControl()
        }
        func setSegementControl() {
            let titles = ["TO BE BILLED", "BILLED"]
            segmentControl = UISegmentedControl(items: titles)
            segmentControl.tintColor = #colorLiteral(red: 0.9176470588, green: 0.6784313725, blue: 0.3019607843, alpha: 1)
            segmentControl.borderColor = #colorLiteral(red: 0.9176470588, green: 0.6784313725, blue: 0.3019607843, alpha: 1)
            segmentControl.borderWidth = 1
            segmentControl.cornerRadius = 3
            segmentControl.backgroundColor = UIColor.white
            segmentControl.selectedSegmentIndex = 0
            for index in 0...titles.count-1 {
                segmentControl.setWidth(120, forSegmentAt: index)
            }
            segmentControl.sizeToFit()
            segmentControl.addTarget(self, action: #selector(segmentChanged(sender:)), for: .valueChanged)
            segmentControl.selectedSegmentIndex = 0
            segmentControl.sendActions(for: .valueChanged)
            navigationItem.titleView = segmentControl
        }
        @objc func segmentChanged(sender:UISegmentedControl) {
            if sender.selectedSegmentIndex == 0 {
                if arrayOfNotPrintedData.count > 0 {
                    self.arrayOfReceiptData = self.arrayOfNotPrintedData
                    ibTableView.reloadData()
                } else {
                    getReceiptByPost()
                }
            } else {
                if arrayOfAllData.count > 0 {
                    self.arrayOfReceiptData = self.arrayOfAllData
                    ibTableView.reloadData()
                } else {
                    getReceiptByPost()
                }
            }
        }
        override func viewWillAppear(_ animated: Bool) {
            self.tabBarController?.tabBar.isHidden = false
            self.arrayOfAllData.removeAllObjects()
            self.arrayOfNotPrintedData.removeAllObjects()
            getReceiptByPost()
        }
        
        func getReceiptByPost(){
            let urlString = urlPostReceiptByDate()
            let branchCode = getUserdefaultDataForKey(Key: BRANCH) as? String ?? ""
            if isConnectedToNetwork(){
                PKHUD.sharedHUD.contentView = PKHUDProgressView()
                PKHUD.sharedHUD.show(onView: self.view)
                var arrayOfReceipt = [String]()
                if segmentControl.selectedSegmentIndex == 0 {
                    arrayOfReceipt = ["N"]
                } else {
                    arrayOfReceipt = ["Y"]
                }
                let parameter = ["fromDate" : startDate, "toDate" : endDate, "branchCode" : "\(branchCode)","isReceiptGenerated":arrayOfReceipt] as [String : Any]
                ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: urlString, parameters: parameter, isBody: false) { (response, isSuccess, error) in
                    PKHUD.sharedHUD.hide()
                    if isSuccess!{
                        let dict = convertStringToDictionary(json: response as! String)
                        if let result = dict!["success"] as? Bool, result{
                            
                            if self.segmentControl.selectedSegmentIndex == 0 {
                                self.arrayOfNotPrintedData = NSMutableArray(array: dict?["result"] as? NSArray ?? NSArray())
                                self.arrayOfReceiptData = self.arrayOfNotPrintedData
                            } else {
                                self.arrayOfAllData = NSMutableArray(array: dict?["result"] as? NSArray ?? NSArray())
                                self.arrayOfReceiptData = self.arrayOfAllData
                            }
                            if self.arrayOfReceiptData.count == 0{
//                                showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "NoDataAvailable", comment: ""))", duration: 1.0)
                            }
                            self.ibTableView.reloadData()
                        }
                    } else{
                        PKHUD.sharedHUD.hide()
                        let er = String(describing: error)
                        print("\(requestTimeOutAlert) -----------\n\(er)")
                    }
                }
            } else {
                PKHUD.sharedHUD.hide()
                showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
            }
        }
        
    }
    
    extension ReceiptViewController : UITableViewDelegate, UITableViewDataSource{
        
        func numberOfSections(in tableView: UITableView) -> Int {
            if tableView.tag == 111 {
                return arrayOfReceiptData.count
            } else {
                return 1
            }
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if tableView.tag == 111 {
                return 1
            } else {
                let tempDict = arrayOfReceiptData[tableView.tag] as? NSDictionary ?? NSDictionary()
                let dataArray = tempDict["DATA"] as? NSArray ?? NSArray()
                return dataArray.count
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if tableView.tag == 111 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OuterCell", for: indexPath) as! OuterCell
                cell.ibTableView.isUserInteractionEnabled = false
                cell.ibTableView.tag = indexPath.section
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReceiptCell
                let tempDict = arrayOfReceiptData[tableView.tag] as? NSDictionary ?? NSDictionary()
                let dataArray = tempDict["DATA"] as? NSArray ?? NSArray()
                let dataDict = dataArray[indexPath.row] as? NSDictionary ?? NSDictionary()
                cell.setCellData(dict: dataDict)
                return cell
            }
        }
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            if let cell = cell as? OuterCell {
                cell.ibTableView.register(UINib(nibName: "ReceiptCell", bundle: nil), forCellReuseIdentifier: "cell")
                cell.ibTableView.dataSource = self
                cell.ibTableView.delegate = self
                cell.ibTableView.reloadData()
            }
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            totalAmt = 0
            let tempDict = arrayOfReceiptData[indexPath.section] as? NSDictionary ?? NSDictionary()
            let dataArray = tempDict["DATA"] as? NSArray ?? NSArray()
            
            let filterArray = dataArray.filter {
                ($0 as! NSDictionary)["isReceiptGenerated"] as! String != "Y"
            }
            for i in 0..<filterArray.count{
                let dict = filterArray[i] as? NSDictionary ?? NSDictionary()
                let val = dict["F31_AMT"] as? Int ?? 0
                totalAmt += val
            }
            if filterArray.count > 0 {
                let vc = ReceiptVC(nibName: "ReceiptVC", bundle: nil)
                vc.TotalAmount = totalAmt
                vc.arrayOfReceiptData = NSMutableArray(array: filterArray)
                self.navigationItem.backBarButtonItem = backButtonForNavBar()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if tableView.tag == 111 {
                let tempDict = arrayOfReceiptData[indexPath.section] as? NSDictionary ?? NSDictionary()
                let dataArray = tempDict["DATA"] as? NSArray ?? NSArray()
                return CGFloat(CGFloat(dataArray.count) * tableheight + 20)
            } else {
                return 56.5
            }
        }
    }
