

import Foundation

// MARK: - Therapy
class TherapyList: Codable {
    let message: String?
    let result: [Therapy]?
    let success: Bool?

    init(message: String?, result: [Therapy]?, success: Bool?) {
        self.message = message
        self.result = result
        self.success = success
    }
}

// MARK: - Result
class Therapy: Codable {
    let id, f22Tcode: String?
    let pbIpad, pbHappy, pbWeekdy, pbWeekend: Int?
    let pbBrn: String?
    let pbDate1: String?
    let pbDate2: String?
    let f22Tname, f22Hhmm: String?
    let f22Tamt1, f22Tnos: Int?
    let f22Point, f22Pt1, f22Pt2: Double?
    let stHappyhr: String?
    let therapyAmmount: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case f22Tcode = "F22_TCODE"
        case pbIpad = "PB_IPAD"
        case pbHappy = "PB_HAPPY"
        case pbWeekdy = "PB_WEEKDY"
        case pbWeekend = "PB_WEEKEND"
        case pbBrn = "PB_BRN"
        case pbDate1 = "PB_DATE1"
        case pbDate2 = "PB_DATE2"
        case f22Tname = "F22_TNAME"
        case f22Hhmm = "F22_HHMM"
        case f22Tamt1 = "F22_TAMT1"
        case f22Tnos = "F22_TNOS"
        case f22Point = "F22_POINT"
        case f22Pt1 = "F22_PT1"
        case f22Pt2 = "F22_PT2"
        case stHappyhr = "ST_HAPPYHR"
        case therapyAmmount
    }

    init(id: String?, f22Tcode: String?, pbIpad: Int?, pbHappy: Int?, pbWeekdy: Int?, pbWeekend: Int?, pbBrn: String?, pbDate1: String?, pbDate2: String?, f22Tname: String?, f22Hhmm: String?, f22Tamt1: Int?, f22Tnos: Int?, f22Point: Double?, f22Pt1: Double?, f22Pt2: Double?, stHappyhr: String?, therapyAmmount: Int?) {
        self.id = id
        self.f22Tcode = f22Tcode
        self.pbIpad = pbIpad
        self.pbHappy = pbHappy
        self.pbWeekdy = pbWeekdy
        self.pbWeekend = pbWeekend
        self.pbBrn = pbBrn
        self.pbDate1 = pbDate1
        self.pbDate2 = pbDate2
        self.f22Tname = f22Tname
        self.f22Hhmm = f22Hhmm
        self.f22Tamt1 = f22Tamt1
        self.f22Tnos = f22Tnos
        self.f22Point = f22Point
        self.f22Pt1 = f22Pt1
        self.f22Pt2 = f22Pt2
        self.stHappyhr = stHappyhr
        self.therapyAmmount = therapyAmmount
    }
}
