//
//  HomeTabVC.swift
//  SukhoThai Mumbai
//
//  Created by Narsu's iMac on 04/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit
import PKHUD
class HomeTabVC: UIViewController {
    
    @IBOutlet weak var ibCreatedAppointmentCV: UICollectionView!
    @IBOutlet weak var ibTherapyListCV: UICollectionView!
    @IBOutlet weak var ibTherapistCV: UICollectionView!
    @IBOutlet weak var ibTotalTherapy: UILabel!
    @IBOutlet weak var ibGroupBtn: UIButton!
    @IBOutlet weak var ibToggleBtn: UIButton!
    var arrayOfCreatedTherapy = NSMutableArray()
    var arrayOfTherapyList = NSMutableArray()
    var arrayOf1FTTherapyList = NSMutableArray()
    var arrayOf2FTTherapyList = NSMutableArray()
    var arrayOfAllTherapyList = NSMutableArray()
    var arrayOfAllTherapyList1 = [Therapy]()
    var arrayOfTherapist = NSMutableArray()
    var selectedTherapyCodeIndex:Int!
    var selectedTherapistIndex = NSMutableArray()
    let seletectedBackgroundColor = #colorLiteral(red: 0.9176470588, green: 0.6784313725, blue: 0.3019607843, alpha: 1)
    var isTwoTherapist = false
    var selectedGroupIndex = NSMutableArray() 
    var isGroupCreationActive = false
    let colorsArray = [#colorLiteral(red: 0.7333333333, green: 0.9176470588, blue: 0.6509803922, alpha: 1),#colorLiteral(red: 0.9450980392, green: 0.7764705882, blue: 0.9058823529, alpha: 1),#colorLiteral(red: 1, green: 1, blue: 0.7725490196, alpha: 1),#colorLiteral(red: 1, green: 0.8235294118, blue: 0.6470588235, alpha: 1),#colorLiteral(red: 0.9725490196, green: 0.7019607843, blue: 0.9215686275, alpha: 1),#colorLiteral(red: 0.7529411765, green: 0.4235294118, blue: 0.5176470588, alpha: 1),#colorLiteral(red: 1, green: 0.8274509804, blue: 0.7137254902, alpha: 1),#colorLiteral(red: 0.8941176471, green: 0.8196078431, blue: 0.5137254902, alpha: 1),#colorLiteral(red: 0.7647058824, green: 0.7450980392, blue: 0.9411764706, alpha: 1),#colorLiteral(red: 0.831372549, green: 0.8392156863, blue: 0.7843137255, alpha: 1),#colorLiteral(red: 0.7137254902, green: 0.9019607843, blue: 0.7411764706, alpha: 1),#colorLiteral(red: 0.7333333333, green: 0.5607843137, blue: 0.662745098, alpha: 1),#colorLiteral(red: 0.7333333333, green: 0.9176470588, blue: 0.6509803922, alpha: 1),#colorLiteral(red: 0.9450980392, green: 0.7764705882, blue: 0.9058823529, alpha: 1),#colorLiteral(red: 1, green: 1, blue: 0.7725490196, alpha: 1)]
    let toggleBtnColor = [#colorLiteral(red: 0.7333333333, green: 0.9176470588, blue: 0.6509803922, alpha: 1),#colorLiteral(red: 0.9450980392, green: 0.7764705882, blue: 0.9058823529, alpha: 1),#colorLiteral(red: 1, green: 1, blue: 0.7725490196, alpha: 1)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ibCreatedAppointmentCV.register(UINib(nibName: "CreatedTherapyCell", bundle: nil), forCellWithReuseIdentifier: "CreatedTherapyCell")
        ibCreatedAppointmentCV.register(UINib(nibName: "TwoTherapistTherapyCell", bundle: nil), forCellWithReuseIdentifier: "TwoTherapistTherapyCell")
        ibTherapyListCV.register(UINib(nibName: "TherapyListCell", bundle: nil), forCellWithReuseIdentifier: "TherapyListCell")
        ibTherapistCV.register(UINib(nibName: "TherapistImageCell", bundle: nil), forCellWithReuseIdentifier: "TherapistImageCell")
        self.title = "APPOINTMENT"
        getAllTherapyCode()
        ibToggleBtn.backgroundColor = toggleBtnColor[0]
    }
    override func viewWillAppear(_ animated: Bool) {
        getAllCreatedTherapy()
        getAllTherapist()
        self.navigationController?.tabBarController?.tabBar.isHidden = false
    }
    @IBAction func groupBtnClick(_ sender: UIButton) {
        if isGroupCreationActive {
            if selectedGroupIndex.count > 0 {
                apiCreateGroup()
            } else {
                isGroupCreationActive = false
                ibGroupBtn.setTitle("CREATE GROUP", for: .normal)
            }
        } else {
            isGroupCreationActive = true
            ibGroupBtn.setTitle("SAVE GROUP", for: .normal)
        }
        ibCreatedAppointmentCV.reloadData()
    }
    @IBAction func groupAllClick(_ sender: UIButton) {
        let dict = arrayOfCreatedTherapy.filter {
            let dict = ($0 as! NSDictionary)["_id"] as? NSDictionary
            if dict?["groupNumber"] as! String == "0" {
                return true
            } else {
                return false
            }
        }
        if dict.count == 0 {
            self.alert(message: "All Appointments already in group. ", title: "")
        } else {
            let dataDict = dict[0] as? NSDictionary
            let dataArray = dataDict?["DATA"] as? NSArray ?? NSArray()
            apiCreateGroup(groupAllDict: dataArray)
        }
    }
    @IBAction func filterToggleAction(_ sender: UIButton) {
        selectedTherapyCodeIndex = 9999
        sender.backgroundColor = toggleBtnColor[sender.tag]
        switch sender.tag {
        case 0:
            sender.tag = 1
            sender.setTitle("ALL", for: .normal)
            arrayOfTherapyList = arrayOfAllTherapyList
        case 1:
            sender.tag = 2
            sender.setTitle("1FT", for: .normal)
            arrayOfTherapyList = arrayOf1FTTherapyList
        case 2:
            sender.tag = 0
            sender.setTitle("2FT", for: .normal)
            arrayOfTherapyList = arrayOf2FTTherapyList
        default:
            break
        }
        ibTherapyListCV.reloadData()
    }
    func apiCreateGroup(groupAllDict:NSArray = NSArray()) {
        let arrayOfids = NSMutableArray()
        var F31_TRANNO = 0
        if groupAllDict.count == 0 {
            for i in selectedGroupIndex {
                let indexpath = i as! IndexPath
                let dict = arrayOfCreatedTherapy[indexpath.section] as? NSDictionary
                let dataArray = dict?["DATA"] as? NSArray ?? NSArray()
                let dict1 = dataArray[indexpath.row] as? NSDictionary
                let id = dict1?["_id"] as? String ?? ""
                arrayOfids.add(id)
                F31_TRANNO = dict1?["F31_TRANNO"] as? Int ?? 0
            }
        } else {
            for d in groupAllDict {
                let dict = d as? NSDictionary
                let id = dict?["_id"] as? String ?? ""
                arrayOfids.add(id)
                F31_TRANNO = dict?["F31_TRANNO"] as? Int ?? 0
            }
        }
        if isConnectedToNetwork() {
            let urlString = urlCreateGroup()
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            let parameter = ["ids":arrayOfids,
                             "groupNumber":F31_TRANNO] as [String : Any]
            ApiManager.shared.requestApiWithDataType(methodType: PUT, urlString: urlString,parameters: parameter, completionHandeler: { (response, isSuccess, error) in
                
                if isSuccess! {
                    self.isGroupCreationActive = false
                    self.ibGroupBtn.setTitle("CREATE GROUP", for: .normal)
                    self.selectedGroupIndex.removeAllObjects()
                    self.getAllCreatedTherapy()
                    PKHUD.sharedHUD.hide()
                } else {
                    let er = String(describing: error.debugDescription)
                    showAlertMessage(message: "\(requestTimeOutAlert) -----------\n\(er)")
                    PKHUD.sharedHUD.hide()
                }
            })
            
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    
    func getAllCreatedTherapy() {
        if isConnectedToNetwork() {
            let branchcode = getUserdefaultDataForKey(Key: BRANCH) as! String
            let urlString = urlGetAllCreatedTherapyCode(branchCode: branchcode)
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString, completionHandeler: { (response, isSuccess, error) in
                
                if isSuccess! {
                    let therapyCountLbl = LocalizationSystem.sharedInstance.localizedStringForKey(key: "TotalTherapy", comment: "")
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary?
                    let responseLength = dict?["responseLength"] as? String ?? "0"
                    self.arrayOfCreatedTherapy = NSMutableArray(array: dict?["result"] as? NSArray ?? NSArray())
                    self.ibTotalTherapy.text = "\(therapyCountLbl) : \(responseLength)"
                    self.ibCreatedAppointmentCV.reloadData()
                    PKHUD.sharedHUD.hide()
                } else {
                    let er = String(describing: error.debugDescription)
                    showAlertMessage(message: "\(requestTimeOutAlert) -----------\n\(er)")
                    PKHUD.sharedHUD.hide()
                }
            })
            
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    func getAllTherapyCode() {
        
        if isConnectedToNetwork() {
            let urlString = urlGetAllTherapyList()
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString, completionHandeler: { (response, isSuccess, error) in
                if isSuccess! {
//                 let  subjectsObj = try? JSONDecoder().decode(TherapyList.self, from: response as! Data)
//                    print(subjectsObj?.result)
                    
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary?
                    self.arrayOfTherapyList = NSMutableArray(array: dict?["result"] as? NSArray ?? NSArray())
                    self.arrayOfAllTherapyList = self.arrayOfTherapyList
                    for d in self.arrayOfTherapyList {
                        let dict = d as! NSDictionary
                        let tnos = dict["F22_TNOS"] as? Int ?? 0
                        if tnos == 2 {
                            self.arrayOf2FTTherapyList.add(dict)
                        } else {
                            self.arrayOf1FTTherapyList.add(dict)
                        }
                    }
                    self.ibTherapyListCV.reloadData()
                    PKHUD.sharedHUD.hide()
                } else {
                    let er = String(describing: error.debugDescription)
                    print("The request timed out -----------\n\(er)")
                    showAlertMessage(message: "\(requestTimeOutAlert) -----------\n\(er)")
                    PKHUD.sharedHUD.hide()
                }
            })
            
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    func getAllTherapist() {
        if isConnectedToNetwork() {
            let branchcode = getUserdefaultDataForKey(Key: BRANCH) as! String
            let urlString = urlGetAllTherapist(branchCode: branchcode)
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString, completionHandeler: { (response, isSuccess, error) in
                if isSuccess! {
                    
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary?
                    let success = dict!["success"] as? Bool ?? false
                    if success {
                        let dataArray = NSMutableArray(array: dict?["data"] as? NSArray ?? NSArray())
//                        let array = dataArray.filter {
//                            ($0 as! NSDictionary)["ERP_______"] as! String != "Y"
//                        }
                        self.arrayOfTherapist = NSMutableArray(array: dataArray)
                        self.ibTherapistCV.reloadData()
                        PKHUD.sharedHUD.hide()
                    }
                } else {
                    let er = String(describing: error.debugDescription)
                    print("The request timed out -----------\n\(er)")
                    showAlertMessage(message: "\(requestTimeOutAlert) -----------\n\(er)")
                    PKHUD.sharedHUD.hide()
                }
            })
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    func postCreateAppointment(){
        var selectedTherapyst = NSDictionary()
        var selectedTherapyst2 = NSDictionary()
        if isTwoTherapist {
            selectedTherapyst = arrayOfTherapist[selectedTherapistIndex[0] as! Int] as? NSDictionary ?? NSDictionary()
            selectedTherapyst2 = arrayOfTherapist[selectedTherapistIndex[1] as! Int] as? NSDictionary ?? NSDictionary()
        } else {
            selectedTherapyst = arrayOfTherapist[selectedTherapistIndex[0] as! Int] as? NSDictionary ?? NSDictionary()
        }
        
        let selectedCode = arrayOfTherapyList[selectedTherapyCodeIndex] as? NSDictionary
        let firstName = selectedTherapyst["F21_FNAME"] as? String ?? ""
        let IMG_PHOTO = selectedTherapyst["IMG_PHOTO"] as? String ?? ""
        let id = selectedTherapyst["F21_IDNO"] as? Int ?? 0
        let firstName2 = selectedTherapyst2["F21_FNAME"] as? String ?? ""
        let IMG_PHOTO2 = selectedTherapyst2["IMG_PHOTO"] as? String ?? ""
        let id2 = selectedTherapyst2["F21_IDNO"] as? Int ?? 0
        
        let F22_HHMM = selectedCode?["F22_HHMM"] as? String ?? ""
        let therapyCode = selectedCode?["F22_TCODE"] as? String ?? ""
        let F22_POINT = selectedCode?["F22_POINT"] as? Double ?? 0
        let F22_PT1 = selectedCode?["F22_PT1"] as? Double ?? 0
        let F22_PT2 = selectedCode?["F22_PT2"] as? Double ?? 0
        let F22_TNOS = selectedCode?["F22_TNOS"] as? Int ?? 0
        let F22_TAMT1 = selectedCode?["therapyAmmount"] as? Int ?? 0
        
        let branchCode = getUserdefaultDataForKey(Key: BRANCH) as? String ?? ""
        let urlString = urlPostCreateAppointment()
        var parameter = [
            "F31_TCODE": therapyCode,
            "F31_IDNO1" : id,
            "F31_IDNM1" : firstName,
            "IMG_PHOTO":IMG_PHOTO,
            "ST_BRN" : branchCode,
            "F31_POINT":F22_POINT,
            "F31_PT1":F22_PT1,
            "F31_PT2":F22_PT2,
            "F22_HHMM":F22_HHMM,
            "F22_TNOS":F22_TNOS,
            "F31_AMT":F22_TAMT1,
            "F31_TOTAL":F22_TAMT1,
            "groupNumber":"0",
            "entryFrom" : "receptionApp"] as [String : Any]
        
        if isTwoTherapist {
            parameter["F31_IDNO2"] = id2
            parameter["F31_IDNM2"] = firstName2
            parameter["IMG_PHOTO1"] = IMG_PHOTO2
            
        }
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType : POST, urlString : urlString, parameters : parameter, isBody :  false) { (response, isSuccess, error) in
                if isSuccess!{
                    let _ = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    self.selectedTherapistIndex.removeAllObjects()
                    self.selectedTherapyCodeIndex = nil
                    self.getAllCreatedTherapy()
                    self.ibTherapistCV.reloadData()
                    self.ibTherapyListCV.reloadData()
                    showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "New", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "therapy", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "created", comment: ""))")
                } else {
                    let errorString = String(describing: error)
                    PKHUD.sharedHUD.contentView = PKHUDTextView(text: errorString)
                    PKHUD.sharedHUD.hide(afterDelay: 1.0)
                }
                
            }
        } else{
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    
}
extension HomeTabVC:UICollectionViewDelegate,UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == ibCreatedAppointmentCV {
            return arrayOfCreatedTherapy.count
        } else {
            return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case ibCreatedAppointmentCV:
            let dict = arrayOfCreatedTherapy[section] as? NSDictionary
            let dataArray = dict?["DATA"] as? NSArray ?? NSArray()
            return dataArray.count
        case ibTherapyListCV:
            return arrayOfTherapyList.count
        case ibTherapistCV:
            return arrayOfTherapist.count
        default:
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case ibCreatedAppointmentCV:
            let dict1 = arrayOfCreatedTherapy[indexPath.section] as? NSDictionary
            let dataArray = dict1?["DATA"] as? NSArray ?? NSArray()
            let dict = dataArray[indexPath.row] as? NSDictionary
            let F22_TNOS = dict?["F22_TNOS"] as? Int ?? 0
            let groupNumber = dict?["groupNumber"] as? String ?? ""
            if F22_TNOS == 2 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwoTherapistTherapyCell", for: indexPath) as! TwoTherapistTherapyCell
                cell.setCellData(dict: dict)
                if groupNumber != "0" && dataArray.count > 1 {
                    cell.ibTherapyCode.backgroundColor = colorsArray[indexPath.section]
                } else {
                    cell.ibTherapyCode.backgroundColor = .gray
                }
                if isGroupCreationActive {
                    if let isReceiptGenerated = dict?["isReceiptGenerated"] as? String,isReceiptGenerated == "Y"{
                        cell.ibCheckBtn.isHidden = true
                    } else {
                        cell.ibCheckBtn.isHidden = false
                        if selectedGroupIndex.contains(indexPath) {
                            cell.ibCheckBtn.setImage(#imageLiteral(resourceName: "ic_check"), for: .normal)
                        } else {
                            cell.ibCheckBtn.setImage(#imageLiteral(resourceName: "ic_uncheck"), for: .normal)
                        }
                    }
                } else {
                    cell.ibCheckBtn.isHidden = true
                }
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreatedTherapyCell", for: indexPath) as! CreatedTherapyCell
                cell.setCellData(dict: dict)
                if groupNumber != "0" && dataArray.count > 1 {
                    cell.ibTherapyCode.backgroundColor = colorsArray[indexPath.section]
                } else {
                    cell.ibTherapyCode.backgroundColor = .gray
                }
                if isGroupCreationActive {
                    if let isReceiptGenerated = dict?["isReceiptGenerated"] as? String,isReceiptGenerated == "Y"{
                        cell.ibCheckBtn.isHidden = true
                    } else {
                        cell.ibCheckBtn.isHidden = false
                        if selectedGroupIndex.contains(indexPath) {
                            cell.ibCheckBtn.setImage(#imageLiteral(resourceName: "ic_check"), for: .normal)
                        } else {
                            cell.ibCheckBtn.setImage(#imageLiteral(resourceName: "ic_uncheck"), for: .normal)
                        }
                    }
                } else {
                    cell.ibCheckBtn.isHidden = true
                }
                return cell
            }
            
            
        case ibTherapyListCV:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TherapyListCell", for: indexPath) as! TherapyListCell
            let dict = arrayOfTherapyList[indexPath.row] as? NSDictionary
            if selectedTherapyCodeIndex == indexPath.row {
                cell.backgroundColor = seletectedBackgroundColor
                cell.ibTherapyCode.textColor = #colorLiteral(red: 0.1764705882, green: 0.01960784314, blue: 0.2, alpha: 1)
                cell.ibTherapyName.textColor = #colorLiteral(red: 0.1764705882, green: 0.01960784314, blue: 0.2, alpha: 1)
            } else {
                cell.backgroundColor = #colorLiteral(red: 0.1764705882, green: 0.01960784314, blue: 0.2, alpha: 1)
                cell.ibTherapyCode.textColor = seletectedBackgroundColor
                cell.ibTherapyName.textColor = seletectedBackgroundColor
            }
            cell.setCellData(dict: dict)
            return cell
        case ibTherapistCV:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TherapistImageCell", for: indexPath) as! TherapistImageCell
            let dict = arrayOfTherapist[indexPath.row] as? NSDictionary
            if selectedTherapistIndex.contains(indexPath.row) {
                cell.backgroundColor = seletectedBackgroundColor
                cell.ibName.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0, blue: 0.5490196078, alpha: 1)
                cell.ibName.textColor = UIColor.white
            } else {
                cell.ibName.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.ibName.textColor = UIColor.white
            }
            cell.setCellData(dict: dict)
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView {
        case ibCreatedAppointmentCV:
            let dict1 = arrayOfCreatedTherapy[indexPath.section] as? NSDictionary
            let dataArray = dict1?["DATA"] as? NSArray ?? NSArray()
            let dict = dataArray[indexPath.row] as? NSDictionary ?? NSDictionary()
            if let isReceiptGenerated = dict["isReceiptGenerated"] as? String,isReceiptGenerated == "Y"{
                return
            }
            if isGroupCreationActive {
                if selectedGroupIndex.contains(indexPath) {
                    selectedGroupIndex.remove(indexPath)
                } else {
                    selectedGroupIndex.add(indexPath)
                }
                collectionView.reloadData()
            } else {
                let bookAptVC = BookAppointmentVC(nibName: "BookAppointmentVC", bundle: nil)
                bookAptVC.selectedAppointmentDict = NSMutableDictionary(dictionary: dict)
                self.navigationItem.backBarButtonItem = backButtonForNavBar()
                self.navigationController?.pushViewController(bookAptVC, animated: true)
            }
        case ibTherapyListCV,ibTherapistCV:
            if collectionView == ibTherapyListCV {
                let dict = arrayOfTherapyList[indexPath.row] as? NSDictionary
                let F22_TNOS = dict?["F22_TNOS"] as? Int ?? 0
                selectedTherapyCodeIndex = indexPath.row
                if F22_TNOS >= 2 {
                    isTwoTherapist = true
                } else {
                    isTwoTherapist = false
                }
            } else {
                if selectedTherapistIndex.contains(indexPath.row) {
                    selectedTherapistIndex.remove(indexPath.row)
                } else {
                    if selectedTherapistIndex.count < 2 {
                        selectedTherapistIndex.add(indexPath.row)
                    }
                }
            }
            collectionView.reloadData()
            if (selectedTherapyCodeIndex != nil && selectedTherapistIndex.count == 1 && !isTwoTherapist) || (selectedTherapyCodeIndex != nil && selectedTherapistIndex.count == 2 && isTwoTherapist){
                postCreateAppointment()
            }
        default:
            break
        }
    }
}
extension HomeTabVC :UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case ibCreatedAppointmentCV:
            let dict1 = arrayOfCreatedTherapy[indexPath.section] as? NSDictionary
            let dataArray = dict1?["DATA"] as? NSArray ?? NSArray()
            let dict = dataArray[indexPath.row] as? NSDictionary
            let F22_TNOS = dict?["F22_TNOS"] as? Int ?? 0
            if F22_TNOS == 2 {
                return CGSize(width: 200, height: 100)
            } else {
                return CGSize(width: 100, height: 100)
            }
        case ibTherapyListCV:
            return CGSize(width: 100, height: 60)
        case ibTherapistCV:
            return CGSize(width: 100, height: (collectionView.frame.height-15)/3)
        default:
            return CGSize.zero
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: 5, height: 100)
    }
}
extension UIColor {
    static var random: UIColor {
        return .init(hue: .random(in: 0...1), saturation: 1, brightness: 1, alpha: 1)
    }
}
