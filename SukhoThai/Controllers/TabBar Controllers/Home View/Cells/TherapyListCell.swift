//
//  TherapyCell.swift
//  SukhoThai Mumbai
//
//  Created by Narsu's iMac on 04/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class TherapyListCell: UICollectionViewCell {
    @IBOutlet weak var ibTherapyName: UILabel!
    @IBOutlet weak var ibTherapyCode: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    func setCellData(dict: NSDictionary?) {
        if let name = dict?["F22_TNAME"] as? String {
            ibTherapyName.text = name
        } else {
            ibTherapyName.text = ""
        }
        if let name = dict?["F22_TCODE"] as? String {
            ibTherapyCode.text = name
        } else {
            ibTherapyCode.text = ""
        }
    }
}
