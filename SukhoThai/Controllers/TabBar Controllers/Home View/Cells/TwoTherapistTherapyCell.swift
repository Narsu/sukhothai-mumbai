//
//  TwoTherapistTherapyCell.swift
//  SukhoThai Mumbai
//
//  Created by Narsu's iMac on 07/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class TwoTherapistTherapyCell: UICollectionViewCell {
    
    @IBOutlet weak var ibTherapyCode: UILabel!
    @IBOutlet weak var ibTherapistImage: UIImageView!
    @IBOutlet weak var ibTherapistImage1: UIImageView!
    @IBOutlet weak var ibCheckMark: UIImageView!
    @IBOutlet weak var ibCheckBtn: UIButton!
    func setCellData(dict:NSDictionary?){
        if let photoCloseUp = dict?["IMG_PHOTO"] as? String,!photoCloseUp.isEmpty {
            ibTherapistImage.setImageToImageView(imageString: photoCloseUp)
        } else {
            ibTherapistImage.image = #imageLiteral(resourceName: "user")
        }
        if let photoCloseUp = dict?["IMG_PHOTO1"] as? String,!photoCloseUp.isEmpty {
            ibTherapistImage1.setImageToImageView(imageString: photoCloseUp)
        } else {
            ibTherapistImage1.image = #imageLiteral(resourceName: "user")
        }
        if let name = dict?["F31_TCODE"] as? String {
            ibTherapyCode.text = name
        } else {
            ibTherapyCode.text = ""
        }
        if let isReceiptGenerated = dict?["isReceiptGenerated"] as? String,isReceiptGenerated == "Y"{
            ibCheckMark.isHidden = false
        } else {
            ibCheckMark.isHidden = true
        }
        
    }
}
