//
//  TherapistCell.swift
//  SukhoThai Mumbai
//
//  Created by Narsu's iMac on 04/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class TherapistImageCell: UICollectionViewCell {

    @IBOutlet weak var ibImgView: UIImageView!
       @IBOutlet weak var ibName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setCellData(dict:NSDictionary?) {
        if let photoCloseUp = dict?["IMG_PHOTO"] as? String,!photoCloseUp.isEmpty {
            ibImgView.setImageToImageView(imageString: photoCloseUp)
        } else {
            ibImgView.image = #imageLiteral(resourceName: "user")
        }
        if let name = dict?["F21_FNAME"] as? String {
            ibName.text = name
        } else {
            ibName.text = ""
        }
    }
}
