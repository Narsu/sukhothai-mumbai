//
//  CreatedTherapyCell.swift
//  SukhoThai Mumbai
//
//  Created by Narsu's iMac on 04/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class CreatedTherapyCell: UICollectionViewCell {
    
    @IBOutlet weak var ibTherapyCode: UILabel!
    @IBOutlet weak var ibTherapistImage: UIImageView!
    @IBOutlet weak var ibCheckBtn: UIButton!
    @IBOutlet weak var ibCheckMark: UIImageView!
    func setCellData(dict:NSDictionary?){
        if let photoCloseUp = dict?["IMG_PHOTO"] as? String,!photoCloseUp.isEmpty {
            ibTherapistImage.setImageToImageView(imageString: photoCloseUp)
        } else {
            ibTherapistImage.image = #imageLiteral(resourceName: "user")
        }
        if let name = dict?["F31_TCODE"] as? String {
            ibTherapyCode.text = name
        } else {
            ibTherapyCode.text = ""
        }
        if let isReceiptGenerated = dict?["isReceiptGenerated"] as? String,isReceiptGenerated == "Y"{
            ibCheckMark.isHidden = false
        } else {
            ibCheckMark.isHidden = true
        }
    }
    
}
