//
//  ViewHotelCell.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 18/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit
import FloatRatingView

class ViewHotelCell: UITableViewCell {

    @IBOutlet weak var ibHotelName: UILabel!
    @IBOutlet weak var ibOwner: UILabel!
    @IBOutlet weak var ibManager: UILabel!
    @IBOutlet weak var ibConcierge: UILabel!
    @IBOutlet weak var ibRatingView: FloatRatingView!
    @IBOutlet weak var ibCallNumber: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
