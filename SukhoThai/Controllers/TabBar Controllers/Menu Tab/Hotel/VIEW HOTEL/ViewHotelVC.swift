//
//  ViewHotelVC.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 18/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit
import PKHUD

class ViewHotelVC: UIViewController {

    var arrayOfHotels = NSMutableArray()
    @IBOutlet weak var ibTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ibTableview.register(UINib(nibName: "ViewHotelCell", bundle: nil), forCellReuseIdentifier: "ViewHotelCell")
        ibTableview.allowsSelection = false
        ibTableview.tableFooterView = UIView()
    }
    override func viewDidAppear(_ animated: Bool) {
        getHotels()
    }
    func getHotels(){
        if isConnectedToNetwork() {
            
            let urlString = urlGetCategoryData(Category: "Hotel")
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString, completionHandeler: { (response, isSuccess, error) in
                if isSuccess!
                {
                    PKHUD.sharedHUD.hide()
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    
                    let success = dict["status"] as? Bool ?? false
                    if success{
                        let arr = dict["data"] as? NSArray ?? NSArray()
                        self.arrayOfHotels = NSMutableArray(array: arr)
                    }
                    self.ibTableview.reloadData()
                } else {
                    let er = String(describing: error)
                    print("The request timed out -----------\n\(er)")
                    PKHUD.sharedHUD.hide()
                }
            })
            
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
}
extension ViewHotelVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfHotels.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewHotelCell", for: indexPath) as! ViewHotelCell
        let dict = arrayOfHotels[indexPath.row] as? NSDictionary ?? NSDictionary()
        cell.ibHotelName?.text = dict["F30_HTLNM"] as? String ?? ""
        cell.ibOwner.text = "Owner: \(dict["owner"] as? String ?? "")"
        cell.ibManager.text = "Manager: \(dict["F30_PERSON"] as? String ?? "")"
        cell.ibConcierge?.text = "Concierge: \(dict["concierge"] as? String ?? "")"
        let rating = dict["F30_LOCN"] as? String ?? ""
        cell.ibRatingView.rating = Float(rating) ?? 0.0
        cell.ibCallNumber.tag = indexPath.row
        cell.ibCallNumber.addTarget(self, action: #selector(callNumber(btn:)), for: .touchUpInside)
        return cell
    }
    @objc func callNumber(btn : UIButton){
        let dict = arrayOfHotels[btn.tag] as? NSDictionary ?? NSDictionary()
        let num = dict["F30_MOBILE"] as? String ?? ""
        if num != ""{
            num.callNumber()
        }else{
            showAlertMessage(message: "No Number Found!")
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 142
    }
}
