//
//  AddHotelVC.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 18/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit
import FloatRatingView
import Alamofire
import PKHUD
import CoreLocation
import DTTextField

class AddHotelVC: UIViewController, UITextFieldDelegate {
    
    var cityArray = ["BANGALORE","MUMBAI","GOA","PUNE"]
    var selectedCity = ""
    var myRating : Float = 1.0
    var lat = ""
    var long = ""
    var locationManager = CLLocationManager()
    var locationValueCoordinate : CLLocationCoordinate2D!
    @IBOutlet weak var ibScrollView: UIScrollView!
    @IBOutlet weak var ibHotelName: DTTextField!
    @IBOutlet weak var ibOwnerName: DTTextField!
    @IBOutlet weak var ibManagerName: DTTextField!
    @IBOutlet weak var ibConcierge: DTTextField!
    @IBOutlet weak var ibMobileNo: DTTextField!
    @IBOutlet weak var ibCityCollectionView: UICollectionView!
    @IBOutlet weak var ibRatingView: FloatRatingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        registerKeyboardNotifications()
        locationManager.delegate = self
        updateMyLocation()
        setToolBar(textField: ibMobileNo)
        ibRatingView.delegate = self
        ibCityCollectionView.register(UINib(nibName: "CityCell", bundle: nil), forCellWithReuseIdentifier: "CityCell")
    }
    func setToolBar(textField:UITextField){
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(cancelClick))]
        numberToolbar.sizeToFit()
        textField.inputAccessoryView = numberToolbar
    }
    @objc func cancelClick(){
        self.view.endEditing(true)
    }
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height+40, right: 0)
        ibScrollView.contentInset = contentInsets
        ibScrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        ibScrollView.contentInset = UIEdgeInsets.zero
        ibScrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func isValidForm() -> Bool{
        var isValid = true
        if ibHotelName.text!.isEmpty{
            showAlertMessage(message: "Enter Hotel Name")
            isValid = false
            ibHotelName.becomeFirstResponder()
        } else if ibManagerName.text!.isEmpty{
            showAlertMessage(message: "Enter Manager Name")
            isValid = false
            ibManagerName.becomeFirstResponder()
        } else if ibMobileNo.text!.isEmpty{
            showAlertMessage(message: "Enter Mobile No.")
            isValid = false
            ibMobileNo.becomeFirstResponder()
        }else if selectedCity == ""{
            showAlertMessage(message: "Select City")
            isValid = false
        }
        return isValid
    }
    
    func postHotelData(){
        var ownerName = ""
        var concierge = ""
        if ibOwnerName.text!.isEmpty{
            ownerName = ""
        }else{
            ownerName = ibOwnerName.text!
        }
        
        if ibConcierge.text!.isEmpty{
            concierge = ""
        }else{
            concierge = ibConcierge.text!
        }
        
        if locationValueCoordinate != nil{
            lat = "\(locationValueCoordinate.latitude)"
            long = "\(locationValueCoordinate.longitude)"
        }
        let parameter : Parameters = [
            "F30_HTLNM": ibHotelName.text!,
            "owner": ownerName,
            "F30_PERSON": ibManagerName.text!,
            "F30_MOBILE": ibMobileNo.text!,
            "F30_LOCN": myRating,
            "concierge": concierge,
            "latitude": lat,
            "longitude": long,
            "F30_REFCTG": "Hotel",
            "F30_CITY": selectedCity
        ]
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: urlPostCategoryData(),parameters: parameter as [String : Any],isBody: false, completionHandeler: { (response, isSuccess, error) in
                
                if isSuccess! {
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    
                    let strMessage = String(describing: dict["message"] ?? "")
                    
                    if let result = dict["success"] as? Bool , result {
                        PKHUD.sharedHUD.contentView = PKHUDTextView(text: strMessage)
                        PKHUD.sharedHUD.contentView = PKHUDProgressView()
                        PKHUD.sharedHUD.show(onView: self.view)
                        self.clearData()
                        PKHUD.sharedHUD.hide()
                    }
                    else{
                        PKHUD.sharedHUD.contentView = PKHUDTextView(text: strMessage)
                        PKHUD.sharedHUD.show(onView: self.view)
                        PKHUD.sharedHUD.hide(afterDelay: 2.0)
                    }
                    
                } else {
                    let errorString = String(describing: error)
                    PKHUD.sharedHUD.contentView = PKHUDTextView(text: errorString)
                    PKHUD.sharedHUD.show(onView: self.view)
                    PKHUD.sharedHUD.hide(afterDelay: 1.0)
                }
            })
        }
        else{
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""), duration: 1.0)
        }
        
    }
    func clearData(){
        ibHotelName.text = ""
        ibOwnerName.text = ""
        ibManagerName.text = ""
        ibConcierge.text = ""
        ibMobileNo.text = ""
        selectedCity = ""
        ibRatingView.rating = 1.0
        myRating = 1.0
        ibCityCollectionView.reloadData()
    }
    @IBAction func ibSubmitPressed(_ sender: UIButton) {
        if isValidForm(){
            postHotelData()
        }
    }
}
extension AddHotelVC : FloatRatingViewDelegate {
    
    // MARK : Returns the rating value when touch events end
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        myRating = ratingView.rating
    }
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Float) {
    }
    
}
extension AddHotelVC : CLLocationManagerDelegate{
    //MARK- Update Location
    func updateMyLocation(){
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.startUpdatingLocation()
        
    }
    
    private func locationManager(_ manager: CLLocationManager, didFailWithError error: NSError) {
        NSLog("Error to update location :%@",error)
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            break
        case .restricted:
            break
        case .denied:
            NSLog("do some error handling")
            break
        default:
            locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("updating location")
        locationValueCoordinate = locValue
    }
}
extension AddHotelVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cityArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CityCell", for: indexPath) as! CityCell
        if selectedCity == cityArray[indexPath.row]{
            cell.ibCityName.textColor = #colorLiteral(red: 0.9333333333, green: 0, blue: 0.5490196078, alpha: 1)
            cell.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.6582290674, blue: 0.9083387939, alpha: 1)
        }else{
            cell.backgroundColor = .lightGray
            cell.ibCityName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        cell.ibCityName.text = cityArray[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCity = cityArray[indexPath.row]
        ibCityCollectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let labeltext = UILabel()
        labeltext.text = cityArray[indexPath.row]
        labeltext.font = UIFont.systemFont(ofSize: 16)
        labeltext.sizeToFit()
        return CGSize(width: labeltext.frame.size.width + 15, height: 35)
    }
}
