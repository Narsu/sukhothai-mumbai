//
//  MenuCell.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 16/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var ibImage: UIImageView!
    @IBOutlet weak var ibTitle: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
}
