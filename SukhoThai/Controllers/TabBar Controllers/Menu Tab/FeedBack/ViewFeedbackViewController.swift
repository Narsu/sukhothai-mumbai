//
//  ViewFeedbackViewController.swift
//  SukhoThai
//
//  Created by Kesari on 10/06/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class ViewFeedbackViewController: UIViewController {
    
    var arrayOfFeedback = NSMutableArray()
    @IBOutlet weak var ibTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ibTableview.register(UINib(nibName: "FeedbackCell", bundle: nil), forCellReuseIdentifier: "cell")
        ibTableview.allowsSelection = false
        ibTableview.separatorColor = separatorColor
        ibTableview.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        ibTableview.tableFooterView = UIView()
        ibTableview.backgroundColor = tableviewBackgroundColor
    }
    override func viewWillAppear(_ animated: Bool) {
        getFeedback()
    }
    func getFeedback()  {
        let branchcode = getUserdefaultDataForKey(Key: BRANCH) as! String
        let urlString = urlGetFeedbackData(branchCode: branchcode)
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString) { (response, isSuccess, error) in
                if isSuccess!{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary?
                    if let result = dict!["success"] as? Bool, result{
                        self.arrayOfFeedback = NSMutableArray(array: dict!["data"] as? NSArray ?? NSArray())
                        self.ibTableview.reloadData()
                    }
                }
            }
        }
        else{
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
}
extension ViewFeedbackViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfFeedback.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FeedbackCell
        cell.backgroundColor = cellBackGroundColor
        let tempDict = arrayOfFeedback[indexPath.row] as? NSDictionary ?? NSDictionary()
        let guestFeedBackMasterDict = tempDict["guestFeedBackMaster"] as? NSDictionary ?? NSDictionary()
        cell.setCellData(dict: guestFeedBackMasterDict)
        return cell
    }
    
    
}
