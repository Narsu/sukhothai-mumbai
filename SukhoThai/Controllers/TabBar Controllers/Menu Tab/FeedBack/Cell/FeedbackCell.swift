//
//  FeedbackCell.swift
//  SukhoThai
//
//  Created by Kesari on 10/06/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class FeedbackCell: UITableViewCell {
    
    @IBOutlet weak var ibName: UILabel!
    @IBOutlet weak var ibMobile: UILabel!
    @IBOutlet weak var ibRating: UILabel!
    @IBOutlet weak var ibFeedbackmsg: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setCellData(dict : NSDictionary){
        if let name = dict["guestName"] as? String{
            ibName.text = name
        }
        else{
            ibName.text = ""
        }
        if let mobile = dict["mobileNumber"] as? String{
            ibMobile.text = mobile
        }
        else{
            ibMobile.text = ""
        }
        if let rating = dict["Rating"] as? String{
            ibRating.text = "Rating : \(rating)"
        }
        else{
            ibRating.text = ""
        }
        if let feedBackMsg = dict["message"] as? String{
            ibFeedbackmsg.text = feedBackMsg
        }
        else{
            ibFeedbackmsg.text = ""
        }
    }
}
