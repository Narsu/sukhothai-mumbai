//
//  AddFeedbackViewController.swift
//  SukhoThai
//
//  Created by Kesari on 10/06/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit
import FloatRatingView

class AddFeedbackViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    var myRating : Float = 1.0
    let toolBar = UIToolbar()
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var ibGuestName: UITextField!
    @IBOutlet weak var ibMobileNo: UITextField!
    @IBOutlet weak var ibFeedbackTextView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var ibProvideFBLBL: UILabel!
    @IBOutlet weak var ibSubmitOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerKeyboardNotifications()
        ratingView.delegate = self
        ratingView.rating = myRating
        toolBar.sizeToFit()
        self.title = "FEEDBACK"
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTapped))
        toolBar.barTintColor = #colorLiteral(red: 0.4115892947, green: 0.2305042148, blue: 0.4444019794, alpha: 1)
        toolBar.tintColor = #colorLiteral(red: 0.9318091273, green: 0.7251556516, blue: 0.241263926, alpha: 1)
        toolBar.setItems([cancelButton, spaceButton,doneButton], animated: true)
        ibFeedbackTextView.autocorrectionType = .no
        ibMobileNo.inputAccessoryView = toolBar
        ibFeedbackTextView.inputAccessoryView = toolBar

    }
    @objc func doneButtonTapped(){
        self.view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        ibSubmitOutlet.setTitle("\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Submit", comment: ""))", for: .normal)
        ibProvideFBLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "PlzProvideFeedback", comment: "")
        ibGuestName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "GuestNamePH", comment: "")
        ibMobileNo.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "MobileNoPH", comment: "")
    }
    @objc func cancelTapped(){
        self.view.endEditing(true)
    }
    @IBAction func submitClicked(_ sender: UIButton) {
       print("MyRating \(myRating)")
        if isvalidForm(){
            postFeedback()
        }
    }
    func postFeedback(){
        let url = urlPostFeedbackData()
        let createdBy = getUserdefaultDataForKey(Key: "createdBy") as? String ?? ""
        let branchCode = getUserdefaultDataForKey(Key: BRANCH) as? String ?? ""
        let branchName = getUserdefaultDataForKey(Key: "branchName") as? String ?? ""
        let parameter = ["guestFeedBackMaster" :
            [
            "Rating" : "\(myRating)",
            "guestName" : "\(ibGuestName.text ?? "")",
            "mobileNumber" : "\(ibMobileNo.text ?? "")",
            "message" : "\(ibFeedbackTextView.text ?? "")",
            "createdBy" : "\(createdBy)",
            "branchCode" : "\(branchCode)",
            "branchName" : "\(branchName)"
            ]]
        if isConnectedToNetwork(){
        ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: url, parameters: parameter, isBody: false) { (response, isSuccess, error) in
            if isSuccess!{
                let dict = convertStringToDictionary(json: response as! String)
                self.clearData()
                showAlertMessage(message: thankyouConst)
            }
            else{
                showAlertMessage(message: requestTimeOutAlert)
            }
        }

    }
        else{
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
 }

    func isvalidForm() -> Bool{
        var isValid = true
        if ibGuestName.text!.isEmptyStr{
            isValid = false
            showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "GuestNamePH", comment: ""))")
            ibGuestName.becomeFirstResponder()
        }
        else if ibMobileNo.text!.isEmptyStr{
            isValid = false
            showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "MobileNoPH", comment: ""))")
            ibMobileNo.becomeFirstResponder()
        }
        else if ibFeedbackTextView.text.isEmptyStr{
            isValid = false
            showAlertMessage(message: MSG_ENTER_SUGGESTION)
            ibFeedbackTextView.becomeFirstResponder()
        }
        else if ((ibMobileNo.text!.count < 10) || (ibMobileNo.text!.count > 14)){
            isValid = false
            showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "valid", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "MobileNoPH", comment: ""))")
        }
        return isValid
    }
    
    func clearData(){
        ratingView.rating = 1.0
        ibGuestName.text = ""
        ibMobileNo.text = ""
        ibFeedbackTextView.text = ""
    }
    //MARK: Keyboard setup
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height+40, right: 0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

}
extension AddFeedbackViewController : FloatRatingViewDelegate {
    
    // MARK : Returns the rating value when touch events end
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        myRating = ratingView.rating
    }
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Float) {
    }
    
}
