//
//  ViewTaxiDriverData.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 16/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class ViewTaxiDriverData: UITableViewCell {

    @IBOutlet weak var ibImageView: UIImageView!
    @IBOutlet weak var ibDriverName: UILabel!
    @IBOutlet weak var ibTaxiNo: UILabel!
    @IBOutlet weak var ibCall: UIButton!
    @IBOutlet weak var ibCurrentLocation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setCellData(dict : NSDictionary){
        if let name = dict["F30_PERSON"] as? String{
            ibDriverName.text = name
        }else{
            ibDriverName.text = ""
        }
        if let name = dict["F30_DESK"] as? String{
            ibTaxiNo.text = "Taxi No - \(name)"
        }else{
           ibTaxiNo.text = ""
        }
        if let name = dict["F30_LOCN"] as? String{
            ibCurrentLocation.text = "Location - \(name)"
        }else{
            ibCurrentLocation.text = ""
        }
        if let name = dict["Image"] as? String{
            ibImageView.setImageToImageView(imageString: name)
            if name == ""{
                ibImageView.image = UIImage(named: "user")
            }
        }else{
            ibImageView.setImageToImageView(imageString: "user")
        }
    }
}
