//
//  ViewTaxiDriverVC.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 16/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class ViewTaxiDriverVC: UIViewController {

    var arrayOfDriverData = NSMutableArray()
    @IBOutlet weak var ibTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ibTableview.register(UINib(nibName: "ViewTaxiDriverData", bundle: nil), forCellReuseIdentifier: "ViewTaxiDriverData")
        ibTableview.allowsSelection = false
        ibTableview.tableFooterView = UIView()
        ibTableview.dataSource = self
        ibTableview.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        getDriverData()
    }
    func getDriverData() {
        if isConnectedToNetwork() {
            let urlString = urlGetCategoryData(Category: "TAXI")
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString, completionHandeler: { (response, isSuccess, error) in
                if isSuccess!
                {
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary?
                    let success = dict!["status"] as? Bool ?? false
                    ProgressHud.hide(fromView: self.view)
                    if success {
                        self.arrayOfDriverData = NSMutableArray(array: dict?["data"] as? NSArray ?? NSArray())
                        self.ibTableview.reloadData()
                    }
                } else {
                    ProgressHud.hide(fromView: self.view)
                    let er = String(describing: error)
                    print("The request timed out -----------\n\(er)")
                }
            })
        } else {
            ProgressHud.hide(fromView: self.view)
            //ProgressHud.textHud(onview: self.view, message: "Network connection not available.")
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }

}
extension ViewTaxiDriverVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfDriverData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewTaxiDriverData", for: indexPath) as! ViewTaxiDriverData
        let dict = arrayOfDriverData[indexPath.row] as? NSDictionary ?? NSDictionary()
        cell.ibCall.tag = indexPath.row
        cell.ibCall.addTarget(self, action: #selector(callNumber(btn:)), for: .touchUpInside)
        
        cell.setCellData(dict: dict)
        return cell
    }
    @objc func callNumber(btn : UIButton){
        let dict = arrayOfDriverData[btn.tag] as? NSDictionary ?? NSDictionary()
        let num = dict["F30_MOBILE"] as? String ?? ""
        if num != ""{
            num.callNumber()
        }else{
            showAlertMessage(message: "No Number Found!")
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 86.0
    }
}
