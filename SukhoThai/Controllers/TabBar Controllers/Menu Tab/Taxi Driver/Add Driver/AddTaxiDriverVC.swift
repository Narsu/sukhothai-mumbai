//
//  AddTaxiDriverVC.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 16/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit
import DTTextField
import PKHUD
import Alamofire
import CoreLocation

class AddTaxiDriverVC: UIViewController, UITextFieldDelegate {
    
    var standingLocationArray = NSMutableArray()
    var tempSelectedImgView = UIImageView()
    let picker = UIImagePickerController()
    var imgUrl:URL!
    var awsAPIurl = ""
    var lat = ""
    var long = ""
    var locationManager = CLLocationManager()
    var locationValueCoordinate : CLLocationCoordinate2D!
    @IBOutlet weak var ibScrollView: UIScrollView!
    @IBOutlet weak var ibDriverName: DTTextField!
    @IBOutlet weak var ibMobileNo: DTTextField!
    @IBOutlet weak var ibTaxiNo: DTTextField!
    @IBOutlet weak var ibStandingLocation: UITextField!
    @IBOutlet weak var ibUserImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerKeyboardNotifications()
        ibDriverName.delegate = self
        ibMobileNo.delegate = self
        ibTaxiNo.delegate = self
        ibStandingLocation.delegate = self
        ibDriverName.cornerRadius = 5
        ibMobileNo.cornerRadius = 5
        ibTaxiNo.cornerRadius = 5
        ibStandingLocation.cornerRadius = 5
        ibDriverName.clipsToBounds = true
        ibMobileNo.clipsToBounds = true
        ibTaxiNo.clipsToBounds = true
        ibStandingLocation.clipsToBounds = true
        ibTaxiNo.maxLength = 10
        ibMobileNo.maxLength = 10
        setToolBar(textField: ibMobileNo)
        locationManager.delegate = self
        updateMyLocation()
        getStandLocation()
    }
    
    func setToolBar(textField:UITextField){
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(cancelClick))]
        numberToolbar.sizeToFit()
        textField.inputAccessoryView = numberToolbar
    }
    @objc func cancelClick(){
        self.view.endEditing(true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == ibStandingLocation{
            let vc = SearchVC(nibName: "SearchVC", bundle: nil)
            vc.navBarTitle = "Select Stand Location"
            vc.isFromTaxiDriver = true
            vc.delegate = self
            vc.arrayOfHotels = self.standingLocationArray
            ibStandingLocation.endEditing(true)
            self.present(vc, animated: true, completion: nil)
        }
    }
    //MARK: Keyboard setup
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height+40, right: 0)
        ibScrollView.contentInset = contentInsets
        ibScrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        ibScrollView.contentInset = UIEdgeInsets.zero
        ibScrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func getStandLocation(){
        
        let urlString = urlGetStandingLocation()
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show(onView: self.view)
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString) { (response, isSuccess, error) in
                if isSuccess!{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    if let success = dict["status"] as? Bool,success {
                        self.standingLocationArray = NSMutableArray(array: dict["data"] as? NSArray ?? NSArray())
                        
                        PKHUD.sharedHUD.hide()
                        if self.standingLocationArray.count > 0{
                            
                        }else{
                            PKHUD.sharedHUD.hide()
                            self.ibStandingLocation.text = ""
                            showAlertMessage(message: "No location available.")
                        }
                    }
                    print(self.standingLocationArray.count)
                    
                }else{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    let message = dict["message"] as? String ?? ""
                    self.alert(message: message, title: "")
                }
                PKHUD.sharedHUD.hide()
            }
        }
        else{
            showAlertMessage(message: "Internet connection not available.")
        }
    }
    @IBAction func clickPhotoPressed(_ sender: UIButton) {
        actionSheet()
    }
    func isValidForm() -> Bool{
        var isValid = true
        if ibDriverName.text!.isEmpty{
            showAlertMessage(message: "Enter Driver Name")
            isValid = false
            ibDriverName.becomeFirstResponder()
        } else if ibTaxiNo.text!.isEmpty{
            showAlertMessage(message: "Enter Taxi No.")
            isValid = false
            ibTaxiNo.becomeFirstResponder()
        } else if ibStandingLocation.text!.isEmpty{
            showAlertMessage(message: "Enter Stand Location")
            isValid = false
            ibStandingLocation.becomeFirstResponder()
        }
        return isValid
    }
    
    func postDriverData(){
        var mobile = ""
        if ibMobileNo.text!.isEmpty{
            mobile = ""
        }else{
            mobile = ibMobileNo.text!
        }
        if locationValueCoordinate != nil{
            lat = "\(locationValueCoordinate.latitude)"
            long = "\(locationValueCoordinate.longitude)"
        }
        let parameter : Parameters = [
            "F30_PERSON": ibDriverName.text!,
            "F30_MOBILE": mobile,
            "F30_LOCN": ibStandingLocation.text!.capitalizingFirstLetter(),
            "F30_DESK" : ibTaxiNo.text!.capitalized,
            "Image": awsAPIurl,
            "latitude": lat,
            "longitude": long,
            "F30_REFCTG": "TAXI",
            "F30_CITY": "GOA"
        ]
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: urlPostCategoryData(),parameters: parameter as [String : Any],isBody: false, completionHandeler: { (response, isSuccess, error) in
                
                if isSuccess! {
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    
                    let strMessage = String(describing: dict["message"] ?? "")
                    
                    if let result = dict["success"] as? Bool , result {
                        PKHUD.sharedHUD.contentView = PKHUDTextView(text: strMessage)
                        PKHUD.sharedHUD.contentView = PKHUDProgressView()
                        PKHUD.sharedHUD.show(onView: self.view)
                        self.clearData()
                        PKHUD.sharedHUD.hide()
                    }
                    else{
                        PKHUD.sharedHUD.contentView = PKHUDTextView(text: strMessage)
                        PKHUD.sharedHUD.show(onView: self.view)
                        PKHUD.sharedHUD.hide(afterDelay: 2.0)
                    }
                    
                } else {
                    let errorString = String(describing: error)
                    PKHUD.sharedHUD.contentView = PKHUDTextView(text: errorString)
                    PKHUD.sharedHUD.show(onView: self.view)
                    PKHUD.sharedHUD.hide(afterDelay: 1.0)
                }
            })
        }
        else{
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""), duration: 1.0)
        }
        
    }
    @IBAction func ibSubmitPressed(_ sender: UIButton) {
        if isValidForm(){
            postDriverData()
        }
    }
    func clearData(){
        ibDriverName.text = ""
        ibTaxiNo.text = ""
        ibMobileNo.text = ""
        ibStandingLocation.text = ""
        ibUserImage.image = UIImage(named: "user")
    }
}
extension AddTaxiDriverVC{
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.picker.allowsEditing = false
            self.picker.delegate = self
            self.picker.sourceType = UIImagePickerController.SourceType.camera
            self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            self.picker.cameraCaptureMode = .photo
            // self.heightImgView.constant = 200
            //self.selectedPickerOption = "camera"
            self.picker.modalPresentationStyle = .fullScreen
            self.present(self.picker,animated: true,completion: nil)
        } else {
            self.noCamera()
        }
    }
    
    func actionSheet(){
        let alertController = UIAlertController(title: "Select Photo", message: "What would you like to do?", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.allowsEditing = false
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerController.SourceType.camera
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.picker.cameraCaptureMode = .photo
                // self.heightImgView.constant = 200
                //self.selectedPickerOption = "camera"
                self.picker.modalPresentationStyle = .fullScreen
                self.present(self.picker,animated: true,completion: nil)
            } else {
                self.noCamera()
            }
        })
        
        let  deleteButton = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.picker.allowsEditing = false
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerController.SourceType.photoLibrary
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                //self.heightImgView.constant = 200
                //  self.selectedPickerOption = "gallery"
                self.picker.modalPresentationStyle = .fullScreen
                self.present(self.picker,animated: true,completion: nil)
            }
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        
        alertController.popoverPresentationController?.sourceRect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        alertController.popoverPresentationController?.sourceView = self.view
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    
    func uploadPhoto(){
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show(onView: self.view)
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append("TeamST_Photos/Driver".data(using: String.Encoding.utf8)!, withName: "path")
                multipartFormData.append(self.imgUrl, withName: "TeamST")
        },
            to: "http://login.sukhothai.in/route/upload/aws/",
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseString { response in
                        switch response.result
                        {
                        case .success(let JSON) :
                            if response.response?.statusCode == 200 {
                                
                                let dict = convertStringToDictionary(json: JSON) as NSDictionary? ?? NSDictionary()
                                self.awsAPIurl = dict["url"] as? String ?? ""
                                PKHUD.sharedHUD.hide()
                            } else {
                                
                            }
                        case .failure(let error):
                            print(error.localizedDescription)
                            PKHUD.sharedHUD.contentView = PKHUDTextView(text: error.localizedDescription as String)
                            PKHUD.sharedHUD.show()
                            PKHUD.sharedHUD.hide(afterDelay: 1.0, completion: { (complet) in
                                
                            })
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
extension AddTaxiDriverVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            ibUserImage.image = image
        }
        //ibUserImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        //let selectedImage = info[]//info[.originalImage] as UIImage
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        // create a name for your image
        let fileURL = documentsDirectoryURL.appendingPathComponent("\(String(Date().timeIntervalSinceReferenceDate).replacingOccurrences(of: ".", with: "")).jpg")
        imgUrl = fileURL
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String
        {
            
            if mediaType  == "public.image" {
                print("Image Selected")
            }
            
            if mediaType == "public.movie" {
                print("Video Selected")
                showAlertMessage(message: "Video file not accepted")
            }
        }
        let rotatedImg = ibUserImage.image?.upOrientationImage()
        do {
            if let imageData = UIImageJPEGRepresentation(rotatedImg!, 0.5) {
                try imageData.write(to: fileURL)
                self.uploadPhoto()
            }
        }
        catch {
            print(error)
        }
        dismiss(animated: true, completion: {
            
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}
extension UIImage {
    func upOrientationImage() -> UIImage? {
        switch imageOrientation {
        case .up:
            return self
        default:
            UIGraphicsBeginImageContextWithOptions(size, false, scale)
            draw(in: CGRect(origin: .zero, size: size))
            let result = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return result
        }
    }
}
extension AddTaxiDriverVC : CLLocationManagerDelegate{
    //MARK- Update Location
    func updateMyLocation(){
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.startUpdatingLocation()
        
    }
    
    private func locationManager(_ manager: CLLocationManager, didFailWithError error: NSError) {
        NSLog("Error to update location :%@",error)
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined: break
        case .restricted: break
        case .denied:
            NSLog("do some error handling")
            break
        default:
            locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        locationValueCoordinate = locValue
    }
}
extension AddTaxiDriverVC : setTitleDelegate{
    func passData(title: String) {
        ibStandingLocation.text = title
        self.view.layoutIfNeeded()
        ibStandingLocation.endEditing(true)
    }
}
