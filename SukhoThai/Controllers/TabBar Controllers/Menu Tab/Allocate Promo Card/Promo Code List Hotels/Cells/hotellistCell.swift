//
//  hotellistCell.swift
//  Sukhothai Management
//
//  Created by KESARI on 11/22/19.
//  Copyright © 2019 Kesari. All rights reserved.
//

import UIKit

class hotellistCell: UITableViewCell {

    @IBOutlet weak var ibGiftCard:UILabel!
    @IBOutlet weak var ibHotelName:UILabel!
    @IBOutlet weak var ibCardCount:UILabel!
    
    
    func setCellData(Dict:NSDictionary) {
        
        if let carddetail = Dict["F26_GIFTNO"] as? Int{
           ibGiftCard.text = "\(carddetail)"
        }else{
           ibGiftCard.text = ""
        }
        
        if let cardCount =  Dict["F26_GIFTID"] as? Int{
            ibCardCount.text = "\(cardCount)"
        }else{
            ibCardCount.text = ""
        }
        
        if let hotelname = Dict["F26_TONAME"] as? String{
            ibHotelName.text = hotelname
        }else{
            ibHotelName.text = ""
        }
        
    
    }
}
