//
//  hotelListVC.swift
//  Sukhothai Management
//
//  Created by KESARI on 11/22/19.
//  Copyright © 2019 Kesari. All rights reserved.
//

import UIKit
import PKHUD
class hotelListVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var dataArray = NSArray()
    @IBOutlet var ibtableview:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getListHotel()
        self.title = "Card Assigned List"
        self.ibtableview.register(UINib(nibName: "hotellistCell", bundle: nil), forCellReuseIdentifier: "hotellistCell")
        ibtableview.tableFooterView = UIView()
    }
    func getListHotel(){
        let urlString = urlgetHotelList()
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show(onView: self.view)
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: "GET", urlString: urlString) { (Response, isSucess, error) in
                if isSucess!{
                    PKHUD.sharedHUD.hide()
                    let json = convertStringToDictionary(json: Response as! String) as NSDictionary? ?? NSDictionary()
                    self.dataArray = json["data"] as? NSArray ?? NSArray()
                    if self.dataArray.count < 1{
                        showAlertMessage(message: "No data available.")
                    }
                    self.ibtableview.reloadData()
                }else{
                    PKHUD.sharedHUD.hide()
                    self.alert(message: "Same went wrong", title: "Sorry")
                }
            }
        }else{
            PKHUD.sharedHUD.hide()
            showAlertMessage(message: "Internet connection not available.")
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"hotellistCell", for: indexPath) as! hotellistCell
        let Dict = dataArray[indexPath.row] as? NSDictionary ?? NSDictionary()
        cell.setCellData(Dict: Dict)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
