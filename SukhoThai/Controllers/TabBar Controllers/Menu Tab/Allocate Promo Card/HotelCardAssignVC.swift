//
//  HotelCardAssignVC.swift
//  Sukhothai Management
//
//  Created by Narsu's iMac on 21/11/19.
//  Copyright © 2019 Kesari. All rights reserved.
//

import UIKit
import PKHUD
class HotelCardAssignVC: UIViewController, UITextFieldDelegate{
    
    var selectedCity = ""
    var giftCardNo = ""
    var arrayOfHotels = NSMutableArray()
    @IBOutlet weak var ibSegmentControl: UISegmentedControl!
    @IBOutlet weak var ibAlertMessage:UILabel!
    @IBOutlet weak var ibChange:UILabel!
    @IBOutlet weak var ibHotelName:UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Allocate Promo Card"
        ibHotelName.delegate = self
        ibChange.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapClicked))
        self.view.addGestureRecognizer(tap)
        let font = UIFont.systemFont(ofSize: 15)
        ibSegmentControl.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                                for: .normal)
        rightNavigationButton()
        ibSegmentControl.selectedSegmentIndex = -1
    }
    @objc func tapClicked() {
        self.view.endEditing(true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        ibHotelName = textField
        ibHotelName.isUserInteractionEnabled = true
        if textField.text!.isEmpty{
            ibHotelName.isUserInteractionEnabled = false
        }else{
            let vc = SearchVC(nibName: "SearchVC", bundle: nil)
            if (self.arrayOfHotels.count > 0){
                vc.navBarTitle = "Select Hotel"
                vc.isFromHotel = true
                vc.delegate = self
                vc.arrayOfHotels = self.arrayOfHotels
                ibHotelName.endEditing(true)
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        ibHotelName.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.isHidden = true
    }
    @IBAction func ibSegmentChanged(_ sender: UISegmentedControl) {
        ibHotelName.text = ""
        switch sender.selectedSegmentIndex {
        case 1:
            selectedCity = "MUMBAI"//sender.titleForSegment(at: 1)!
            getHotelNames(cityName: selectedCity)
            print(selectedCity)
        case 2:
            selectedCity = "GOA"//sender.titleForSegment(at: 2)!
            getHotelNames(cityName: selectedCity)
            print(selectedCity)
        case 3:selectedCity = "PUNE"//sender.titleForSegment(at: 3)!
        getHotelNames(cityName: selectedCity)
        print(selectedCity)
        default:
            selectedCity = "BANGALORE"
            getHotelNames(cityName: selectedCity)
            print(selectedCity)
        }
    }
    @IBAction func qrCodeScanAction(_ sender: UIButton) {
        if #available(iOS 10.0, *) {
            let vc1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QRScannerController") as! QRScannerController
            vc1.delegate = self
            let navVC = UINavigationController(rootViewController: vc1)
            self.present(navVC, animated: true, completion: nil)
        } else {
            
        }
    }
    func getHotelNames(cityName : String){
        
        let urlString = urlGetHotels(name: cityName)
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show(onView: self.view)
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString) { (response, isSuccess, error) in
                if isSuccess!{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    if let success = dict["success"] as? Bool,success {
                        self.arrayOfHotels = NSMutableArray(array: dict["data"] as? NSArray ?? NSArray())
                        let vc = SearchVC(nibName: "SearchVC", bundle: nil)
                        if self.arrayOfHotels.count > 0{
                            PKHUD.sharedHUD.hide()
                            vc.navBarTitle = "Select Hotel"
                            vc.isFromHotel = true
                            vc.delegate = self
                            vc.arrayOfHotels = self.arrayOfHotels
                            self.present(vc, animated: true, completion: nil)
                        }else{
                            PKHUD.sharedHUD.hide()
                            self.ibChange.isHidden = true
                            self.ibHotelName.text = ""
                            showAlertMessage(message: "No hotel available.",duration: 2.0)
                        }
                    }
                    print(self.arrayOfHotels.count)
                    
                }else{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    let message = dict["message"] as? String ?? ""
                    self.alert(message: message, title: "")
                }
                PKHUD.sharedHUD.hide()
            }
        }
        else{
            showAlertMessage(message: "Internet connection not available.")
        }
    }
    func validateGiftCard(cardNo:String){
        
        let urlString = urlScanGiftCard(number: cardNo)
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show(onView: self.view)
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString.urlEncode()) { (response, isSuccess, error) in
                if isSuccess!{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    if let success = dict["success"] as? Bool,success {
                        if let data = dict["data"] as? String,data.isEmpty {
                            self.ibAlertMessage.text = "Your card is valid."
                            self.ibAlertMessage.textColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
                            self.giftCardNo = cardNo
                        } else {
                            let data = dict["data"] as? String ?? ""
                            self.ibAlertMessage.text = "Your card is already assign to \(data)."
                            self.ibAlertMessage.textColor = UIColor.red
                        }
                        
                    } else {
                        self.ibAlertMessage.text = "Invalid card. Please scan again."
                        self.ibAlertMessage.textColor = UIColor.red
                    }
                    PKHUD.sharedHUD.hide()
                }else{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    let message = dict["message"] as? String ?? ""
                    self.alert(message: message, title: "")
                }
                PKHUD.sharedHUD.hide()
            }
        }
        else{
            showAlertMessage(message: "Internet connection not available.")
        }
    }
    func insertData(){
        
        let urlString = urlAssignCardToHotel()
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show(onView: self.view)
        let parameter = ["saleId":"\(getUserdefaultDataForKey(Key: RECEPTIONIST_ID) as? Int ?? 0)",
            "F26_GFTSTR":giftCardNo,
            "F26_TONAME":ibHotelName.text ?? ""]
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: urlString.urlEncode(),parameters: parameter) { (response, isSuccess, error) in
                PKHUD.sharedHUD.hide()
                if isSuccess!{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    if let success = dict["success"] as? Bool,success {
                        let alertController = UIAlertController(title: "Congratulations", message: "Your card is successfully assign to \(self.ibHotelName.text!)", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (success) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                    }
                    PKHUD.sharedHUD.hide()
                }else{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    let message = dict["message"] as? String ?? ""
                    self.alert(message: message, title: "")
                }
                PKHUD.sharedHUD.hide()
            }
        }
        else{
            showAlertMessage(message: "Internet connection not available.")
        }
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        if giftCardNo.isEmpty {
            alert(message: "Please scan valid card", title: "Error")
        } else if ibHotelName.text!.isEmpty {
            alert(message: "Please enter hotel name", title: "Error")
        } else {
            insertData()
        }
    }
}
extension HotelCardAssignVC:QRscannerDelegate {
    func getQrScanData(giftCardNo: String) {
        print(giftCardNo)
        if giftCardNo.count == 13 {
            validateGiftCard(cardNo:giftCardNo)
        } else {
            alert(message: "Please use valid Gift Card", title: "Error")
        }
    }
}
extension UIViewController {
    func alert(message: String, title: String ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
extension HotelCardAssignVC : setTitleDelegate{
    func passData(title: String) {
        ibHotelName.text = title
        if title.count > 0{
            ibChange.isHidden = false
        }else{
            ibChange.isHidden = true
        }
        ibHotelName.endEditing(true)
    } 
}
extension HotelCardAssignVC:UIPopoverPresentationControllerDelegate {
    
    func rightNavigationButton(){
        let rightButton = UIButton(type: .custom)
        // rightButton.setTitle("List", for: .normal)
        rightButton.setImage(UIImage(named:"menu"), for: .normal)
        // rightButton.setTitleColor(.white, for: .normal)
        rightButton.addTarget(self, action: #selector(getAssignhotel(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    @objc func getAssignhotel(_ sender:UIBarButtonItem){
        let vc = hotelListVC(nibName:"hotelListVC",bundle:nil)
        self.navigationItem.backBarButtonItem = backButtonForNavBar()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
