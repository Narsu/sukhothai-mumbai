//
//  SearchVC.swift
//  Sukhothai Management
//
//  Created by Narsu's iMac on 25/11/19.
//  Copyright © 2019 Kesari. All rights reserved.
//

import UIKit

protocol setTitleDelegate {
    func passData(title : String)
}

class SearchVC: UIViewController {
    
    @IBOutlet weak var ibTitleLbl: UILabel!
    @IBOutlet weak var ibSearchBar: UISearchBar!
    @IBOutlet weak var ibTableview: UITableView!
    var arrayOfHotels = NSMutableArray()
    var searchActive : Bool = false
    var filteredArray = NSMutableArray()
    var delegate : setTitleDelegate?
    var isFromHotel = false
    var isFromTaxiDriver = false
    var navBarTitle = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        ibTitleLbl.text = navBarTitle
        ibTableview.tableFooterView = UIView()
        ibTableview.allowsSelectionDuringEditing = true
        ibSearchBar.delegate = self
    }
    @IBAction func cancelPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension SearchVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            if filteredArray.count > 0{
                return filteredArray.count
            }else{
                return 0
            }
        } else {
            return arrayOfHotels.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.textColor = .black
        var dict = NSDictionary()
        if searchActive {
            if filteredArray.count > 0{
                dict = filteredArray[indexPath.row] as? NSDictionary ?? NSDictionary()
            }
        } else {
            dict = arrayOfHotels[indexPath.row] as! NSDictionary
        }
        if isFromHotel{
            cell.textLabel?.text = dict["F30_HTLNM"] as? String ?? ""
        }else if isFromTaxiDriver{
            cell.textLabel?.text = dict["standLocation"] as? String ?? ""
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var title = ""
        var dict = NSDictionary()
        if searchActive{
            if filteredArray.count > 0{
                dict = filteredArray[indexPath.row] as? NSDictionary ?? NSDictionary()
                if isFromHotel{
                    title = dict["F30_HTLNM"] as? String ?? ""
                    delegate?.passData(title: title)
                }else if isFromTaxiDriver{
                    title = dict["standLocation"] as? String ?? ""
                    delegate?.passData(title: title)
                }
            }
        } else {
            dict = arrayOfHotels[indexPath.row] as? NSDictionary ?? NSDictionary()
            if isFromHotel{
                title = dict["F30_HTLNM"] as? String ?? ""
                delegate?.passData(title: title)
            }else if isFromTaxiDriver{
                title = dict["standLocation"] as? String ?? ""
                delegate?.passData(title: title)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension SearchVC : UISearchBarDelegate{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.ibTableview.reloadData()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        if filteredArray.count < 1{
            searchBar.text = ""
        }
        self.view.endEditing(true)
        ibTableview.reloadData()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if filteredArray.count > 0
        {
            filteredArray.removeAllObjects()
        }
        _ = arrayOfHotels.filter { dict in
            let dict1 = dict as! NSDictionary
            if isFromTaxiDriver{
                if (dict1["standLocation"] as? NSString)?.lowercased.range(of:searchText.lowercased()) != nil{
                    filteredArray = NSMutableArray(array: filteredArray.adding(dict1))
                    searchActive = true
                    return true
                }
            } else if isFromHotel{
                if (dict1["F30_HTLNM"] as? NSString)?.lowercased.range(of:searchText.lowercased()) != nil {
                    filteredArray = NSMutableArray(array: filteredArray.adding(dict1))
                    searchActive = true
                    return true
                }
            }
            self.ibTableview.reloadData()
            return false
        }
    }
}
