//
//  MenuVC.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 16/10/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class MenuVC: UIViewController {
    
    let titleArray = ["Taxi Driver Registration","Hotel Registration","Allocate Promo Card","Card Status","Log Out"]
    let titleImage = ["taxi","hotel","PromoCard","search","logout"]
    let subTitleArray = ["ADD/VIEW","ADD/VIEW","ADD/VIEW","VIEW","New Login"]
    let loginsubtitle = "New Login"
    @IBOutlet weak var ibTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MENU"
        ibTableview.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        ibTableview.tableFooterView = UIView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
}
extension MenuVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.ibTitle.text = titleArray[indexPath.row]
        cell.detailLabel.text = subTitleArray[indexPath.row]
        cell.ibImage.image = UIImage(named: titleImage[indexPath.row])
        //        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        //        cell.textLabel?.text = titleArray[indexPath.row]
        //        cell.detailTextLabel?.text = subTitleArray[indexPath.row]
        //        cell.imageView?.image = UIImage(named: titleImage[indexPath.row])
        //        cell.imageView?.image?.withRenderingMode(.alwaysTemplate)
        if subTitleArray[indexPath.row] != loginsubtitle{
            cell.accessoryType = .disclosureIndicator
        }else{
            cell.accessoryType = .none
        }
        
        return cell
    }
}
extension MenuVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let vc = TaxiDriverVC(nibName: "TaxiDriverVC", bundle: nil)
            self.navigationItem.backBarButtonItem = backButtonForNavBar()
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1{
            let vc = HotelVC(nibName: "HotelVC", bundle: nil)
            self.navigationItem.backBarButtonItem = backButtonForNavBar()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 2{
            let vc = HotelCardAssignVC(nibName: "HotelCardAssignVC", bundle: nil)
            self.navigationItem.backBarButtonItem = backButtonForNavBar()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 3{
            let vc = ScanCardVC(nibName: "ScanCardVC", bundle: nil)
            self.navigationItem.backBarButtonItem = backButtonForNavBar()
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 4{
            let alert = UIAlertController(title: "Logout?", message: "you can always access your content by login back in", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                print("Log out cancelled")
            }))
            alert.addAction(UIAlertAction(title: "Log Out", style: .destructive, handler: { action in
                
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                let redViewController = ErpLoginVC(nibName: "ErpLoginVC", bundle: nil)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
}
