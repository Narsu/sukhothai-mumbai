//
//  CardHeaderCell.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 02/12/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class CardHeaderCell: UITableViewCell {

    @IBOutlet weak var ibBalance: UILabel!
    @IBOutlet weak var ibCardNo: UILabel!
    @IBOutlet weak var ibValidTill: UILabel!
    @IBOutlet weak var ibBranch: UILabel!
    @IBOutlet weak var ibOwner: UILabel!
    @IBOutlet weak var ibFrom: UILabel!
    @IBOutlet weak var ibCredit: UILabel!
    @IBOutlet weak var ibShowLedger: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
