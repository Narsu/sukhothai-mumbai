//
//  CardLedgerCell.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 02/12/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit

class CardLedgerCell: UITableViewCell {
    
    @IBOutlet weak var ibLedgerDetails: UILabel!
    @IBOutlet weak var ibDate: UILabel!
    @IBOutlet weak var ibDrCr: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCellData(dict : NSDictionary){
        // Common for debit / credit
        let ledgerType = dict["sort_DrCr"] as? String ?? ""
        let branch = dict["ST_BRN"] as? String ?? ""
        
        
        //For Debit
        let dbtAmt = dict["F18_AMT"] as? Int ?? 0
        let therapyCode = dict["F31_TCODE"] as? String ?? ""
        let recptNo = dict["F41_RECNO"] as? Int ?? 0
        let drDateString = dict["F18_TRANDT"] as? String ?? ""
        let drTranDate = convertDateString(dateString: drDateString, dateFormat: "MMM d, yyyy")
        
        //For Credit
        let crAmt = dict["F28_AMT"] as? Int ?? 0
        let tranNo = dict["F28_TRANNO"] as? Int ?? 0
        let tranId = dict["F28_TRANID"] as? Int ?? 0
        let crDateString = dict["F28_TRANDT"] as? String ?? ""
        let crTranDate = convertDateString(dateString: crDateString, dateFormat: "MMM d, yyyy")
        
        
        
        if ledgerType == "DR"{
            ibDate.text = drTranDate
            ibDrCr.text = "\(dbtAmt)/-"
            ibLedgerDetails.text = "Utilised agnst Rec# \(branch)/\(recptNo) \(therapyCode)"
            ibDrCr.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }else{
            ibDate.text = crTranDate
            ibDrCr.text = "\(crAmt)/-"
            ibLedgerDetails.text = "Credited By Brn: \(branch) Tran#\(tranNo)/\(tranId)"
            ibDrCr.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        }
    }
    
}
