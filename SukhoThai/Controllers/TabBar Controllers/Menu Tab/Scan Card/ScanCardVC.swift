//
//  ScanCardVC.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 29/11/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit
import PKHUD

class ScanCardVC: UIViewController, UITextFieldDelegate {
    
    var giftCardNo = "" //"2000882409148" //"1000100009124"
    var dataDict = NSMutableDictionary()
    @IBOutlet weak var ibSegmentControl: UISegmentedControl!
    @IBOutlet weak var ibAlertMessage:UILabel!
    @IBOutlet weak var ibCardNumber:UITextField!
    @IBOutlet weak var ibScanBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ibCardNumber.isHidden = true
        ibCardNumber.delegate = self
        ibCardNumber.addTarget(self, action: #selector(textChanged(_:)), for: .allEditingEvents)
        self.title = "Card Status"
        let font = UIFont.systemFont(ofSize: 15)
        ibSegmentControl.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                                for: .normal)
    }
    @objc func textChanged(_ textfield :  UITextField){
        if ibSegmentControl.selectedSegmentIndex == 1{
            if textfield.text?.count == 13{
                getCardDetails(CardNumber: ibCardNumber.text!)
                ibCardNumber.text = ""
            }
        }
    }
    func getCardDetails(CardNumber : String){
        
        let urlString = urlGetCardDetails(number : CardNumber)
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show(onView: self.view)
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString) { (response, isSuccess, error) in
                if isSuccess!{
                    let dict = convertStringToDictionary(json: (response as? String)!) as NSDictionary? ??  NSDictionary()
                    self.dataDict = NSMutableDictionary(dictionary: dict) as NSMutableDictionary? ?? NSMutableDictionary()
                    let vc = CardDetailsVC(nibName: "CardDetailsVC", bundle: nil)
                    let cardNo = self.dataDict["F26_GIFTNO"] as? Int ?? 0
                    let expiry = self.dataDict["F26_VLDDT2"] as? String ?? ""
                    let expiryDate = convertDateString(dateString: expiry, dateFormat: "MMM d, yyyy")
                    let branch = self.dataDict["ST_BRN"] as? String ?? ""
                    let owner = self.dataDict["F26_TONAME"] as? String ?? ""
                    let from = self.dataDict["F26_FRNAME"] as? String ?? ""
                    let credit = self.dataDict["F26_TOTVAL"] as? Int ?? 0
                    let bal = self.dataDict["balanceRemain"] as? Int ?? 0
                    vc.balance = bal
                    vc.cardNo = cardNo
                    vc.expiryDate = expiryDate
                    vc.branch = branch
                    vc.owner = owner
                    vc.from = from
                    vc.credit = credit
                    self.navigationItem.backBarButtonItem = backButtonForNavBar()
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    let message = dict["message"] as? String ?? ""
                    self.alert(message: message, title: "")
                }
                PKHUD.sharedHUD.hide()
            }
        }
        else{
            showAlertMessage(message: "Internet connection not available.")
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func ibSegmentChanged(_ sender: UISegmentedControl) {
        ibCardNumber.text = ""
        switch sender.selectedSegmentIndex {
        case 0:
            self.view.endEditing(true)
            ibScanBtn.isHidden = false
            ibCardNumber.isHidden = true
            ibAlertMessage.isHidden = false
            giftCardNo = ""
        case 1:
            ibCardNumber.isHidden = false
            ibAlertMessage.isHidden = true
            ibScanBtn.isHidden = true
            giftCardNo = ""
            ibCardNumber.becomeFirstResponder()
        default:
            print(giftCardNo)
        }
    }
    
    @IBAction func qrCodeScanAction(_ sender: UIButton) {
        if #available(iOS 10.0, *) {
            let vc1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QRScannerController") as! QRScannerController
            vc1.delegate = self
            let navVC = UINavigationController(rootViewController: vc1)
            self.present(navVC, animated: true, completion: nil)
        } else {
            showAlertMessage(message: "Upgrade to Ios ver.> 10.0")
        }
    }
}
extension ScanCardVC:QRscannerDelegate {
    
    func getQrScanData(giftCardNo: String) {
        print(giftCardNo)
        if giftCardNo.count == 13 {
            getCardDetails(CardNumber: giftCardNo)
        } else {
            alert(message: "Please use valid Gift Card", title: "Error")
        }
    }
}

