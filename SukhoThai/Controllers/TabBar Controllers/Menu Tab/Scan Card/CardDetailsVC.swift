//
//  CardDetailsVC.swift
//  SukhoThai Mumbai
//
//  Created by ITMAC5 on 02/12/19.
//  Copyright © 2019 Kesari Tours. All rights reserved.
//

import UIKit
import PKHUD

class CardDetailsVC: UIViewController {
    
    var balance = 0
    var isShow = false
    var cardNo = 0
    var expiryDate = ""
    var branch = ""
    var owner = ""
    var from = ""
    var credit = 0
    var ledgerArray = NSMutableArray()
    var titleLabel = "Show Ledger"
    @IBOutlet weak var ibTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Current Status"
        ibTableView.register(UINib(nibName: "CardHeaderCell", bundle: nil), forCellReuseIdentifier: "CardHeaderCell")
        ibTableView.register(UINib(nibName: "CardLedgerCell", bundle: nil), forCellReuseIdentifier: "CardLedgerCell")
        getLedgerDetails(CardNumber: "\(cardNo)")
        ibTableView.tableFooterView = UIView()
        ibTableView.backgroundColor = .groupTableViewBackground
    }
    func getLedgerDetails(CardNumber : String){
        
        let urlString = urlGetCardLedger(number: "\(cardNo)")
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show(onView: self.view)
        if isConnectedToNetwork(){
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString) { (response, isSuccess, error) in
                if isSuccess!{
                    let dictArray = convertStringToArray(json: (response as? String)!) as NSArray? ??  NSArray()
                    self.ledgerArray = NSMutableArray(array: dictArray)
                    print(dictArray.count)
                    self.ibTableView.reloadData()
                }else{
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                    let message = dict["message"] as? String ?? ""
                    self.alert(message: message, title: "")
                }
                PKHUD.sharedHUD.hide()
            }
        }
        else{
            showAlertMessage(message: "Internet connection not available.")
        }
    }
}
//extension CardDetailsVC : UITableViewDataSource, UITableViewDelegate{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if section == 0{
//            return 1
//        }else{
//            if isShow{
//                return ledgerArray.count
//            }
//            else{
//                return 0
//            }
//        }
//    }
//    func numberOfSections(in tableView: UITableView) -> Int {
//        if isShow{
//            return 2
//        }else{
//            return 1
//        }
//
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.section == 0{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "CardHeaderCell") as! CardHeaderCell
//            cell.ibBalance.text = "\(balance)/-"
//            cell.ibCardNo.text = "\(cardNo)"
//            cell.ibValidTill.text = expiryDate
//            cell.ibBranch.text = branch
//            cell.ibOwner.text = owner
//            cell.ibFrom.text = from
//            cell.ibCredit.text = "\(credit)"
//            cell.ibShowLedger.addTarget(self, action: #selector(showLedger(sender:)), for: .touchUpInside)
//            return cell
//        }else{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "CardLedgerCell", for: indexPath) as! CardLedgerCell
////            if indexPath.row % 2 == 0{
////                cell.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
////            }else{
////                cell.backgroundColor = .white
////            }
//            let dict = ledgerArray[indexPath.row] as? NSDictionary ?? NSDictionary()
//            cell.setCellData(dict: dict)
//            return cell
//        }
//    }

//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == 0{
//            return 265
//        }else{
//            if isShow{
//                return 67
//            }else{
//                return 0
//            }
//
//        }
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.section == 1{
//            print(indexPath.row)
//        }
//    }
//}
extension CardDetailsVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isShow{
            return ledgerArray.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardLedgerCell", for: indexPath) as! CardLedgerCell
        let dict = ledgerArray[indexPath.row] as? NSDictionary ?? NSDictionary()
        if indexPath.row % 2 == 0{
            cell.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }else{
            cell.backgroundColor = .groupTableViewBackground
        }
        cell.setCellData(dict: dict)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 67
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardHeaderCell") as! CardHeaderCell
        cell.backgroundColor = .groupTableViewBackground
        cell.ibBalance.text = "\(balance)/-"
        cell.ibCardNo.text = "\(cardNo)"
        cell.ibValidTill.text = expiryDate
        cell.ibBranch.text = branch
        cell.ibOwner.text = owner
        cell.ibFrom.text = from
        cell.ibCredit.text = "\(credit)/-"
        cell.ibShowLedger.setTitle(titleLabel, for: .normal)
        cell.ibShowLedger.addTarget(self, action: #selector(showLedger(sender:)), for: .touchUpInside)
        return cell
    }
    @objc func showLedger(sender : UIButton){
        if isShow == true{
            isShow = false
            titleLabel = "Show Ledger"
        }else{
            titleLabel = "Hide Ledger"
            isShow = true
        }
        ibTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 265
    }
}
