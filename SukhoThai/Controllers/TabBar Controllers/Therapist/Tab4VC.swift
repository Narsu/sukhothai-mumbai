//
//  Tab4VC.swift
//  SukhoThai
//
//  Created by kesarimac2 on 17/07/18.
//  Copyright © 2018 Kesari Tours. All rights reserved.
//


import UIKit

class Tab4VC: GLViewPagerViewController,GLViewPagerViewControllerDataSource,GLViewPagerViewControllerDelegate {
    
    var viewControllers: NSArray = NSArray()
    var tabTitles: NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationItem.title = "Therapist"
        self.setDataSource(newDataSource: self)
        self.setDelegate(newDelegate: self)
        self.padding = 10
        self.leadingPadding = 10
        self.trailingPadding = 10
        self.defaultDisplayPageIndex = 0
        self.tabAnimationType = GLTabAnimationType.GLTabAnimationType_End
        //self.indicatorColor = UIColor.orange
        self.indicatorColor = #colorLiteral(red: 0.9333333333, green: 0, blue: 0.5490196078, alpha: 1)
        self.supportArabic = false
        self.fixTabWidth = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller1 = storyboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        controller1.isFromTabVC = true
        let controller2 = storyboard.instantiateViewController(withIdentifier: "TherapistListVC") as! TherapistListVC
        
        self.viewControllers = [
            controller1,
            controller2
        ]
        self.tabTitles = ["\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Add", comment: ""))","\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "View", comment: ""))"]//["ADD","VIEW"]
        self.tabWidth = self.view.frame.width/CGFloat(viewControllers.count) - 20
    }
    
    // MARK: - GLViewPagerViewControllerDataSource
    func numberOfTabsForViewPager(_ viewPager: GLViewPagerViewController) -> Int {
        return self.viewControllers.count
    }
    
    func viewForTabIndex(_ viewPager: GLViewPagerViewController, index: Int) -> UIView {
        let label:UILabel = UILabel.init()
        label.text = self.tabTitles.object(at: index) as? String
        label.textColor = #colorLiteral(red: 0.9318091273, green: 0.7251556516, blue: 0.241263926, alpha: 1)
        label.textAlignment = NSTextAlignment.center
        label.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
        return label
    }
    
    func contentViewControllerForTabAtIndex(_ viewPager: GLViewPagerViewController, index: Int) -> UIViewController {
        return self.viewControllers.object(at: index) as! UIViewController
    }
    
    // MARK: - GLViewPagaerViewControllerDelegate
    func didChangeTabToIndex(_ viewPager: GLViewPagerViewController, index: Int, fromTabIndex: Int) {
        let prevLabel:UILabel = viewPager.tabViewAtIndex(index: fromTabIndex) as! UILabel
        let currentLabel:UILabel = viewPager.tabViewAtIndex(index: index) as! UILabel
        prevLabel.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
        currentLabel.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
//        prevLabel.textColor = UIColor.white//UIColor.init(colorLiteralRed: 0.3, green: 0.3, blue: 0.3, alpha: 1.0)
//        currentLabel.textColor = UIColor.white//UIColor.init(colorLiteralRed: 0.5, green: 0.0, blue: 0.5, alpha: 1.0)
        prevLabel.textColor = #colorLiteral(red: 0.9318091273, green: 0.7251556516, blue: 0.241263926, alpha: 1)
        currentLabel.textColor = #colorLiteral(red: 0.9333333333, green: 0, blue: 0.5490196078, alpha: 1)
    }
    
    func willChangeTabToIndex(_ viewPager: GLViewPagerViewController, index: Int, fromTabIndex: Int, progress: CGFloat) {
        if fromTabIndex == index {
            return;
        }
        
        let prevLabel:UILabel = viewPager.tabViewAtIndex(index: fromTabIndex) as! UILabel
        let currentLabel:UILabel = viewPager.tabViewAtIndex(index: index) as! UILabel
        prevLabel.transform = CGAffineTransform.identity.scaledBy(x: 1.0 - (0.1 * progress), y: 1.0 - (0.1 * progress))
        currentLabel.transform = CGAffineTransform.identity.scaledBy(x: 0.9 + (0.1 * progress), y: 0.9 + (0.1 * progress))
//        currentLabel.textColor = UIColor.white // UIColor.init(colorLiteralRed: Float(0.5 + 0.2 * progress), green: Float(0.5 - 0.3 * progress), blue: Float(0.5 + 0.2 * progress), alpha: 1.0)
//        prevLabel.textColor = UIColor.white //UIColor.init(colorLiteralRed: Float(0.5 - 0.2 * progress), green: Float(0.0 + 0.3 * progress), blue: Float(0.5 - 0.2 * progress), alpha: 1.0)
        prevLabel.textColor = #colorLiteral(red: 0.9318091273, green: 0.7251556516, blue: 0.241263926, alpha: 1)
        currentLabel.textColor = #colorLiteral(red: 0.9333333333, green: 0, blue: 0.5490196078, alpha: 1)
    }
    
    func widthForTabIndex(_ viewPager: GLViewPagerViewController, index: Int) -> CGFloat {
        let prototypeLabel:UILabel = UILabel.init()
        prototypeLabel.text = self.tabTitles.object(at: index) as? String
        prototypeLabel.textAlignment = NSTextAlignment.center
        prototypeLabel.font = UIFont.systemFont(ofSize: 16.0)
        return prototypeLabel.intrinsicContentSize.width
    }
    
}
