//
//  RegisterVC.swift
//  SukhoThai
//
//  Created by kesarimac2 on 26/07/18.
//  Copyright © 2018 Kesari Tours. All rights reserved.
//

import UIKit
import DTTextField
import PKHUD
import Alamofire

protocol CustomDelegates {
    func selectedPopoverData(selectedText:String)
    func selectedGender(selectedText : String)
//    func getTherapyCodeVcData(selectedDictionary : NSDictionary)
//    func changeLanguage(language : String)
}

protocol getTherapyCodeDelegate{
    func getTherapyCodeVcData(selectedDictionary : NSDictionary)
}

class RegisterVC: UIViewController,CustomDelegates, UITextFieldDelegate {
    func selectedGender(selectedText: String) {
        
    }

    var arrayOfDestination = [String]()
    
    let picker = UIImagePickerController()
    var imgUrl:URL!
    var awsAPIurl = ""
    var isFromTabVC = false
    var dateToPass = ""
    var designation = "Therapist"
    var hidePassField = true
    @IBOutlet weak var ibBirthDate: DTTextField!
    @IBOutlet weak var ibFirstName: DTTextField!
    @IBOutlet weak var ibLastName: DTTextField!
    @IBOutlet weak var ibEmailId: DTTextField!
    @IBOutlet weak var ibMobileNo: DTTextField!
    @IBOutlet weak var ibSpaName: DTTextField!
    @IBOutlet weak var ibPassportNo: DTTextField!
    @IBOutlet weak var ibLineId: DTTextField!
    @IBOutlet weak var ibBoysCount: DTTextField!
    @IBOutlet weak var ibGirlsCount: DTTextField!
    @IBOutlet weak var ibHomeTown: DTTextField!
    @IBOutlet weak var ibBkkStayPlace: DTTextField!
    @IBOutlet weak var ibTherapistCertificate: DTTextField!
    @IBOutlet weak var ibTrainingschoolName: DTTextField!
    @IBOutlet weak var ibNoOfYears: DTTextField!
    @IBOutlet weak var ibSpeciality: DTTextField!
    @IBOutlet weak var ibIdProof: DTTextField!
    @IBOutlet weak var ibPassword: DTTextField!
    @IBOutlet weak var ibHeightConstraintSelect: NSLayoutConstraint!
    
    @IBOutlet weak var ibSelectSukhothai: UIButton!
    //Localization Outlets
    @IBOutlet weak var btnTitle:UIButton!
    @IBOutlet weak var btnStatus:UIButton!
    @IBOutlet weak var ibSubmitOutlet: UIButton!
    @IBOutlet weak var ibPhotoHandsOutlet: UIButton!
    @IBOutlet weak var ibPhotoFullOutlet: UIButton!
    @IBOutlet weak var ibPhotoCloseUpOutlet: UIButton!
    @IBOutlet weak var ibCloseupImgView: UIImageView!
    @IBOutlet weak var ibFullImgView: UIImageView!
    @IBOutlet weak var ibHandsImgView: UIImageView!
    
    var datePicker : UIDatePicker!
    var activeTextField = UITextField()
    var tempPopOverBtn = UIButton()
    var tempSelectedImgView = UIImageView()
    var imageExists = false
    var therapyCodeDictionary = NSDictionary()
    var arrayOfSukhothai = [String]()
    var branchCodeR = ""
    var branchNameR = ""
    var createdByR = ""
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if hidePassField{
            ibHeightConstraintSelect.constant = 0
            ibPassword.isHidden = true
        }
        else{
            getSukhothai()
            ibHeightConstraintSelect.constant = 40
            ibPassword.isHidden = false
        }
        registerKeyboardNotifications()
        pickUpDate(textField: ibBirthDate)
        self.ibCloseupImgView.image = UIImage(named: "logo2")
        //Button Localization
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        btnTitle.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Mr", comment: ""), for: .normal)
        btnStatus.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Single", comment: ""), for: .normal)
        ibSubmitOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Submit", comment: ""), for: .normal)
        ibPhotoCloseUpOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "PhotoCloseUp", comment: ""), for: .normal)
        ibPhotoFullOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "PhotoFull", comment: ""), for: .normal)
        ibPhotoHandsOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "PhotoHands", comment: ""), for: .normal)
        //TextField Placeholder Localization
        ibFirstName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "FirstNamePH", comment: "")
        ibLastName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "LastNamePH", comment: "")
        ibEmailId.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "EmailPH", comment: "")
        ibMobileNo.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "MobileNoPH", comment: "")
        ibBirthDate.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "BirthDatePH", comment: "")
        ibSpaName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "SpaNamePH", comment: "")
        ibPassportNo.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "PassportPH", comment: "")
        ibLineId.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "LineIDPH", comment: "")
        ibBoysCount.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "BoysCountPH", comment: "")
        ibGirlsCount.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "GirlsCountPH", comment: "")
        ibHomeTown.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "HomeTownPH", comment: "")
        ibBkkStayPlace.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "BKKStayPlacePH", comment: "")
        ibTherapistCertificate.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "TherapistCertPH", comment: "")
        ibTrainingschoolName.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "TrainingSchoolPH", comment: "")
        ibNoOfYears.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "NoOfYrsPH", comment: "")
        ibSpeciality.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "SpecialityPH", comment: "")
        ibIdProof.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "IDProofPH", comment: "")
        ibPassword.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "password", comment: "")
    }
    
    @IBAction func selectsukhothaiPressed(_ sender: UIButton) {
        showPopOver(sender: sender, titles: arrayOfSukhothai)
    }
    func getSukhothai(){
        if isConnectedToNetwork() {
          
            let urlString = urlGetAllSukhoThai()
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString, completionHandeler: { (response, isSuccess, error) in
                if isSuccess!
                {
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary?
                    
                    let success = dict!["success"] as? Bool ?? false
                    let result = dict!["result"] as? NSArray ?? NSArray()
                    let branchDictArray = result[0] as? NSDictionary ?? NSDictionary()
                    let branchDict = branchDictArray["branch"] as? NSDictionary ?? NSDictionary()
                    let branchNameRegister = branchDict["branchName"] as? String ?? ""
                    let branchCodeRegister = branchDict["branchCode"] as? String ?? ""
                    let createdByRegister = branchDict["createdBy"] as? String ?? ""
                        if success {
                        self.branchNameR = branchNameRegister
                        self.branchCodeR = branchCodeRegister
                        self.createdByR = createdByRegister
                        self.arrayOfSukhothai.append(branchNameRegister)
                        PKHUD.sharedHUD.hide()
                    }
                } else {
                    let er = String(describing: error)
                    print("The request timed out -----------\n\(er)")
                    showAlertMessage(message: "\(requestTimeOutAlert) -----------\n\(er)")
                    PKHUD.sharedHUD.hide()
                }
            })
            
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    @IBAction func merrageSelection(_ sender: UIButton) {
        
        let marriedConst = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Married", comment: "")
        let arrayOfStatus = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "Single", comment: ""),marriedConst]
        showPopOver(sender: sender, titles: arrayOfStatus)
    }
    
    @IBAction func titleSelection(_ sender: UIButton) {
        let Mr = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Mr", comment: "")
        let Ms = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Ms", comment: "")
        let Mrs = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Mrs", comment: "")
        let arrayOfTitles = [Mr,Ms,Mrs]//["Mr","Ms","Mrs"]
        showPopOver(sender: sender, titles: arrayOfTitles)
    }
    
    func selectedPopoverData(selectedText: String) {
        tempPopOverBtn.setTitle(selectedText, for: .normal)
    }
    
    @IBAction func closeupPhoto(_ sender: Any) {
        
        tempSelectedImgView = ibCloseupImgView
        actionSheet()
    }
    @IBAction func fullPhoto(_ sender: Any) {
        tempSelectedImgView = ibFullImgView
        actionSheet()
    }
    @IBAction func handsPhoto(_ sender: Any) {
        tempSelectedImgView = ibHandsImgView
        actionSheet()
    }
     @IBAction func submit(_ sender: Any) {

       if isValidform() && imageExists{
            register()
            self.view.endEditing(true)
            //ProgressHud.textHud(onview: self.view, message: "Therapist Added")
        }
        
    }
   
    func showPopOver(sender:UIButton,titles:[String]) {
//        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "PopOverVC") as! PopOverVC
//        vc.delegate = self
//        tempPopOverBtn = sender
//        vc.arrayOfTitles = titles
//        vc.selectGender = true
//        vc.preferredContentSize = CGSize(width: 300, height: titles.count*45)
//        vc.modalPresentationStyle = UIModalPresentationStyle.popover
//        let popover: UIPopoverPresentationController = vc.popoverPresentationController!
//        
//        popover.sourceView = sender
//        popover.sourceRect = sender.bounds
//        popover.delegate = self
//        self.present(vc, animated: true, completion: nil)
    }
    
    func setToolBar(textField:UITextField){
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(cancelClick))]
        numberToolbar.sizeToFit()
        textField.inputAccessoryView = numberToolbar
    }
    
    func pickUpDate(textField : UITextField){
        activeTextField = textField
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        self.datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        textField.inputView = self.datePicker
        datePicker.maximumDate = Date()
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.barTintColor = #colorLiteral(red: 0.4115892947, green: 0.2305042148, blue: 0.4444019794, alpha: 1)
        toolBar.tintColor = #colorLiteral(red: 0.9318091273, green: 0.7251556516, blue: 0.241263926, alpha: 1)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        let date = Date()
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        // Set date format
        //dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.dateFormat = "dd-MM-yyyy"
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: sender.date)
        activeTextField.text = selectedDate
    }
    
    // MARK:- Button Done and Cancel
    
    @objc func doneClick() {
        let date = Date()
        let dateFormatter1 = DateFormatter()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter1.dateFormat = "dd-MM-yyyy"
        dateToPass = dateFormatter.string(from: datePicker.date)
        activeTextField.text = dateFormatter1.string(from: datePicker.date)
        activeTextField.resignFirstResponder()
    }
    @objc func cancelClick() {
        self.view.endEditing(true)
    }
    
    //MARK: Keyboard setup
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height+40, right: 0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func getDestination() {
        if isConnectedToNetwork() {
            let  urlString = urlGetDestination()
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show(onView: self.view)
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString, completionHandeler: { (response, isSuccess, error) in
                PKHUD.sharedHUD.hide()
                
                if isSuccess!
                {
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary?
                    let arrayList = NSMutableArray(array: dict?["data"] as? NSArray ?? NSArray())
                    for list in arrayList {
                        let d = list as! NSDictionary
                        let resultDict = d["designation"] as? NSDictionary ?? NSDictionary()
                        let destination = resultDict["designation"] as? String ?? ""
                        self.arrayOfDestination.append(destination)
                    }
                    
                    
                } else {
                    let er = String(describing: error)
                    print("The request timed out -----------\n\(er)")
                }
            })
        } else {
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    
    //Email Validation Code
    func isValidEmailId(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func isValidform() -> Bool {
        
        var isValid = true
        
//        if ((ibFirstName.text?.count == 0) && (ibLastName.text?.count == 0) && (ibEmailId.text?.count == 0) && (ibMobileNo.text?.count == 0)){
//            ibFirstName.becomeFirstResponder()
//            isValid = false
//            showAlertMessage(message: fillInReqDetailsAlert)
//        }
//        else
            if (ibFirstName.text?.isEmptyStr)!{
            showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "FirstNamePH", comment: ""))")
            isValid = false
            ibFirstName.becomeFirstResponder()
        }
        else if (ibLastName.text?.isEmptyStr)!{
            showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "LastNamePH", comment: ""))")
            isValid = false
            ibLastName.becomeFirstResponder()
        }
        else if (ibEmailId.text?.isEmptyStr)!{
            showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "EmailPH", comment: ""))")
            isValid = false
            ibEmailId.becomeFirstResponder()
        }
        else if (ibMobileNo.text == ""){
            showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "MobileNoPH", comment: ""))")
            isValid = false
            ibMobileNo.becomeFirstResponder()
        }
       else if (ibBirthDate.text?.isEmptyStr)!{
            showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "select", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "BirthDatePH", comment: ""))")
            isValid = false
            ibBirthDate.becomeFirstResponder()
        }
        else if (ibIdProof.text?.isEmptyStr)!{
            showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "IDProofPH", comment: ""))")
            isValid = false
            ibIdProof.becomeFirstResponder()
        }
        else if !hidePassField{
                if (ibPassword.text?.isEmptyStr)!{
                    showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "password", comment: ""))")
                    isValid = false
                    ibPassword.becomeFirstResponder()
                }
            }
        else if imageExists == false{
                isValid = false
                showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "select", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "PhotoCloseUp", comment: ""))")
                ibCloseupImgView.becomeFirstResponder()
            }
        
        else if ((ibEmailId.text?.isEmptyStr == false) && (ibMobileNo.text!.count > 0)){
            if isValidEmailId(testStr: ibEmailId.text!) == false{
                isValid = false
                showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "valid", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "EmailPH", comment: ""))")
                ibEmailId.becomeFirstResponder()
            }
            else if ((ibMobileNo.text!.count < 10) || (ibMobileNo.text!.count > 14)){
                isValid = false
                showAlertMessage(message: "\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Enter", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "valid", comment: "")) \(LocalizationSystem.sharedInstance.localizedStringForKey(key: "MobileNoPH", comment: ""))")
                ibMobileNo.becomeFirstResponder()
            }
            
        }

        print("isValid is \(isValid)")
        return isValid
    }
    
    func register() {
        var createdBy = ""
        var branchCode = ""
        var branchName = ""
        
        var guestTitle = ""
        if btnTitle.title(for: .normal) == "นาย"{
            guestTitle = "Mr"
        }
        else if btnTitle.title(for: .normal) == "นางสาว"{
            guestTitle = "Ms"
        }
        else if btnTitle.title(for: .normal) == "นาง"{
            guestTitle = "Mrs"
        }
        else{
            guestTitle = btnTitle.title(for: .normal)!
        }
        
        var guestMaritalStatus = ""
        if btnStatus.title(for: .normal) == "เดียว"{
            guestMaritalStatus = "Single"
        }
        else if btnStatus.title(for: .normal) == "แต่งงาน"{
            guestMaritalStatus = "Married"
        }
        else{
            guestMaritalStatus = btnStatus.title(for: .normal)!
        }
        if !hidePassField{
            branchCode = branchCodeR
            branchName = branchNameR
            createdBy = createdByR
        }
        else{
            createdBy = getUserdefaultDataForKey(Key: "createdBy") as? String ?? ""
            branchCode = getUserdefaultDataForKey(Key: BRANCH) as? String ?? ""
            branchName = getUserdefaultDataForKey(Key: "branchName") as? String ?? ""
        }
        let parameter:Parameters = [
            "title":  guestTitle, "firstName": ibFirstName.text ?? "","lastName": ibLastName.text ?? "",
            "DOB": dateToPass,"emailId":ibEmailId.text ?? "","mobileNo": ibMobileNo.text ?? "","maritalStatus": guestMaritalStatus,"designation": designation ,"spaName":ibSpaName.text ?? "","passportNumber":ibPassportNo.text ?? "",
            "lineID":ibLineId.text ?? "","boyCount": ibBoysCount.text ?? "","girlsCount":ibGirlsCount.text ?? "",
            "bkkStayPlace":ibBkkStayPlace.text ?? "","therapistCertificate":ibTherapistCertificate.text ?? "",
            "trainingSchoolName": ibTrainingschoolName.text ?? "","homeTown":ibHomeTown.text ?? "","noOfYearsAsTherapist":ibNoOfYears.text ?? "","speciality":ibSpeciality.text ?? "","idProof":ibIdProof.text ?? "","photoFull": "", "photoCloseUp" : "\(awsAPIurl)", "photoHands" : "","branchCode" : branchCode, "branchName" : branchName, "createdBy" : createdBy,"deviceID" : DEVICE_ID,"password" : ibPassword.text ?? ""
            ]
        if isConnectedToNetwork(){
        ApiManager.shared.requestApiWithDataType(methodType: POST, urlString: urlPostRegister(),parameters: parameter as [String : Any],isBody: false, completionHandeler: { (response, isSuccess, error) in
            
            if isSuccess! {
                let dict = convertStringToDictionary(json: response as! String) as NSDictionary? ?? NSDictionary()
                
                let strMessage = String(describing: dict["message"] ?? nil)
                
                if let result = dict["success"] as? Bool , result {
                    PKHUD.sharedHUD.contentView = PKHUDTextView(text: strMessage)//"\(newConst) \(therapistConst) \(createdConst)")
                    PKHUD.sharedHUD.show(onView: self.view)
                    PKHUD.sharedHUD.hide(afterDelay: 2.0, completion: { (success) in
                    self.clearData()
                   // self.navigationController?.popViewController(animated: true)
                    })
                }
                else{
                    PKHUD.sharedHUD.contentView = PKHUDTextView(text: strMessage)
                    PKHUD.sharedHUD.show(onView: self.view)
                    PKHUD.sharedHUD.hide(afterDelay: 2.0)
                }
                
            } else {
                let errorString = String(describing: error)
                PKHUD.sharedHUD.contentView = PKHUDTextView(text: errorString)
                PKHUD.sharedHUD.show(onView: self.view)
                PKHUD.sharedHUD.hide(afterDelay: 1.0)
            }
        })
        }
        else{
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""), duration: 1.0)
        }
}
}
extension RegisterVC:UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}

extension RegisterVC {
    func actionSheet(){
        let alertController = UIAlertController(title: "Select Photo", message: "What would you like to do?", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "Camera", style: .default, handler: { (action) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.allowsEditing = false
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerControllerSourceType.camera
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.picker.cameraCaptureMode = .photo
                // self.heightImgView.constant = 200
                //self.selectedPickerOption = "camera"
                self.picker.modalPresentationStyle = .fullScreen
                self.present(self.picker,animated: true,completion: nil)
            } else {
                self.noCamera()
            }
        })
        
        let  deleteButton = UIAlertAction(title: "Gallery", style: .default, handler: { (action) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.picker.allowsEditing = false
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                //self.heightImgView.constant = 200
                //  self.selectedPickerOption = "gallery"
                self.picker.modalPresentationStyle = .fullScreen
                self.present(self.picker,animated: true,completion: nil)
            }
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        
        alertController.popoverPresentationController?.sourceRect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        alertController.popoverPresentationController?.sourceView = self.view
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    
    func uploadPhoto(){
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show(onView: self.view)
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append("Sukhothai_imagescloseup/IosThailand".data(using: String.Encoding.utf8)!, withName: "path")
                multipartFormData.append(self.imgUrl, withName: "Billing")
        },
            to: "http://login.sukhothai.in/route/upload/aws/",
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseString { response in
                        switch response.result
                        {
                        case .success(let JSON) :
                            if response.response?.statusCode == 200 {
                                
                                let dict = convertStringToDictionary(json: JSON) as NSDictionary? ?? NSDictionary()
                                self.awsAPIurl = dict["url"] as? String ?? ""
                                self.imageExists = true
                                PKHUD.sharedHUD.hide()
                            } else {
                                
                            }
                        case .failure(let error):
                            print(error.localizedDescription)
                            PKHUD.sharedHUD.contentView = PKHUDTextView(text: error.localizedDescription as String)
                            PKHUD.sharedHUD.show()
                            PKHUD.sharedHUD.hide(afterDelay: 1.0, completion: { (complet) in
                                
                            })
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
    }
   
    func clearData(){
        ibFirstName.text = ""
        ibLastName.text = ""
        ibEmailId.text = ""
        ibMobileNo.text = ""
        ibBirthDate.text = ""
        ibSpaName.text = ""
        ibPassportNo.text = ""
        ibLineId.text = ""
        ibBoysCount.text = ""
        ibGirlsCount.text = ""
        ibHomeTown.text = ""
        ibBkkStayPlace.text = ""
        ibTherapistCertificate.text = ""
        ibTrainingschoolName.text = ""
        ibNoOfYears.text = ""
        ibSpeciality.text = ""
        ibIdProof.text = ""
        btnTitle.setTitle("\(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Mr", comment: ""))", for: .normal)
        btnStatus.setTitle(singleConst, for: .normal)
        ibCloseupImgView.image  = UIImage(named: "logo2")
        ibFullImgView.image = UIImage(named: "logo2")
        ibHandsImgView.image = UIImage(named: "logo2")
        imageExists = false
        ibPassword.text = ""
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
extension RegisterVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        tempSelectedImgView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        // create a name for your image
        let fileURL = documentsDirectoryURL.appendingPathComponent("\(String(Date().timeIntervalSinceReferenceDate).replacingOccurrences(of: ".", with: "")).jpg")
        imgUrl = fileURL
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String {
            
            if mediaType  == "public.image" {
                print("Image Selected")
            }
            
            if mediaType == "public.movie" {
                print("Video Selected")
                showAlertMessage(message: videofilenotAccepted)
            }
        }
        
        do {
            if let imageData = UIImageJPEGRepresentation(tempSelectedImgView.image!, 0.5) {
                try imageData.write(to: fileURL)
                self.uploadPhoto()
            }
        }
        catch {
            print(error)
        }
        dismiss(animated: true, completion: {
            
        })
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

