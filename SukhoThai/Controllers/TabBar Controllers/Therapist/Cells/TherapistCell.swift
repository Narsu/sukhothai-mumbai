//
//  TherapistCell.swift
//  SukhoThai
//
//  Created by kesarimac2 on 06/08/18.
//  Copyright © 2018 Kesari Tours. All rights reserved.
//

import UIKit

class TherapistCell: UITableViewCell {

    @IBOutlet weak var ibQrCodeImgView : UIImageView!
    @IBOutlet weak var ibName: UILabel!
    @IBOutlet weak var ibtherapistId: UILabel!
    @IBOutlet weak var ibEmailId: UILabel!
    @IBOutlet weak var ibMobNo: UILabel!
   
    func setCellData(dict:NSDictionary) {
        let firstName = dict["firstName"] as? String ?? ""
        let lastName = dict["lastName"] as? String  ?? ""
        let title = dict["title"] as? String  ?? ""
        ibName.text = "\(title) \(firstName) \(lastName)"
            
        if let name = dict["therapistId"] as? String {
            ibtherapistId.text = name
        } else {
            ibtherapistId.text = ""
        }
        if let name = dict["emailId"] as? String {
            ibEmailId.text = name
        } else {
            ibEmailId.text = ""
        }
        if let name = dict["mobileNo"] as? String {
            ibMobNo.text = name
        } else {
            ibMobNo.text = ""
        }
    }
    
}
