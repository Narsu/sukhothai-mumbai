//
//  TherapistListVC.swift
//  SukhoThai
//
//  Created by kesarimac2 on 06/08/18.
//  Copyright © 2018 Kesari Tours. All rights reserved.
//

import UIKit

class TherapistListVC: UIViewController {
    
    let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
    var arrayOfTherapist = NSMutableArray()
    var blurEffectView = UIVisualEffectView()
    let imageView = UIImageView()
    var qrcodeImageString = ""
    @IBOutlet weak var ibTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ibTableView.register(UINib(nibName: "TherapistCell", bundle: nil), forCellReuseIdentifier: "cell")
        ibTableView.tableFooterView = UIView()
        ibTableView.allowsSelection = true
        ibTableView.backgroundColor = tableviewBackgroundColor
        ibTableView.separatorColor = separatorColor//#colorLiteral(red: 0.4115892947, green: 0.2305042148, blue: 0.4444019794, alpha: 1)
        ibTableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let imgWidth = self.view.frame.width
        let imgHeight = self.view.frame.height
        imageView.frame = CGRect(x: self.view.center.x/2 - 30, y: self.view.center.y/2 - 60, width: imgWidth/2 + 60, height: imgWidth/2 + 60)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        self.blurEffectView.addGestureRecognizer(tap)
    }
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        blurEffectView.removeFromSuperview()
        imageView.removeFromSuperview()
        dismiss(animated: true, completion: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        blurEffectView.removeFromSuperview()
        imageView.removeFromSuperview()
    }
    override func viewDidAppear(_ animated: Bool) {
        getAllTherapist()
    }
    func getAllTherapist() {
        if isConnectedToNetwork() {
            let branchcode = getUserdefaultDataForKey(Key: BRANCH) as! String
            let  urlString = urlGetAllTherapist(branchCode: branchcode)
            ApiManager.shared.requestApiWithDataType(methodType: GET, urlString: urlString, completionHandeler: { (response, isSuccess, error) in
                if isSuccess!
                {
                    let dict = convertStringToDictionary(json: response as! String) as NSDictionary?
                    let success = dict!["success"] as? Bool ?? false
                    ProgressHud.hide(fromView: self.view)
                    if success {
                        self.arrayOfTherapist = NSMutableArray(array: dict?["data"] as? NSArray ?? NSArray())
                        self.ibTableView.reloadData()
                        
                    }
                } else {
                    ProgressHud.hide(fromView: self.view)
                    let er = String(describing: error)
                    print("The request timed out -----------\n\(er)")
                }
            })
        } else {
            ProgressHud.hide(fromView: self.view)
            //ProgressHud.textHud(onview: self.view, message: "Network connection not available.")
            showAlertMessage(message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "InternetConnectionAlert", comment: ""))
        }
    }
    func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
}

extension TherapistListVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOfTherapist.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TherapistCell
        cell.backgroundColor = cellBackGroundColor
        let dict = self.arrayOfTherapist[indexPath.row] as? NSDictionary
        
        let codeDict = dict!["therapistQrCode"] as? String
        let therapistImage = dict!["photoCloseUp"] as? String
        let qrcodeString = codeDict?.components(separatedBy: ",").last
        qrcodeImageString = qrcodeString!
        if qrcodeString != nil{
            //cell.ibQrCodeImgView.image = convertBase64ToImage(imageString: qrcodeString!)
            cell.ibQrCodeImgView.setImageToImageView(imageString: therapistImage!)
        }
        cell.setCellData(dict: dict!)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.imageView.image = self.convertBase64ToImage(imageString: self.qrcodeImageString)
        self.blurEffectView.frame = self.view.bounds
        self.blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.isHidden = false
        imageView.isHidden = false
        self.view.addSubview(self.blurEffectView)
        self.view.addSubview(self.imageView)
    }
}
