//
//  TabBarController.swift
//  SukhoThai
//
//  Created by kesarimac2 on 17/07/18.
//  Copyright © 2018 Kesari Tours. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navBarColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        let tabBarColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        let tintColor = #colorLiteral(red: 0.9318091273, green: 0.7251556516, blue: 0.241263926, alpha: 1)
        self.tabBar.barTintColor = tabBarColor//UIColor(named: "tabColor")
        self.tabBar.tintColor = tintColor
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let nav1 = NavigationController()
        nav1.navigationBar.barTintColor = navBarColor//UIColor(named: "tabColor")
        let hometabvc = HomeTabVC(nibName: "HomeTabVC", bundle: nil)
        
        nav1.viewControllers = [hometabvc]
        nav1.title = LocalizationSystem.sharedInstance.localizedStringForKey(key: "APPOINTMENT", comment: "")//"APPOINTMENT"
        nav1.tabBarItem.image = #imageLiteral(resourceName: "home")
        nav1.navigationBar.barStyle = .default
        
        let nav2 = UINavigationController()
        nav2.navigationBar.barTintColor = navBarColor//UIColor(named: "tabColor")
        nav2.navigationBar.isHidden = false
        let SecondVC = storyboard.instantiateViewController(withIdentifier: "ReceiptViewController") as! ReceiptViewController
        nav2.viewControllers = [SecondVC]
        nav2.title = "BILLING"
        nav2.tabBarItem.image = #imageLiteral(resourceName: "tab4")
        
        
        let nav3 = UINavigationController()
        nav3.navigationBar.barTintColor = navBarColor//UIColor(named: "tabColor")
       // let ThirdVC = storyboard.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
       // nav3.viewControllers = [ThirdVC]
        nav3.title = "TRANSACTION"
        nav3.tabBarItem.image = #imageLiteral(resourceName: "dollar")
        
        let nav4 = UINavigationController()
        nav4.navigationBar.barTintColor = navBarColor//UIColor(named: "tabColor")
        let FourthVC = MenuVC(nibName: "MenuVC", bundle: nil)//storyboard.instantiateViewController(withIdentifier: "FeedBackViewController") as! FeedBackViewController
        nav4.viewControllers = [FourthVC]
        nav4.title = "MENU"
        
        nav4.tabBarItem.image = #imageLiteral(resourceName: "menu")
        self.viewControllers = [nav1,nav2,nav3,nav4]

    }

}
